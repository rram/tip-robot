// Sriramajayam

#ifndef EL_OBJECTIVE_ANGLE_PROFILE_H
#define EL_OBJECTIVE_ANGLE_PROFILE_H

#include <el_ObjectiveFunctional.h>
#include <functional>

namespace el
{
  using AngleProfileFunction = std::function<void(const double& s, const double& theta,
						  double* Jval, double* dJval, void* params)>;
  
  //! Objective functional that seeks to match a
  //! given profile as a function of arc length
  class Objective_AngleProfile: public ObjectiveFunctional
  {
  public:
    //! Constructor
    //! \param[in] obj_func Function pointer to evaluate
    //! the integrand and its sensitivities
    //! \param[in] fparams Any function parameters to be passed
    inline Objective_AngleProfile(AngleProfileFunction obj_func, void* fparams=nullptr)
      :ObjectiveFunctional(),
      func_ptr(obj_func),
      func_params(fparams) {}

    //! Destructor
    inline virtual ~Objective_AngleProfile() {}

    // Disable copy and assignment
    Objective_AngleProfile(const Objective_AngleProfile&) = delete;
    Objective_AngleProfile operator=(const Objective_AngleProfile&) = delete;
    
    //! Main functionality: implement objective evaluation
    //! \param[in] theta State at which to evaluate functional
    //! \param[in] params Parameters to aid in evaluation
    //! \param[out] Jval Computed functional value
    virtual void Evaluate(const TendonElastica& elastica, double& Jval, void* params=nullptr) override;

    //! Main functionality:
    //! Evaluate the objective and its sensitivities wrt each load
    //! \param[in] theta State at which to evaluate functional
    //! \param[in] alphaVec Sensitivities
    //! \param[in] params Parameters to aid in evaluation
    //! \param[out] Jval Computed functional value
    //! \param[out] dJval Computed functional gradient
    virtual void Evaluate(const TendonElastica& elastica, 
			  double& Jval, std::vector<double>& dJvals, void* params=nullptr) override;

  private:
    AngleProfileFunction func_ptr; //!< Function pointer to evaluate integrand and its sensitivity
    void* func_params; //!< Parameters to be passed during function evaluation
  };
}

#endif
