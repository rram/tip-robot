// Sriramajayam

#include <el_Objective_PointwiseControl.h>
#include <cmath>

namespace el
{
  // Set the location of a node
  // Either inserts or updates
  void Objective_PointwiseControl::
  SetNodeLocation(const std::vector<double> pt)
  { TargetPt = pt; }
  
  // Main functionality: implement objective evaluation
  void Objective_PointwiseControl::Evaluate(const TendonElastica& elastica,
					    double& Jval, void* params)
  {
    assert(params==nullptr);
    
    // Cartesian coordinates of nodes
    const auto& xy = elastica.GetCartesianCoordinates();
    
    // Compute the objective
    const double* qt = &xy[2*NodeNum]; // Current coordinates
    Jval = 0.5*( (TargetPt[0]-qt[0])*(TargetPt[0]-qt[0]) +
		  (TargetPt[1]-qt[1])*(TargetPt[1]-qt[1]) );
    
    // -- done --
    return;
  }
  

  // Main functionality:
  void Objective_PointwiseControl::Evaluate(const TendonElastica& elastica,
					    double& Jval,  std::vector<double>& dJvals,
					    void* params)
  {
    assert(params==nullptr);
    
    // Initialize objective and sensitivities
    Jval = 0.;
    const int nLoads = elastica.GetNumLoads();
    if(static_cast<int>(dJvals.size())<nLoads) dJvals.resize(nLoads);
    std::fill(dJvals.begin(), dJvals.end(), 0.);
    
    // Access cartesian coordinates and their sensitivities
    const auto& xy = elastica.GetCartesianCoordinates();
    const auto& xy_sensitivity = elastica.GetCoordinateSensitivity();

    // node coordinates
    const double* qt = &xy[2*NodeNum]; 
    
    // Functional
    Jval = 0.5*( (qt[0]-TargetPt[0])*(qt[0]-TargetPt[0]) + (qt[1]-TargetPt[1])*(qt[1]-TargetPt[1]) );

    // Derivatives
    for(int L=0; L<nLoads; ++L)
      dJvals[L] += (qt[0]-TargetPt[0])*xy_sensitivity[L][2*NodeNum] + (qt[1]-TargetPt[1])*xy_sensitivity[L][2*NodeNum+1];

    // -- done --
    return;
  }
}
