// Sriramajayam

#ifndef EL_OBJECTIVE_POINTWISE_CONTROL_H
#define EL_OBJECTIVE_POINTWISE_CONTROL_H

#include <el_ObjectiveFunctional.h>
#include <map>
#include <cassert>

namespace el
{
  //! Objective functional that enforces specific nodes
  //! to match specific points
  class Objective_PointwiseControl: public ObjectiveFunctional
  {
  public:
    //! Constructor
    //! \param[in] nodenum Node number to control
    //! \param[in] pt Desired location for the point
    inline Objective_PointwiseControl(const int nodenum, const std::vector<double> pt)
      :ObjectiveFunctional(),
      NodeNum(nodenum),
      TargetPt(pt)
      {}

    //! Destructor
    inline virtual ~Objective_PointwiseControl() {}
    
    // Disable copy and assignment
    Objective_PointwiseControl(const Objective_PointwiseControl&) = delete;
    Objective_PointwiseControl operator=(const Objective_PointwiseControl&) = delete;

    //! Set the location of a node
    //! Either inserts or updates
    //! \param[in] pt desired Cartesian coordinates
    void SetNodeLocation(const std::vector<double> pt);
      
    //! Main functionality: implement objective evaluation
    //! \param[in] theta State at which to evaluate functional
    //! \param[in] params Parameters to aid in evaluation
    //! \param[out] Jval Computed functional value
    virtual void Evaluate(const TendonElastica& elastica, double& Jval, void* params=nullptr) override;

    //! Main functionality:
    //! Evaluate the objective and its sensitivities wrt each load
    //! \param[in] theta State at which to evaluate functional
    //! \param[in] alphaVec Sensitivities
    //! \param[in] params Parameters to aid in evaluation
    //! \param[out] Jval Computed functional value
    //! \param[out] dJval Computed functional gradient
    virtual void Evaluate(const TendonElastica& elastica, 
			  double& Jval, std::vector<double>& dJvals, void* params=nullptr) override;

  private:
    const int NodeNum; //!< Node number to be controlled
    std::vector<double> TargetPt; //!< Desired location for the node
  };
}

#endif
