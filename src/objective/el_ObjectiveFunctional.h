// Sriramajayam

#ifndef EL_OBJECTIVE_FUNCTIONAL_H
#define EL_OBJECTIVE_FUNCTIONAL_H

#include <el_TendonElastica.h>
#include <vector>
#include <cassert>
#include <el_Utils.h>

namespace el
{
  //! Base class for defining objective functionals
  class ObjectiveFunctional
  {
  public:
    //! Constructor
    inline ObjectiveFunctional() {}

    //! Destructor
    inline virtual ~ObjectiveFunctional() {}

    // Disable copy and assignment
    ObjectiveFunctional(const ObjectiveFunctional&) = delete;
    ObjectiveFunctional& operator=(const ObjectiveFunctional&) = delete;
    
    //! Main functionality: implement objective evaluation
    //! \param[in] theta State at which to evaluate functional
    //! \param[in] params Parameters to aid in evaluation
    //! \param[out] Jval Computed functional value
    //! To be implemented by derived classes
    virtual void Evaluate(const TendonElastica& elastica, double& Jval, void* params=nullptr) = 0;

    //! Main functionality:
    //! Evaluate the objective and its sensitivities wrt each load
    //! \param[in] theta State at which to evaluate functional
    //! \param[in] alphaVec Sensitivities
    //! \param[in] params Parameters to aid in evaluation
    //! \param[out] Jval Computed functional value
    //! \param[out] dJval Computed functional gradient
    //! To be implemented by derived classes
    virtual void Evaluate(const TendonElastica& elastica, 
			  double& Jval, std::vector<double>& dJvals, void* params=nullptr) = 0;
    
  };
}


#endif
