// Sriramajayam

#include <el_Objective_AngleProfile.h>

namespace el
{
  // Main functionality: evaluate the objective functional
  void Objective_AngleProfile::Evaluate(const TendonElastica& elastica, double& Jval, void* params)
  {
    assert(params==nullptr);
    
    // Initialize objective
    Jval = 0.;

    // Access
    const auto& ElmArray = elastica.GetElementArray();
    const auto& L2GMap = elastica.GetLocalToGlobalMap();
    const int nElements = static_cast<int>(ElmArray.size());
    const auto& thetadofs = elastica.GetStateField();

    // Integrate
    double fval, thetaval;
    for(int e=0; e<nElements; ++e)
      {
	const auto& Qpts = ElmArray[e]->GetIntegrationPointCoordinates(0);
	const auto& Qwts = ElmArray[e]->GetIntegrationWeights(0);
	const int nQuad = static_cast<int>(Qwts.size());
	const int nDof = ElmArray[e]->GetDof(0);
	const auto& ShpVals = ElmArray[e]->GetShape(0);
	for(int q=0; q<nQuad; ++q)
	  {
	    // Value of the state at this quadrature point
	    thetaval = 0.;
	    for(int a=0; a<nDof; ++a)
	      thetaval += thetadofs[L2GMap.Map(0,a,e)]*ShpVals[q*nDof+a];
	      
	    // Value of the integrand at this point
	    func_ptr(Qpts[q], thetaval, &fval, nullptr, func_params);
	      
	    // Update the objective
	    Jval += Qwts[q]*fval;
	  }
      }

    // -- done --
    return;
  }


  // Main functionality: evaluate the objective functional and its sensitivities
  void Objective_AngleProfile::Evaluate(const TendonElastica& elastica,
					double& Jval, std::vector<double>& dJvals, void* params)
  {
    assert(params==nullptr);
    
    // Initialize objective and sensitivities
    Jval = 0.;
    const int nLoads = elastica.GetNumLoads();
    if(static_cast<int>(dJvals.size())<nLoads) dJvals.resize(nLoads);
    std::fill(dJvals.begin(), dJvals.end(), 0.);

    // Access
    const auto& ElmArray = elastica.GetElementArray();
    const auto& L2GMap = elastica.GetLocalToGlobalMap();
    const int nElements = static_cast<int>(ElmArray.size());
    const auto& thetadofs = elastica.GetStateField();
    const auto& alphaVec = elastica.GetSensitivityFields();

    // Integrate
    double fval, dfval;
    double thetaval;
    double alphaval;
    for(int e=0; e<nElements; ++e)
      {
	const auto& Qpts = ElmArray[e]->GetIntegrationPointCoordinates(0);
	const auto& Qwts = ElmArray[e]->GetIntegrationWeights(0);
	const int nQuad = static_cast<int>(Qwts.size());
	const int nDof = ElmArray[e]->GetDof(0);
	const auto& ShpVals = ElmArray[e]->GetShape(0);
	
	for(int q=0; q<nQuad; ++q)
	  {
	    // Theta value here
	    thetaval = 0.;
	    for(int a=0; a<nDof; ++a)
	      thetaval += thetadofs[L2GMap.Map(0,a,e)]*ShpVals[q*nDof+a];
	    
	    // Integrand and its derivative wrt theta
	    func_ptr(Qpts[q], thetaval, &fval, &dfval, func_params);

	    // Update the objective
	    Jval += Qwts[q]*fval;

	    // Update the sensitivities
	    for(int loadnum=0; loadnum<nLoads; ++loadnum)
	      {
		// Value of the sensitivity wrt loadnum at this point
		alphaval = 0.;
		for(int a=0; a<nDof; ++a)
		  alphaval += alphaVec[loadnum][L2GMap.Map(0,a,e)]*ShpVals[q*nDof+a];
		
		// Update
		dJvals[loadnum] += Qwts[q]*dfval*alphaval;
	      }
	  }
      }

    // -- done --
    return;
  }
  
}
