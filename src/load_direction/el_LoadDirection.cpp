// Sriramajayam

#include <el_LoadDirection.h>
#include <el_TendonElastica.h>

namespace el
{
  // Constructor
  LoadDirection::LoadDirection(const int pnum, const TendonElastica& str)
    :elastica(str),
     LoadNum(pnum),
     nLoads(str.GetNumLoads()),
     ElmIntervals({}),
     dHval(nLoads),
     dVval(nLoads),
     dir_deriv_Hval({}),
     dir_deriv_Vval({}),
     dir_deriv_index({})
  { assert(LoadNum>=0 && LoadNum<nLoads); }
  
}
