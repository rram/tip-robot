// Sriramajayam

#ifndef EL_FOLLOWER_LOAD_DIRECTION_H
#define EL_FOLLOWER_LOAD_DIRECTION_H

#include <el_LoadDirection.h>
#include <cassert>

namespace el
{
  // Forward declaration
  class TendonElastica;
  
  //! Class defining a follower load that always
  //! remains normal to the elastica at the loading point
  class FollowerLoadDirection: public LoadDirection
    {
    public:
      //! Constructor
      //! \param[in] pnode Node at which load is applied
      //! \param[in] pnum load number corresponding to this direction
      //! \param[in] nloads Total number of loads
      FollowerLoadDirection(const int pnode, const int pnum, const TendonElastica& str);
      
      //! Destructor
      inline virtual ~FollowerLoadDirection() {}

      // Disable copy and assignment
      FollowerLoadDirection(const FollowerLoadDirection&) = delete;
      FollowerLoadDirection operator=(const FollowerLoadDirection&) = delete;

      //! Returns the loading node
      inline int GetLoadingNode() const
      { return PNode; }
      
      //! Main functionality
      //! update the direction cosines and their sensitivities
      virtual void UpdateDirection() override;

    private:
      const int PNode;
    };
}

#endif
