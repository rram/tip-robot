// Sriramajayam

#ifndef EL_CONSTANT_LOAD_DIRECTION_H
#define EL_CONSTANT_LOAD_DIRECTION_H

#include <el_LoadDirection.h>
#include <cassert>
#include <cmath>

namespace el
{
  // Forward declaration
  class TendonElastica;
  
  //! As the name suggests, this class implements
  //! a constant loading direction that does not change
  //! with the solution
  class ConstantLoadDirection: public LoadDirection
  {
  public:
    //! Constructor
    //! \param[in] pnode Node at which load is imposed
    //! \param[in] pnum Load number corresponding to this direction
    //! \param[in] nloads Total number of loads
    //! \param[in] dir Loading direction to use
    inline ConstantLoadDirection(const int pnode, const int pnum, const TendonElastica& str, const double* dir)
      :LoadDirection(pnum, str),
      PNode(pnode)
    {
      // Check that the provided direction has unit magnitude
      assert(std::abs(dir[0]*dir[0]+dir[1]*dir[1]-1.)<1.e-6);
      
      // Assign element intervals
      ElmIntervals.resize(1);
      ElmIntervals[0].first = 0;
      ElmIntervals[0].second = PNode-1;

      // Direction cosines
      Hval = dir[0]; Vval = dir[1];

      // Sensitivities: trivial
      std::fill(dHval.begin(), dHval.end(), 0.);
      std::fill(dVval.begin(), dVval.end(), 0.);
      
      // No variations
    }
    
    //! Destructor
    inline virtual ~ConstantLoadDirection() {}
    
    // Disable copy and assignment
    ConstantLoadDirection(const ConstantLoadDirection&) = delete;
    ConstantLoadDirection operator=(const ConstantLoadDirection&) = delete;
    
    //! Returns the loading node
    inline int GetLoadingNode() const
    { return PNode; }
    
    //! No need to update the direction
    inline virtual void UpdateDirection() override
    { return; }

  private:
    const int PNode;
      
  };

}
#endif
