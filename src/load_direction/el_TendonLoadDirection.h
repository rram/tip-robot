// Sriramajayam

#ifndef EL_TENDON_LOAD_DIRECTION_H
#define EL_TENDON_LOAD_DIRECTION_H

#include <el_LoadDirection.h>
#include <el_TendonLoadPoints.h>

namespace el
{
  // Forward declaration
  class TendonElastica;
  
  //! Class defining a follower load that is oriented towards a fixed point.
  //! Such a direction is generally realized with tendon loads, with the tendon
  //! routed through a fixed point.
  class TendonLoadDirection: public LoadDirection
    {
    public:
      //! Constructor
      //! \param[in] pnode Node at which load is applied
      //! \param[in] pnum Load number corrersponding to this direction
      //! \param[in] nloads Total number of loads
      //! \param[in] pcoords Coordinates of the fixed routing point
    TendonLoadDirection(const int pnode,
			const int pnum, const TendonElastica& str,
			const RoutingPost& rpost);
   
      //! Destructor
      inline virtual ~TendonLoadDirection() {}

      // Disable copy and assignment
      TendonLoadDirection(const TendonLoadDirection&) = delete;
      TendonLoadDirection operator=(const TendonLoadDirection&) = delete;
      
      //! Returns the location of the routing point
      inline const RoutingPost* GetRoutingPost() const
      { return &RtPost; }

      //! Returns the loading node
      inline int GetLoadingNode() const
      { return PNode; }
      
      //! Main functionality
      //! Update the load direction and its sensitivity
      virtual void UpdateDirection() override;

    private:
      const int PNode;   //!< Loading node
      const RoutingPost RtPost; //!< Routing post information
      const detail::TendonLoadPoints lpObj; //!< For coordinate computions
      detail::TendonCoordsStruct rtStruct; //!< Coordinates, sensitivities, variations of loaded nodes.
    };
}

#endif
