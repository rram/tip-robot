// Sriramajayam

#include <el_FollowerLoadDirection.h>
#include <el_TendonElastica.h>
#include <el_Utils.h>
#include <cmath>
#include <iostream>

namespace el
{
  // Constructor
  FollowerLoadDirection::FollowerLoadDirection(const int pnode, const int pnum, const TendonElastica& str)
    :LoadDirection(pnum, str), PNode(pnode)
  {
    // Element interval: (0,PNode-1)
    ElmIntervals.resize(1);
    ElmIntervals[0].first = 0;
    ElmIntervals[0].second = PNode-1;

    // Resize variations: only one non-trivial variation
    dir_deriv_index.resize(1);
    dir_deriv_index[0] = PNode;
	
    dir_deriv_Hval.resize(1);
    dir_deriv_Vval.resize(1);
    
    // Direction cosines, sensitivity and variations are state dependent
  }
  
  // Update the follower load direction and its sensitivities
  void FollowerLoadDirection::UpdateDirection()
  {
    // Access
    const auto& theta = elastica.GetStateField();
    const auto& alphaVec = elastica.GetSensitivityFields();
    
    // Sanity checks
    assert(static_cast<int>(alphaVec.size())==nLoads);
    assert(static_cast<int>(ElmIntervals.size())==1 && ElmIntervals[0].first==0 && ElmIntervals[0].second==PNode-1);
    assert(static_cast<int>(dHval.size())==nLoads && static_cast<int>(dVval.size())==nLoads);
    assert(static_cast<int>(dir_deriv_Hval.size())==1);
    assert(static_cast<int>(dir_deriv_Vval.size())==1);
    assert(static_cast<int>(dir_deriv_index.size())==1 && dir_deriv_index[0]==PNode);
    
    // Node number corresponding to this load: PNode
    
    // Get the angle at the load location
    const double thetaval = theta[PNode];

    // Update the load direction for elements (0,...,PNode-1)
    Hval = -std::sin(thetaval);
    Vval = std::cos(thetaval);
    
    // Update the sensitivities of this direction wrt each load
    for(int p=0; p<nLoads; ++p)
      {
	// Get the sensitivity wrt the p-th load 
	const double alphaval = alphaVec[p][PNode];
	
	// Update the sensitivity of the load direction
	dHval[p] = -alphaval*std::cos(thetaval);
	dVval[p] = -alphaval*std::sin(thetaval);
      }

    // Update the directional derivatives of Hval and Vval
    // <dHval, dtheta> and <dHval, dtheta>
    dir_deriv_Hval[0] = -std::cos(thetaval);
    dir_deriv_Vval[0] = -std::sin(thetaval);

    // done
    return;
  }
  
}
