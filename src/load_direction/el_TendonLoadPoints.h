// Sriramajayam

#ifndef EL_TENDON_LOAD_POINTS_H
#define EL_TENDON_LOAD_POINTS_H

#include <el_LoadDirection.h>

namespace el
{
  class TendonElastica;
  
  //! Helper struct defining the details of a routing post
  struct RoutingPost
  {
    double center[2];   //!< Center of the post
    double radius;         //!< Radius of the post. Can be zero
    double orientation; //!< +1 or -1, defining if the winding is clockwise/counter-clockwise
    inline RoutingPost()
      :center{0.,0.}, radius(0.), orientation(0.) {}
    inline RoutingPost(const RoutingPost& obj)
      :center{obj.center[0],obj.center[1]},
      radius(obj.radius), orientation(obj.orientation) {}
  };

  
  namespace detail
  {
    using Coord2D = std::pair<double,double>; //!< 2D coordinates

    //! Helper struct
    struct TendonCoordsStruct
    {
      std::vector<Coord2D> Coords; //!< Coordinates
      std::vector<std::vector<Coord2D>> dCoords; //!< Sensitivities of coordinates, dCoords[i][L] = d(Xi,Yi)/dPL
      std::vector<std::vector<Coord2D>> var_Coords; //!< Variations of coordinates, var_Coords[i][a] = <delta(Xi,Yi),Na>

      //! Constructor
      TendonCoordsStruct();

      //! Copy constructor
      TendonCoordsStruct(const TendonCoordsStruct&);

      //! Resize
      void Resize(const int nPoints, const int nLoads, const int nVars);

      //! Check sizes
      void CheckSizes(const int nPoints, const int nLoads, const int nVars) const;
    };

    
    //! Helper class for computing loading point coordinates, their variations
    //! and their sensitivities
    class TendonLoadPoints
    {
    public:
      //! Constructor
      //! \param[in] rtpost Routing post. Referred to
      //! \param[in] rtnodes Routing nodes, in sequence, copied
      //! \param[in] deltas Offset values along the normal to the centerline
      //! \param[in] nloads Number of loads
      TendonLoadPoints(const RoutingPost& rtpost,
		       const std::vector<int> rtnodes,
		       const std::vector<double> deltas,
		       const int nloads);

      //! Destructor
      inline virtual ~TendonLoadPoints() {}

      //! Copy constructor
      //! \param[in] obj Object to be copied from
      TendonLoadPoints(const TendonLoadPoints& obj);

      //! Main functionality: compute the coordinates, sensitivities and
      //! variations of locations of routing nodes
      void ComputeRoutingNodeDetails(const TendonElastica& elastica,
				     const std::vector<std::pair<int,int>>& ElmIntervals,
				     TendonCoordsStruct& rtStruct) const;
      
    private:
      const RoutingPost* RtPost; //!< Routing post details
      const std::vector<int> RtNodes; //!< Routing nodes
      const std::vector<double> Offsets; //!< Offsets at routing points
      const int nLoads; //!< Number of loads
    };
    
  }
}

#endif
