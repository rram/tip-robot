// Sriramajayam

#ifndef EL_LOAD_DIRECTION_H
#define EL_LOAD_DIRECTION_H

#include <cassert>
#include <vector>

namespace el
{
  //! Distinction between horizontal and vertical directions
  enum class DirCosine
  { Horizontal, Vertical };

  // Forward declaration
  class TendonElastica;
  
  //! Class defining follower load directions
  //! Meant to be used such that each load for an elastica
  //! has one instance of the direction object.
  class LoadDirection
  {
  public:
    //! Constructor
    //! \param[in] pnum Load number corresponding to this direction
    //! \param[in] nloads Total number of loads
    LoadDirection(const int pnum, const TendonElastica& str);
    
    //! Destructor
    //! Nothing to be done
    inline virtual ~LoadDirection() {}

    // Disable copy and assignment
    LoadDirection(const LoadDirection&) = delete;
    LoadDirection& operator=(const LoadDirection&) = delete;
      
    //! Returns the load number
    inline int GetLoadNumber() const
    { return LoadNum; }

    //! Returns the total number of loads
    inline int GetNumLoads() const
    { return nLoads; }
    
    //! Main functionality
    //! Updates the direction for the load based on the current state/sensitivites
    //! To be implemented by derived classes
    //! \param[in] elastica Object with which to synchronize
    virtual void UpdateDirection() = 0;

    //! Main functionality
    //! Get the components of the direction cosines
    //! \param[in] elm Element number for which to return value
    //! \param[in] dir Direction along which to return direction cosine
    inline double GetDirection(const int elm, const DirCosine dir) const
    {
      if(dir==DirCosine::Horizontal)
	return Hval;
      else
	return Vval;
    }
    
    //! Main functionality
    //! Get the sensitivity of the direction cosines
    //! \param[in] elm Element number for which to return value
    //! \param[in] dir Direction along which to return direction cosine
    //! Length equals "nLoads"
    inline const std::vector<double>& GetDirectionSensitivity(const int elm, const DirCosine dir) const
    { 
      if(dir==DirCosine::Horizontal)
	return dHval;
      else
	return dVval;
    }

    //! Main functionality
    //! Get the directional derivatives of the direction cosines
    //! \param[in] dir Direction along which to return direction cosine
    //! Length equals the length of dir_deriv_index
    inline const std::vector<double>& GetDirectionalDerivatives(const DirCosine dir) const
    {
      if(dir==DirCosine::Horizontal)
	return dir_deriv_Hval;
      else
	return dir_deriv_Vval;
    }

    //! Returns the indices of directional derivatives
    inline const std::vector<int>& GetDirectionalDerivativeIndices() const
    { return dir_deriv_index; }

    //! Return the element indices for directional derivatives
    inline const std::vector<std::pair<int,int>>& GetElementIntervals() const
    { return ElmIntervals; }
    
  protected:
    const TendonElastica& elastica;
    const int LoadNum; //!< Load number
    const int nLoads; //!< Total number of loads
    std::vector<std::pair<int,int>> ElmIntervals; //!< Element intervals for directional derivative components
    double Hval, Vval; //!< Direction cosines, HVal,VVal
    std::vector<double> dHval, dVval; //!< Sensitivity of this direction wrt each load. dHval,dVval[L] -> d/dPL
    std::vector<double> dir_deriv_Hval, dir_deriv_Vval; //!< Directional derivatives of Hval and Vval. 
    std::vector<int> dir_deriv_index; //!< Indices of non-zero directional derivatives. 
  };
}

#endif
