// Sriramajayam

#include <el_TendonLoadDirection.h>
#include <el_TendonElastica.h>
#include <cmath>
#include <Element.h>
#include <LocalToGlobalMap.h>
#include <el_Utils.h>
#include <array>
#include <algorithm>
#include <cmath>
#include <cassert>

namespace el
{
  // Constructor
  TendonLoadDirection::TendonLoadDirection(const int pnode,
					   const int pnum, const TendonElastica& str,
					   const RoutingPost& rtpost)
    :LoadDirection(pnum, str),
     PNode(pnode),
     RtPost(rtpost),
     lpObj(RtPost, std::vector<int>({pnode}), std::vector<double>({0.}), str.GetNumLoads())
  {
    // Element intervals
    ElmIntervals.resize(1);
    ElmIntervals[0].first = 0;
    ElmIntervals[0].second = PNode-1;

    // Variations
    const int nVars = PNode+1;
    dir_deriv_index.resize(nVars);
    for(int i=0; i<nVars; ++i)
      dir_deriv_index[i] = i;

    dir_deriv_Hval.resize(nVars);
    dir_deriv_Vval.resize(nVars);
    
    // Coordinate calculations for loading node
    rtStruct.Resize(1, nLoads, nVars); 

    // Load directions etc are state dependent
  } 


  void TendonLoadDirection::UpdateDirection()
  {
    // Sanity checks
    assert(static_cast<int>(ElmIntervals.size())==1 &&
	   ElmIntervals[0].first==0 &&
	   ElmIntervals[0].second==PNode-1);
    assert(static_cast<int>(dHval.size())==nLoads && static_cast<int>(dVval.size())==nLoads);
    const int nVars = PNode+1;
    assert(static_cast<int>(dir_deriv_index.size())==nVars);
    assert(static_cast<int>(dir_deriv_Hval.size())==nVars);
    assert(static_cast<int>(dir_deriv_Vval.size())==nVars);

    // Compute routing node coordinates, sensitivities and varitions 
    lpObj.ComputeRoutingNodeDetails(elastica, ElmIntervals, rtStruct);

    // Cartesian cooridnates (x0, y0) of the post
    const double& X0 = RtPost.center[0];
    const double& Y0 = RtPost.center[1];

    // No sensitivity in the position of the post wrt loads
    const double dX0 = 0.;
    const double dY0 = 0.;

    // No variations of routing post coordinates
    const double var_X0 = 0.;
    const double var_Y0 = 0.;
    
    // Cartesian coordinates (x,y) of the loading point
    const double X = rtStruct.Coords[0].first;
    const double Y = rtStruct.Coords[0].second;
    
    // Compute the loading direction xy ----> rt post
    const double norm = std::sqrt( (X0-X)*(X0-X) + (Y0-Y)*(Y0-Y) );
    assert(norm>1.e-4 && "el::TendonLoadingDirection::UpdateDirection- tendon length is zero");
    Hval = (X0-X)/norm;
    Vval = (Y0-Y)/norm;

    // Compute the sensitivities of the loading direction xy ---> fixed point
    for(int L=0; L<nLoads; ++L)
      {
	// Sensitivity of the loaded point
	const double& dX = rtStruct.dCoords[0][L].first;
	const double& dY = rtStruct.dCoords[0][L].second;
	
	const double dnorm = ((X-X0)*(dX-dX0) + (Y-Y0)*(dY-dY0))/norm;
	dHval[L] = (dX0-dX)/norm - (X0-X)*dnorm/(norm*norm);
	dVval[L] = (dY0-dY)/norm - (Y0-Y)*dnorm/(norm*norm);
      }
    
    // Compute the directional derivatives of Hval and Vval for each nodal variation
    for(int n=0; n<=PNode; ++n)
      {
	// Variations of loading point coordinates
	const double& var_X = rtStruct.var_Coords[0][n].first;
	const double& var_Y = rtStruct.var_Coords[0][n].second;

	// Variation of the norm
	const double var_norm = ((X-X0)*(var_X-var_X0) + (Y-Y0)*(var_Y-var_Y0))/norm;
	  
	// Variations of Hval and Vval
	dir_deriv_Hval[n] = (var_X0-var_X)/norm - (X0-X)*var_norm/(norm*norm);
	dir_deriv_Vval[n] = (var_Y0-var_Y)/norm - (Y0-Y)*var_norm/(norm*norm);
      }

    // done
    return;
  }

}
