// Sriramajayam

#include <el_MomentFunctional.h>
#include <el_TendonElastica.h>

namespace el
{
  // Constructor
  MomentFunctional::MomentFunctional(const TendonElastica& str)
    :elastica(str),
     nLoads(str.GetNumLoads()),
     FuncVal({}),
     dFuncVal({}),
     dir_deriv_FuncVal({}),
     dir_deriv_index({})
  { assert( nLoads>=0 && "el::MomentFunctional- unexpected number of loads"); }

}
