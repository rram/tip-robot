// Sriramajayam

#ifndef EL_MOMENT_FUNCTIONAL_H
#define EL_MOMENT_FUNCTIONAL_H

#include <vector>
#include <map>

namespace el
{
  // Forward declaration
  class TendonElastica;
  
  //! Class defining moment functionals for the for P*M[theta] + G[theta],
  //! where P is a load parameter
  //! Meant to be used such that each one instance
  //! of this class is required for each moment acting
  //! on the elastica
  class MomentFunctional
  {
  public:
    //! Constructor
    //! \param[in] nmom Total number of moments
    MomentFunctional(const TendonElastica& str);
    
    //! Destructor
    inline virtual ~MomentFunctional() {}

    //! Disable copy and assignment
    MomentFunctional(const MomentFunctional&) = delete;
    MomentFunctional& operator=(const MomentFunctional&) = delete;

    //! Returns the total number of loads
    inline int GetNumLoads() const
    { return nLoads; }

    //! Main functionality
    //! Updates the moment functional based on the current state/sensitivities
    //! To be implemented by derived classes
    //! \param[in] elastica Object with which to update
    //! \param[in] load_params Loading parameters
    virtual void UpdateFunctional(const double load_value)  = 0;

    //! Main functionality
    //! Get the moment functional
    inline const std::map<int,double>& GetFunctional() const
    { return FuncVal; }

    //! Main functionality
    //! Get the sensitivity of the moment functional
    inline const std::map<int,std::vector<double>>& GetSensitivities() const
    { return dFuncVal; }

    //! Main functionality
    //! Get the directional derivatives of the moment functional
    inline const std::map<int,std::vector<double>>& GetDirectionalDerivatives() const
    { return dir_deriv_FuncVal; }

    //! Returns the indices of the directional derivatives
    inline const std::map<int,std::vector<int>>&
      GetDirectionalDerivativeIndices() const
    { return dir_deriv_index; }

  protected:
    const TendonElastica& elastica;
    const int nLoads; //!< Total number of loads
    std::map<int, double> FuncVal; //!< Moment functional value, FuncVal[a] = Ma[theta]
    std::map<int, std::vector<double>> dFuncVal; //!< Sensitivity of the functional wrt each load
    std::map<int, std::vector<double>> dir_deriv_FuncVal; //!< Directional derivatives of FuncVal
    std::map<int, std::vector<int>> dir_deriv_index; //!< Indices of directional derivatives
  };
}


#endif
