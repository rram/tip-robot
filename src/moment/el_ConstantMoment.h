// Sriramajayam

#ifndef EL_CONSTANT_MOMENT_H
#define EL_CONSTANT_MOMENT_H

#include <el_MomentFunctional.h>

namespace el
{
  // Forward declare
  class TendonElastica;
  
  //! Class implementing a constant moment M[theta]
  //! There is no depenedence on any external loading
  class ConstantMoment: public MomentFunctional
  {
  public:
    //! Constructor
    //! \param[in] mnode Node at which moment is applied
    //! \param[in] nloads Total number of loads
    //! \param[in] val Value of the moment
    inline ConstantMoment(const int mnode, const TendonElastica& str,
			  const double val)
      :MomentFunctional(str),
      MNode(mnode), MValue(val)
      {
	assert(MNode>=0 && "el::ConstantMoment: Unexpected moment node");
	
	// Value of the functional
	// Negative sign so that moment functional shifts to the LHS
	FuncVal[MNode] = -MValue;
	
	// Sensitivity of the functional to loads is zero
	dFuncVal[MNode] = std::vector<double>(nLoads,0.);
	
	// All directional derivatives are zero
	dir_deriv_FuncVal.clear();
	dir_deriv_index.clear();
      }

    //! Destructor
    inline virtual ~ConstantMoment() {}

    //! Disable copy and assignment
    ConstantMoment(const ConstantMoment&) = delete;
    ConstantMoment operator=(const ConstantMoment&) = delete;

    //! Returns the node at which moment is applied
    inline int GetMomentNode() const
    { return MNode; }
    
    //! Nothing to update
    //! \param[in] obj Ignored.
    //! \param[in] load_value Ignored.
    inline virtual void UpdateFunctional(const double load_value) override
    { return; }

  private:
    const int MNode; //!< Node at which moment is applied
    const double MValue; //!< Value of the moment
  };
}

#endif
