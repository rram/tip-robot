// Sriramajayam

#include <el_TendonElastica.h>
#include <el_TendonElastica_Sensitivity_SNESFuncs.h>

namespace el
{
  // Consistency test for sensitivity calculations
  void TendonElastica::SensitivityConsistencyTest(const int L,
						  const double* alphaVals,
						  LoadParams& load_params,
						  const std::map<int,double>& bc_params)
  {
    // Perturbation values
    const double pEPS = 1.e-6;
    const double mEPS = -1.e-6;

    // Access the solver internals
    auto& snes = alpha_solver.snes;
    auto& solVec = alpha_solver.solVec;
    auto& resVec = alpha_solver.resVec;
    auto& kMat = alpha_solver.kMat;

    // Set values for alpha
    const int nDof = L2GMap->GetTotalNumDof();
    PetscErrorCode ierr;
    for(int i=0; i<nDof; ++i)
      {
	ierr = VecSetValues(solVec, 1, &i, &alphaVals[i], INSERT_VALUES); CHKERRV(ierr);
	alphaVec[L][i] = alphaVals[i];
      }
    ierr = VecAssemblyBegin(solVec); CHKERRV(ierr);
    ierr = VecAssemblyEnd(solVec); CHKERRV(ierr);
    
    // Update the operations, functionals and sensitivities
    UpdateStateOperations(load_params);
    UpdateSensitivityOperations(L, load_params);
    UpdateTendonDirections(load_params.dirs);
    UpdateMomentFunctionals(load_params.values, load_params.moments);

    // Dummy solve parameters with consistent linearization on
    Solve_Params solve_params({.consistentLinearization=true,
	  .EPS=1.e-10, .resScale=1., .dofScale=1., .nMaxIters=0, .verbose=true});

    // Context
    SNESContext ctx;
    ctx.elastica = this;
    ctx.load_params = &load_params;
    ctx.bc_params = &bc_params;
    ctx.solve_params = &solve_params;
    ctx.LoadNum = L;
    ierr = SNESSetApplicationContext(snes, &ctx); CHKERRV(ierr);

    // Assemble the stiffness matrix
    ierr = Sensitivity_Jacobian_Func(snes, solVec, kMat, kMat, nullptr); CHKERRV(ierr);

    // Compute the residual at a pair of perturbed states
    Vec resPlus;
    ierr = VecDuplicate(resVec, &resPlus); CHKERRV(ierr);
    Vec resMinus;
    ierr = VecDuplicate(resVec, &resMinus); CHKERRV(ierr);
    Vec solPlus;
    ierr = VecDuplicate(solVec, &solPlus); CHKERRV(ierr);
    Vec solMinus;
    ierr = VecDuplicate(solVec, &solMinus); CHKERRV(ierr);

    // Perturb alpha and compute residuals
    // Don't alter dirichlet dofs
    for(int a=0; a<nDof; ++a)
      if(bc_params.find(a)==bc_params.end())
	{
	  // Positive perturbation
	  ierr = VecCopy(solVec, solPlus); CHKERRV(ierr);
	  ierr = VecSetValues(solPlus, 1, &a, &pEPS, ADD_VALUES); CHKERRV(ierr);
	  ierr = VecAssemblyBegin(solPlus); CHKERRV(ierr);
	  ierr = VecAssemblyEnd(solPlus); CHKERRV(ierr);
	  for(int n=0; n<nDof; ++n)
	    { ierr = VecGetValues(solPlus, 1, &n, &alphaVec[L][n]); CHKERRV(ierr); }
	  
	  // Residual at positive perturbation
	  UpdateStateOperations(load_params);
	  UpdateSensitivityOperations(L, load_params);
	  UpdateTendonDirections(load_params.dirs);
	  UpdateMomentFunctionals(load_params.values, load_params.moments);
	  ierr = Sensitivity_Residual_Func(snes, solPlus, resPlus, nullptr); CHKERRV(ierr);

	  // Negative perturbation
	  ierr = VecCopy(solVec, solMinus); CHKERRV(ierr);
	  ierr = VecSetValues(solMinus, 1, &a, &mEPS, ADD_VALUES); CHKERRV(ierr);
	  ierr = VecAssemblyBegin(solMinus); CHKERRV(ierr);
	  ierr = VecAssemblyEnd(solMinus); CHKERRV(ierr);
	  for(int n=0; n<nDof; ++n)
	    { ierr = VecGetValues(solMinus, 1, &n, &alphaVec[L][n]); CHKERRV(ierr); }
	  
	  // Residual at negative perturbation
	  UpdateStateOperations(load_params);
	  UpdateSensitivityOperations(L, load_params);
	  UpdateTendonDirections(load_params.dirs);
	  UpdateMomentFunctionals(load_params.values, load_params.moments);
	  ierr = Sensitivity_Residual_Func(snes, solMinus, resMinus, nullptr); CHKERRV(ierr);

	  // Compare numerical and implemented stiffness
	  for(int b=0; b<nDof; ++b)
	    if(bc_params.find(b)==bc_params.end())
	      {
		double kval;
		ierr = MatGetValues(kMat, 1, &b, 1, &a, &kval); CHKERRV(ierr);
		double rplus, rminus;
		ierr = VecGetValues(resPlus, 1, &b, &rplus); CHKERRV(ierr);
		ierr = VecGetValues(resMinus, 1, &b, &rminus); CHKERRV(ierr);
		double knum = (rplus-rminus)/(pEPS-mEPS);
		//std::cout<<"\nK("<<a<<","<<b<<"): "<<kval<<" should be "<<knum<<std::flush;
		if(std::abs(knum-kval)>1.e-5)
		  {
		    std::cout<<"\nel::TendonElastica::SensitivityConsistency failed: "
			     <<kval<<" should be close to "<<knum<<std::flush;
		    std::cout<<"  <---- "<<std::flush;
		    assert(std::abs(knum-kval)<1.e-5);
		  }
	      }
	}

    // Clean up
    ierr = VecDestroy(&resPlus); CHKERRV(ierr);
    ierr = VecDestroy(&resMinus); CHKERRV(ierr);
    ierr = VecDestroy(&solMinus); CHKERRV(ierr);
    ierr = VecDestroy(&solPlus); CHKERRV(ierr);
    return;
  }
    
}
