// Sriramajayam

#include <el_TendonElastica.h>
#include <el_TendonElastica_State_SNESFuncs.h>
#include <el_TendonElastica_Sensitivity_SNESFuncs.h>
#include <el_Strut_State_Op.h>
#include <el_Strut_Sensitivity_Op.h>
#include <P11DElement.h>
#include <iostream>

namespace el
{
  // Constructor
  TendonElastica::TendonElastica(const std::vector<double>& coords,
				 const double ei, 
				 const std::vector<int>& PNodes,
				 const double* datum)
    :coordinates(coords),
     EI(ei),
     origin{datum[0], datum[1]},
     nNodes(static_cast<int>(coords.size())),
     nElements(nNodes-1),
     nLoads(PNodes.size()),
     Load2ElmIntervalMap(nLoads),
     LoadCoeff(nLoads, std::vector<double>(nLoads,0.)),
     state_is_dirty(true),
     sense_is_dirty(true)
  {
    // Check that coordinates are in sequence
    for(int n=1; n<nNodes; ++n)
      { assert(coordinates[n]>coordinates[n-1]); }
    
    // Check that the loading nodes are in sequence
    assert(nElements==nNodes-1 && "el::TendonElastica- Unexpected number of nodes/elements");
    assert(nLoads>0 && "el::TendonElastica- Expected at least one load");
    assert(PNodes[0]>0 && "el::TendonElastica- Cannot impose a load at the clamped end");
    for(int p=0; p<nLoads-1; ++p)
      { assert(PNodes[p+1]>PNodes[p] && "el::TendonElastica- Expected loads in sequence"); }

    // Element connectivities
    connectivity.resize(2*nElements);
    for(int e=0; e<nElements; ++e)
      { connectivity[2*e] = e+1;
	connectivity[2*e+1] = e+2; }
    
    // Loading coefficients for each section of elements
    //          P0         P1                 Pn
    // |---------|----------|-----------...---|------
    // or Pn acts at the end
    for(int n=0; n<nLoads; ++n)
      {
	std::fill(LoadCoeff[n].begin(), LoadCoeff[n].end(), 0.);
	for(int m=n; m<nLoads; ++m)
	  LoadCoeff[n][m] = 1.;
      }
    nElementSections = nLoads;

    // If Pn is not the terminal load, the last section of elements needs 0 coefficients
    if(PNodes[nLoads-1]<nNodes-1) 
      {	LoadCoeff.push_back( std::vector<double>(nLoads, 0.) );
	++nElementSections; }

    // Active regions for each load
    // Load i is active from element 0 to element PNodes[i]-1
    for(int i=0; i<nLoads; ++i)
      { Load2ElmIntervalMap[i][0] = 0;
	Load2ElmIntervalMap[i][1] = PNodes[i]-1; }
    
    // Element intervals
    ElmIntervals.resize(nElementSections);
    ElmIntervals[0][0] = 0;
    for(int i=1; i<nLoads; ++i)
      ElmIntervals[i][0] = PNodes[i-1];
    for(int i=0; i<nLoads; ++i)
      ElmIntervals[i][1] = PNodes[i]-1;
    // Add the last section
    if(nElementSections>nLoads) 
      { ElmIntervals[nElementSections-1][0] = PNodes[nLoads-1];
	ElmIntervals[nElementSections-1][1] = nElements-1; }
	  
    // Create elements
    Segment<1>::SetGlobalCoordinatesArray(coordinates);
    ElmArray.resize(nElements);
    for(int e=0; e<nElements; ++e)
      ElmArray[e] = new P11DElement<1>(connectivity[2*e], connectivity[2*e+1]);

    // Local to global map
    L2GMap = new StandardP11DMap(ElmArray);

    // State variable calculations
    //------------------------------
    // Operations: set a trivial load
    state_OpsArray.resize(nElements);
    for(int e=0; e<nElements; ++e)
      state_OpsArray[e] = new Strut_State_Op(e, ElmArray[e], EI, 0., 0.);
      
    // Assembler
    state_Asm = new StandardAssembler<Strut_State_Op>(state_OpsArray, *L2GMap);
    // State
    theta.resize(nNodes);
    std::fill(theta.begin(), theta.end(), 0.);
    xy.resize(2*nNodes);
    
    // Sensitivity variable calculations
    // -----------------------------------
    // Operations: set trivial loads
    sense_OpsArray.resize(nElements);
    for(int e=0; e<nElements; ++e)
      sense_OpsArray[e] = new Strut_Sensitivity_Op(e, ElmArray[e], EI, 0., 0., 0., 0.);
      
    // Assembler
    sense_Asm = new StandardAssembler<Strut_Sensitivity_Op>(sense_OpsArray, *L2GMap);
    // Sensitivity
    alphaVec.resize(nLoads);
    xy_sensitivity.resize(nLoads);
    for(int i=0; i<nLoads; ++i)
      {
	alphaVec[i].resize(nNodes);
	std::fill(alphaVec[i].begin(), alphaVec[i].end(), 0.);
	xy_sensitivity[i].resize(2*nNodes);
      }

    // State sovler
    std::vector<int> nnz;
    state_Asm->CountNonzeros(nnz);
    theta_solver.Initialize(nnz,
			    State_Residual_Func,
			    State_Jacobian_Func);
    
    // Sensitivity solver
    nnz.clear();
    sense_Asm->CountNonzeros(nnz);
    alpha_solver.Initialize(nnz,
			    Sensitivity_Residual_Func,
			    Sensitivity_Jacobian_Func);
    
    // -- done --
  }

  // Destructor
  TendonElastica::~TendonElastica()
  {
    for(auto& x:ElmArray) delete x;
    delete L2GMap;
    for(auto& x:state_OpsArray) delete x;
    delete state_Asm;
    for(auto& x:sense_OpsArray) delete x;
    delete sense_Asm;
    theta_solver.Destroy();
    alpha_solver.Destroy();
  }

        
  // Utility function that updates loading directions based on the current solution
  void TendonElastica::UpdateTendonDirections(std::vector<LoadDirection*>& load_dirs) const
  {
    assert(static_cast<int>(load_dirs.size())==nLoads);
      
    // Update loading directions with the current solution guess
    for(int L=0; L<nLoads; ++L)
      load_dirs[L]->UpdateDirection();

    // -- done --
    return;
  }


  // Utility function that updates moment functionals based on the current solution
  void TendonElastica::UpdateMomentFunctionals(const std::vector<double>& load_values,
					       std::vector<MomentFunctional*>& moments) const
  {
    assert(static_cast<int>(load_values.size())==nLoads &&
	   "el::TendonElastica::UpdateMomentFunctionals- unexpected number of loads");
    
    const int nMoments = static_cast<int>(moments.size());
    for(int L=0; L<nMoments; ++L)
      moments[L]->UpdateFunctional(load_values[L]);

    // -- done --
    return;
  }

  
  // Update loads in state operations
  void TendonElastica::UpdateStateOperations(const LoadParams& load_params)
  {
    // Unpackage
    const auto& loads = load_params.values;
    const auto& load_dirs = load_params.dirs;

    // Temporaries
    std::vector<double> frcCosines(2*nLoads);
    double Hfrc, Vfrc;
    
    // Loop over element sections
    // Compute direction cosines for each element within each element
    // Update operation
    for(int i=0; i<nElementSections; ++i)
      for(int e=ElmIntervals[i][0]; e<=ElmIntervals[i][1]; ++e)
	{
	  // Direction cosines for this element
	  for(int L=0; L<nLoads; ++L)
	    { frcCosines[2*L] = load_dirs[L]->GetDirection(e, DirCosine::Horizontal);
	      frcCosines[2*L+1] = load_dirs[L]->GetDirection(e, DirCosine::Vertical); }

	  // Compute loading for this element
	  Hfrc = Vfrc = 0.;
	  for(int j=0; j<nLoads; ++j)
	    {
	      Hfrc += LoadCoeff[i][j]*loads[j]*frcCosines[2*j];
	      Vfrc += LoadCoeff[i][j]*loads[j]*frcCosines[2*j+1];
	    }

	  // Set (Hfrc, Vfrc) for the operation over this element
	  state_OpsArray[e]->SetLoad(Hfrc, Vfrc);
	}
    
    // -- done --
    return;
  }

  // Update sensitivity operations using the given load number
  void TendonElastica::UpdateSensitivityOperations(const int k,
						   const LoadParams& load_params) const
  {
    assert(k>=0 && k<nLoads);
    
    // Unpackage
    const auto& loads = load_params.values;
    const auto& load_dirs = load_params.dirs;

    // Temporaries
    // frcCosines[2j,2j+1] = (hj,vj)
    std::vector<double> frcCosines(2*nLoads);
    // dfrcCosines[2j,2j+1] = (dhj/dPk, dvj/dPk) -> sensitivity of j-th direction wrt k-th load
    std::vector<double> dfrcCosines(2*nLoads);
    double frc[2], dfrc[2];
    
    // Loop over element sections
    // Loop over elements
    // Compute direction cosines and sensitivites for it
    // Update
    for(int i=0; i<nElementSections; ++i)
      for(int e=ElmIntervals[i][0]; e<=ElmIntervals[i][1]; ++e)
	{
	  // Direction cosines for each load
	  // Their derivatives wrt Pk
	  for(int L=0; L<nLoads; ++L)
	    {
	      frcCosines[2*L] = load_dirs[L]->GetDirection(e, DirCosine::Horizontal);
	      frcCosines[2*L+1] = load_dirs[L]->GetDirection(e, DirCosine::Vertical);
	      dfrcCosines[2*L] = load_dirs[L]->GetDirectionSensitivity(e, DirCosine::Horizontal)[k];
	      dfrcCosines[2*L+1] = load_dirs[L]->GetDirectionSensitivity(e, DirCosine::Vertical)[k];
	    }

	  // Compute the load and its sensitivity
	  frc[0] = frc[1] = 0.;
	  dfrc[0] = dfrc[1] = 0.;
	  for(int j=0; j<nLoads; ++j)
	    {
	      frc[0]  += LoadCoeff[i][j]*loads[j]*frcCosines[2*j];
	      frc[1]  += LoadCoeff[i][j]*loads[j]*frcCosines[2*j+1];
	      dfrc[0] += LoadCoeff[i][j]*loads[j]*dfrcCosines[2*j];  // P*(dH/dPk, dV/Pk)
	      dfrc[1] += LoadCoeff[i][j]*loads[j]*dfrcCosines[2*j+1]; 
	    }
	  
	  dfrc[0] += LoadCoeff[i][k]*frcCosines[2*k+0]; // Additional term: 1*(Hk,Vk)
	  dfrc[1] += LoadCoeff[i][k]*frcCosines[2*k+1];
	  
	  // Set the loads and sensitivities into operations
	  sense_OpsArray[e]->SetLoadValues(frc, dfrc);
	}
    
    // -- done --
    return;
  }
  

  // Compute the strain energy of the elastica in its current configuration
  double TendonElastica::GetStrainEnergy() const
  {
    assert(state_is_dirty==false);
    double SE = 0.;
    
    // Compute the total strain energy
    double dtheta;
    const int nDof = 2;
    for(int e=0; e<nElements; ++e)
      {
	// Access integration weights
	const auto& Qwts = ElmArray[e]->GetIntegrationWeights(0);
	const int nQuad = static_cast<int>(Qwts.size());
	
	// Integrate
	for(int q=0; q<nQuad; ++q)
	  {
	    dtheta = 0.;
	    for(int a=0; a<nDof; ++a)
	      dtheta += theta[L2GMap->Map(0,a,e)]*ElmArray[e]->GetDShape(0,q,a,0);
	    
	    // Update the energy
	    SE += Qwts[q]*0.5*EI*dtheta*dtheta;
	  }
      }
    return SE;
  }

}

