// Sriramajayam

#include <el_TendonElastica.h>

namespace el
{
  // Compute nodal Cartesian coordinates with the current state
  void TendonElastica::ComputeCartesianCoordinates() const
  {
    //assert(!state_is_dirty);
    std::fill(xy.begin(), xy.end(), 0.);

    // origin at node #0
    xy[0] = origin[0];
    xy[1] = origin[1];

    double delta_x, delta_y, thetaval;
    for(int e=0; e<nElements; ++e)
      {
	const auto* Elm = ElmArray[e];
	const auto& Qwts = Elm->GetIntegrationWeights(0);
	const int nQuad = static_cast<int>(Qwts.size());
	const int nDof = Elm->GetDof(0);
	delta_x = 0.; // integral over this element
	delta_y = 0.;
	for(int q=0; q<nQuad; ++q)
	  {
	    // theta value here
	    thetaval = 0.;
	    for(int a=0; a<nDof; ++a)
	      thetaval += theta[L2GMap->Map(0,a,e)]*Elm->GetShape(0,q,a);

	    // Integrate
	    delta_x += Qwts[q]*std::cos(thetaval);
	    delta_y += Qwts[q]*std::sin(thetaval);
	  }

	// Update Cartesian coordinates
	const int n0 = L2GMap->Map(0,0,e);
	const int n1 = L2GMap->Map(0,1,e);
	xy[2*n1+0] = xy[2*n0+0] + delta_x;
	xy[2*n1+1] = xy[2*n0+1] + delta_y;
      }

    // done
    return;

  }
  
  // Compute sensitivities of Cartesian coordinates
  void TendonElastica::ComputeCartesianCoordinateSensitivities() const
  {
    //assert(!state_is_dirty && !sense_is_dirty);

    double thetaval;
    double alphaval;
    double delta_xsense, delta_ysense;
    for(int L=0; L<nLoads; ++L)
      {
	const auto& alpha = alphaVec[L];
	auto& xysense = xy_sensitivity[L];
	std::fill(xysense.begin(), xysense.end(), 0.);
	
	// sensitivity of node# 0 -> (0,0) automatically 

	for(int e=0; e<nElements; ++e)
	  {
	    const auto* Elm = ElmArray[e];
	    const auto& Qwts = Elm->GetIntegrationWeights(0);
	    const int nQuad = static_cast<int>(Qwts.size());
	    const int nDof = Elm->GetDof(0);

	    delta_xsense = 0.;
	    delta_ysense = 0.;
	    for(int q=0; q<nQuad; ++q)
	      {
		thetaval = 0.;
		alphaval = 0.;
		for(int a=0; a<nDof; ++a)
		  {
		    const double& Na = Elm->GetShape(0,q,a);
		    const int node = L2GMap->Map(0,a,e);
		    thetaval += theta[node]*Na;
		    alphaval += alpha[node]*Na;
		  }

		delta_xsense -= Qwts[q]*std::sin(thetaval)*alphaval;
		delta_ysense += Qwts[q]*std::cos(thetaval)*alphaval;
	      }

	    // Update nodal sensitivities
	    const int n0 = L2GMap->Map(0,0,e);
	    const int n1 = L2GMap->Map(0,1,e);
	    xysense[2*n1+0] = xysense[2*n0+0] + delta_xsense;
	    xysense[2*n1+1] = xysense[2*n0+1] + delta_ysense;
	  }
      }

    // -- done --
    return;

  }
}
