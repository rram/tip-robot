// Sriramajayam

#ifndef EL_TENDON_ELASTICA_H
#define EL_TENDON_ELASTICA_H

#include <array>
#include <Element.h>
#include <LocalToGlobalMap.h>
#include <Assembler.h>
#include <SNES_Solver.h>
#include <el_TendonElastica_Structs.h>
#include <el_Strut_State_Op.h>
#include <el_Strut_Sensitivity_Op.h>

namespace el
{
  class TendonElastica
  {
  public:
    //! Constructor
    //! \param[in] coords Nodal coordinates (1D). Referred to, not copied
    //! \param[in] conn Element connectuivities. Referred to, not copied
    //! \param[in] ei Modulus. Copied
    //! \param[in] pnodes Nodes at which the loads are imposed. Used to partition the element set
    //! Assumed to be in increasing order, numbered from 0. Referred to, not copied
    //! Left end is assumed to be clamped
    //! \warning Assumes that the global coordinates have been set
    TendonElastica(const std::vector<double>& coords,
		   const double ei, 
		   const std::vector<int>& pnodes,
		   const double* datum);

    //! Destructor
    virtual ~TendonElastica();

    //! Disable copy and assignment
    TendonElastica(const TendonElastica&) = delete;
    TendonElastica& operator=(const TendonElastica&) = delete;

    //! Returns the element array
    inline const std::vector<Element*>& GetElementArray() const
    { return ElmArray; }

    //! Returns the local to global map
    inline const LocalToGlobalMap& GetLocalToGlobalMap() const
    { return *L2GMap; }

    //! Returns the number of loads
    inline int GetNumLoads() const
    { return nLoads; }

    //! Returns the coordinates array
    inline const std::vector<double>& GetCoordinates() const
    { return coordinates; }

    //! Returns the connectivity
    inline const std::vector<int>& GetConnectivity() const
    { return connectivity; }

    //! Returns the number of elements
    inline int GetNumElements() const
    { return nElements; }

    //! Returns the number of nodes
    inline int GetNumNodes() const
    { return nNodes; }

    //! Returns the state
    inline const std::vector<double>& GetStateField() const
    { return theta; }
    
    //! Set the state field
    inline void SetStateField(const double* vals)
    { const int nDof = L2GMap->GetTotalNumDof();
      for(int a=0; a<nDof; ++a)
	theta[a] = vals[a];
    }
    
    //! Returns the sensitivity wrt a given a load number
    inline const std::vector<double>& GetSensitivityField(const int loadnum) const
    { return alphaVec[loadnum]; }
    
    //! Returns the sensitivities wrt all loads
    inline const std::vector<std::vector<double>>& GetSensitivityFields() const
    { return alphaVec; }

    //! Access the nodal coordinates
    inline const std::vector<double>& GetCartesianCoordinates() const
    {
      if(state_is_dirty)
	ComputeCartesianCoordinates();
      return xy;
    }

    //! Access sensitivities of nodal coordinates
    inline const std::vector<double>& GetCoordinateSensitivity(const int LoadNum) const
    {
      if(state_is_dirty) ComputeCartesianCoordinates();
      if(sense_is_dirty) ComputeCartesianCoordinateSensitivities();
      return xy_sensitivity[LoadNum];
    }

    //! Access sensitivities of nodal coordinates
    inline const std::vector<std::vector<double>>& GetCoordinateSensitivity() const 
    {
      if(state_is_dirty) ComputeCartesianCoordinates();
      if(sense_is_dirty) ComputeCartesianCoordinateSensitivities();
      return xy_sensitivity;
    }

    //! Main functionality
      //! Computes the solution theta for a given set of loads
      //! \param[in] loads Set of loads
      //! \param[in] solve_params Parameters for the solution algorithm
      virtual void ComputeState(LoadParams& load_params,
				const std::map<int,double>& bc_params,
				const Solve_Params& solve_params);

    //! Computes the sensitivity at a given set of loads
    //! The state is recomputed.
    //! \param[in] loads Set of loads
    //! \param[in] solve_params Parameters for the solution algorithm
    virtual void ComputeStateAndSensitivities(LoadParams& load_params,
					      const std::map<int,double>& bc_params,
					      const Solve_Params& solve_params);

    //! Utility function that updates loading directions based on the current solution
    void UpdateTendonDirections(std::vector<LoadDirection*>& load_dirs) const;

    //! Utility function that updates moment functionals based on the currrent solution
    void UpdateMomentFunctionals(const std::vector<double>& load_values, std::vector<MomentFunctional*>& moments) const;
    
    //! Update loads in state operations
    void UpdateStateOperations(const LoadParams& load_params);

    //! Update loads in sensitivity operations
    void UpdateSensitivityOperations(const int loadnum,
				     const LoadParams& load_params) const;

    //! Consistency test for state calculations
    void StateConsistencyTest(const double* stateVals,
			      LoadParams& load_params,
			      const std::map<int,double>& bc_params);

    //! Consistency test for sensitivity calculations
    void SensitivityConsistencyTest(const int L,
				    const double* alphaVals,
				    LoadParams& load_params,
				    const std::map<int,double>& bc_params);

    //! Compute the strain energy of the elastica in its current configuration
    double GetStrainEnergy() const;

    //! Coordinates of the tip
    inline const double* GetTipCoordinates() const
    { assert(!state_is_dirty);
      return &xy[2*(nNodes-1)]; }
    
    //! Computes the sensitivity of the elastica tip wrt each load
    //! \param[out] xsense Computed sensitivities wrt each load
    //! \param[out] ysense Computed sensitivities wrt each load
    inline std::vector<std::array<double,2>> GetTipSensitivities() const
    { assert(!state_is_dirty);
      assert(!sense_is_dirty);
      std::vector<std::array<double,2>> xysense(nLoads);
      for(int L=0; L<nLoads; ++L)
	for(int k=0; k<2; ++k)
	  xysense[L][k] = xy_sensitivity[L][2*(nNodes-1)+k];
      return xysense;
    }

  protected:
    // Compute nodal Cartesian coordinates with the current state
    void ComputeCartesianCoordinates() const;

    // Compute sensitivities of Cartesian coordinates
    void ComputeCartesianCoordinateSensitivities() const;

    // Members
    const std::vector<double>& coordinates; //!< Reference to nodal coordinates
    std::vector<int> connectivity; //!< Element connectivities
    const double EI;
    const double origin[2];
    const int nNodes; //!< Number of nodes
    const int nElements; //!< Number of elements
    const int nLoads; //!< Number of loads
    std::vector<std::array<int,2>> Load2ElmIntervalMap; //!< Range of elements over which a given load is active
    std::vector<std::vector<double>> LoadCoeff; //!< Loading coefficients for element intervals
    int nElementSections; //!< Number of discrete sections of elements
    std::vector<std::array<int,2>> ElmIntervals; //!< Discrete sections of element intervals
    std::vector<Element*> ElmArray; //!< Element array
    LocalToGlobalMap* L2GMap; //!< Local to global map

    // For state calculations
    std::vector<Strut_State_Op*> state_OpsArray; //!< Operations to compute the state (Laplacian)
    StandardAssembler<Strut_State_Op>* state_Asm; //!< Assembler for OpsArray calculations
    std::vector<double> theta; //!< Theta field
    mutable std::vector<double> xy; //!< Nodal Cartesian coordinates
    
    // For sensitivity calculations
    std::vector<std::vector<double>> alphaVec; //!< Sensitivities wrt loads
    std::vector<Strut_Sensitivity_Op*> sense_OpsArray; //!< Operations for sensitivity calcs
    StandardAssembler<Strut_Sensitivity_Op>* sense_Asm; //!< Assmbler for sensitivity calcs
    mutable std::vector<std::vector<double>> xy_sensitivity; //!< Sensitivities of the nodal xy-coordinates, xy_sensitivity[L][2*n]

    
    SNESSolver theta_solver; //!< Nonlinear solver for the state
    SNESSolver alpha_solver; //!< Nonlinear solver for sensitivities

    // Monitor whether state and sensitivities are clean/dirty
    mutable bool state_is_dirty;
    mutable bool sense_is_dirty;

    //! Helper struct for communication with SNES residual and stiffness evaluation
    struct SNESContext
    {
      TendonElastica* elastica; //!< Pointer to the elastica
      LoadParams* load_params; //!< Pointer to load parameters
      const std::map<int,double>* bc_params; //!< Pointer to dirichlet bc parameters
      const Solve_Params* solve_params; //!< Solution parameters
      int LoadNum=-1; //!< Load number for which to compute sensitivities
      inline void Check() const
      { assert(elastica!=nullptr && load_params!=nullptr && bc_params!=nullptr);
	load_params->Check();
	assert(static_cast<int>(bc_params->size())>0 && static_cast<int>(bc_params->size())<=2);
	assert(LoadNum>=0 && LoadNum<static_cast<int>(load_params->values.size()));
      }
    };
    
    // Friends

    // Required for the state SNES solver
    friend PetscErrorCode State_Residual_Func(SNES, Vec, Vec, void*);
    friend void AppendConsistentLoadVariations(SNESContext&, Mat&);
    friend PetscErrorCode State_Jacobian_Func(SNES, Vec, Mat, Mat, void*);

    // Required for the sensitivity SNES solver
    friend PetscErrorCode Sensitivity_Residual_Func(SNES, Vec, Vec, void*);
    friend PetscErrorCode Sensitivity_Jacobian_Func(SNES, Vec, Mat, Mat, void*);
  };
}


#endif
