// Sriramajayam

#ifndef EL_TENDON_ELASTICA_STRUCTS_H
#define EL_TENDON_ELASTICA_STRUCTS_H

#include <cassert>
#include <el_LoadDirection.h>
#include <el_MomentFunctional.h>
#include <el_Utils.h>

namespace el
{
  //! Helper struct for algorithmic parameters
  struct Solve_Params
  {
    bool consistentLinearization; //!< Consistent linearization for computing the Jacobian matrix
    double EPS; //!< Absolute tolerance
    double resScale; //!< Scaling factor for convergence of residuals
    double dofScale; //!< Scaling factor for convergence of dofs
    int nMaxIters; //!< Max number of iterations
    bool verbose; //!< Print convergence details

    //! Sanity check on parameters
    inline void Check() const
    { assert(EPS>0. && resScale>0. && dofScale>0. && nMaxIters>0); }
  };

  
  // Loads
  struct LoadParams
  {
    std::vector<double> values; //!< Loading values
    std::vector<LoadDirection*> dirs; //!< Loading directions
    std::vector<MomentFunctional*> moments; //!< Moment functionals
    
    //!< Sanity checks
    inline void Check() const
    { const int nLoads = static_cast<int>(values.size());
      assert(static_cast<int>(dirs.size())==nLoads);
      for(int L=0; L<nLoads; ++L)
	{ assert(dirs[L]!=nullptr);
	  assert(dirs[L]->GetLoadNumber()==L);
	  assert(dirs[L]->GetNumLoads()==nLoads); }
      return; }
    };
}

#endif
