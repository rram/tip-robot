// Sriramajayam

#ifndef EL_TENDON_ELASTICA_SENSE_SNES_FUNCS_H
#define EL_TENDON_ELASTICA_SENSE_SNES_FUNCS_H

#include <SNES_Solver.h>
#include <el_Strut_Sensitivity_Op.h>
#include <el_TendonElastica_State_SNESFuncs.h>
#include <cassert>
#include <iostream>

namespace el
{
  // Add contributions to the residual from moment contributions
  inline PetscErrorCode Sensitivity_Residual_Moment_Add(const int LoadNum,
							const std::vector<MomentFunctional*>& moments,
							Vec& resVec)
  {
    const int nMoments = static_cast<int>(moments.size());
    if(nMoments==0) return 0;
      
    assert(LoadNum<nMoments);
    PetscErrorCode ierr;
    for(int m=0; m<nMoments; ++m)
      {
	const auto& moment = *moments[m];

	// Map from node number to sensitivities
	const auto& msense_map = moment.GetSensitivities();
	for(auto& it:msense_map)
	  { // Node number to which this moment is attached
	    const int mnode = it.first;

	    // Sensitivities wrt all loads
	    const auto& msenses = it.second;
	      
	    // Sensitivity of this moment wrt the specified load
	    const double& senseVal = msenses[LoadNum];
	      
	    // Update the residual
	    ierr = VecSetValues(resVec, 1, &mnode, &senseVal, ADD_VALUES); CHKERRQ(ierr);
	  }
      }
      
    // Finish up assembly
    ierr = VecAssemblyBegin(resVec); CHKERRQ(ierr);
    ierr = VecAssemblyEnd(resVec); CHKERRQ(ierr);
      
    // -- done --
    return 0;
  }
      
      
  // Function for evaluating sensitivity residuals
  // Assumes that dirichlet BCs have been set in th configuration
  inline PetscErrorCode Sensitivity_Residual_Func(SNES snes, Vec solVec, Vec resVec, void* usr)
  {
    // Get the context
    TendonElastica::SNESContext* ctx;
    PetscErrorCode ierr = SNESGetApplicationContext(snes, &ctx); CHKERRQ(ierr);
    assert(ctx!=nullptr && "el::Sensitivity_Residual_Func- context is null");
    ctx->Check();       // Sanity checks
    auto& elastica = *ctx->elastica;
    auto& load_params = *ctx->load_params;
    auto& bc_params = *ctx->bc_params;
    const auto& LoadNum = ctx->LoadNum; // Load number for which to compute sensitivity
    const auto& theta = elastica.theta;
    auto& alpha = elastica.alphaVec[LoadNum];
    auto& Asm = *elastica.sense_Asm;
    const auto& L2GMap = *elastica.L2GMap;
      
    // Set the sensitivity values at which to evaluate the residual
    int nDofs;
    ierr = VecGetSize(solVec, &nDofs); CHKERRQ(ierr);
    for(int n=0; n<nDofs; ++n)
      { ierr = VecGetValues(solVec, 1, &n, &alpha[n]); CHKERRQ(ierr); }

    // Update tendon directions
    elastica.UpdateTendonDirections(load_params.dirs);

    // Update moment functionals
    elastica.UpdateMomentFunctionals(load_params.values, load_params.moments);
      
    // Update loads in sensitivity operations
    elastica.UpdateSensitivityOperations(LoadNum, load_params);

    // Configuration at which to evaluate the residual
    SensitivityConfiguration config;
    config.state = &theta;
    config.sensitivity = &alpha;
    config.L2GMap = &L2GMap;
      
    // Assemble
    Asm.Assemble(&config, resVec);

    // Add contributions from moments
    ierr = Sensitivity_Residual_Moment_Add(LoadNum, load_params.moments, resVec); CHKERRQ(ierr);
      
    // Set Dirichlet dofs
    for(auto& it:bc_params)
      { double zero = 0.;
	ierr = VecSetValues(resVec, 1, &it.first, &zero, INSERT_VALUES); CHKERRQ(ierr); }
    ierr = VecAssemblyBegin(resVec); CHKERRQ(ierr);
    ierr = VecAssemblyEnd(resVec); CHKERRQ(ierr);
      
    // -- done--
    return 0;
  }

    
  // Function for evaluating sensitivity Jacobian
  inline PetscErrorCode Sensitivity_Jacobian_Func(SNES snes, Vec solVec, Mat kMat, Mat pMat, void* usr)
  {
    // Get the context
    TendonElastica::SNESContext *ctx;
    PetscErrorCode ierr = SNESGetApplicationContext(snes, &ctx);
    assert(ctx!=nullptr && "el::Sensitivity_Jacobian_Func- context is null");
    ctx->Check();       // Sanity checks
    auto& elastica = *ctx->elastica;
    auto& load_params = *ctx->load_params;
    auto& bc_params = *ctx->bc_params;
    auto& solve_params = *ctx->solve_params;
    const auto& LoadNum = ctx->LoadNum;
    const auto& theta = elastica.theta;
    auto& alpha = elastica.alphaVec[LoadNum];
    auto& Asm = *elastica.sense_Asm;
    auto& tempVec = elastica.alpha_solver.tempVec;
    const auto& L2GMap = *elastica.L2GMap;

    // Set the sensitivity values at which to evaluate the residual
    int nDofs;
    ierr = VecGetSize(solVec, &nDofs); CHKERRQ(ierr);
    for(int n=0; n<nDofs; ++n)
      { ierr = VecGetValues(solVec, 1, &n, &alpha[n]); CHKERRQ(ierr); }
      
    // Update loading direction based on the current solution guess
    elastica.UpdateTendonDirections(load_params.dirs);

    // Update loads in sensitivity operations
    elastica.UpdateSensitivityOperations(LoadNum, load_params);
      
    // Configuration at which to evaluate the residual
    SensitivityConfiguration config;
    config.state = &theta;
    config.sensitivity = &alpha;
    config.L2GMap = &L2GMap;

    // Assemble
    Asm.Assemble(&config, tempVec, kMat);

    // Add contributions from directional derivatives of follower loads
    // These contributions are identical from the state matrix. Reuse the routines
    if(solve_params.consistentLinearization==true)
      {
	AppendConsistentLoadVariations(*ctx, kMat);
	AppendConsistentMomentVariations(load_params.moments, kMat);
      }
      
    // Set Dirichlet bcs
    for(auto& it:bc_params)
      { ierr = MatZeroRows(kMat, 1, &it.first, 1., PETSC_NULLPTR, PETSC_NULLPTR);
	CHKERRQ(ierr); }
    ierr = MatAssemblyBegin(kMat, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
    ierr = MatAssemblyEnd(kMat, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);

    // -- done --
    return 0;
  }

}


#endif
