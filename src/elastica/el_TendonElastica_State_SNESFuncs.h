// Sriramajayam

#ifndef EL_TENDON_ELASTICA_STATE_SNES_FUNCS_H
#define EL_TENDON_ELASTICA_STATE_SNES_FUNCS_H

#include <SNES_Solver.h>
#include <el_Strut_State_Op.h>
#include <el_TendonElastica_Structs.h>
#include <cassert>
#include <iostream>

namespace el
{
  // Add moment contributions to the residual
  inline PetscErrorCode State_Residual_Moment_Add(const std::vector<MomentFunctional*>& moments, Vec& resVec)
  {
    // Loop over moments, update the residual at the loaded nodes
    PetscErrorCode ierr;
    const int nMoments = static_cast<int>(moments.size());
    for(int m=0; m<nMoments; ++m)
      { assert(moments[m]!=nullptr && "el::State_Residual_Moment_Add- moment functional is null");
	const auto& NodeValuePairs = moments[m]->GetFunctional();
	for(auto& it:NodeValuePairs)
	  { const int mnode = it.first;
	    const double mval = it.second;
	    ierr = VecSetValues(resVec, 1, &mnode, &mval, ADD_VALUES); CHKERRQ(ierr); } }
      
    // Finish up assembly
    ierr = VecAssemblyBegin(resVec); CHKERRQ(ierr);
    ierr = VecAssemblyEnd(resVec); CHKERRQ(ierr);
      
    return 0;
  }
    
  // Assemble the residual for the state
  // Assumes that the Dirichlet BCs have been set in the configuration
  inline PetscErrorCode State_Residual_Func(SNES snes, Vec solVec, Vec resVec, void* usr)
  {
    // Get the context
    TendonElastica::SNESContext* ctx;
    PetscErrorCode ierr = SNESGetApplicationContext(snes, &ctx); CHKERRQ(ierr);
    assert(ctx!=nullptr && "el::State_Residual_Func- context is null");

    // Access
    auto& elastica = *ctx->elastica;
    auto& load_params = *ctx->load_params;
    auto& bc_params = *ctx->bc_params;
    load_params.Check();
    assert(static_cast<int>(bc_params.size())>0 && static_cast<int>(bc_params.size())<=2);
    auto& theta = elastica.theta;
    auto& Asm = *elastica.state_Asm;
    auto& L2GMap = *elastica.L2GMap;

    // Get the configuration at which to evaluate the residual (PETSc guess)
    int nDofs;
    ierr = VecGetSize(solVec, &nDofs); CHKERRQ(ierr);
    for(int n=0; n<nDofs; ++n)
      { ierr = VecGetValues(solVec, 1, &n, &theta[n]);
	CHKERRQ(ierr); }
      
    // Update loading direction based on the current solution guess
    elastica.UpdateTendonDirections(load_params.dirs);

    // Update moment functionals based on the current solution guess
    elastica.UpdateMomentFunctionals(load_params.values, load_params.moments);
      
    // Update loads in state operations
    elastica.UpdateStateOperations(load_params);

    // Assemble the residual
    StateConfiguration config;
    config.state = &theta;
    config.L2GMap = &L2GMap;
    Asm.Assemble(&config, resVec);

    // Add contributions from moments
    ierr = State_Residual_Moment_Add(load_params.moments, resVec); CHKERRQ(ierr);
      
    // Set dirichlet BCs into the residual
    for(auto& it:bc_params)
      { double zero = 0.;
	ierr = VecSetValues(resVec, 1, &it.first, &zero, INSERT_VALUES); CHKERRQ(ierr); }
    ierr = VecAssemblyBegin(resVec); CHKERRQ(ierr);
    ierr = VecAssemblyEnd(resVec); CHKERRQ(ierr);
      
    // -- done --
    return 0;
  }


  // Append moment variation contributions to the stiffness matrix: <dM,Nb>
  inline void AppendConsistentMomentVariations(const std::vector<MomentFunctional*>& moments, Mat& kMat)
  {
    // Allow for new nonzeros
    PetscErrorCode ierr = MatSetOption(kMat, MAT_NEW_NONZERO_ALLOCATION_ERR, PETSC_FALSE); CHKERRV(ierr);

    // Update the stiffness matrix
    // Kab += <var_Ma[theta],Nb>
    const int nMoments = static_cast<int>(moments.size());
    for(int m=0; m<nMoments; ++m)
      {
	const auto& moment = *moments[m];

	// Get the directional derivatives and their indices
	const auto& dir_deriv_map = moment.GetDirectionalDerivatives();
	const auto& dir_deriv_index_map = moment.GetDirectionalDerivativeIndices();
	assert(dir_deriv_map.size()==dir_deriv_index_map.size());
	for(auto& it:dir_deriv_map)
	  { const int a = it.first;
	    const std::vector<double>& var_M = it.second;
	    std::map<int,std::vector<int>>::const_iterator jt = dir_deriv_index_map.find(a);
	    assert(jt!=dir_deriv_index_map.end());
	    const std::vector<int>& b_indices = jt->second;
	    const int ncols = static_cast<int>(b_indices.size());
	    ierr = MatSetValues(kMat, 1, &a, ncols, &b_indices[0], &var_M[0], ADD_VALUES); CHKERRV(ierr);
	  }
      }
      
    // Finish up assembly
    ierr = MatAssemblyBegin(kMat, MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
    ierr = MatAssemblyEnd(kMat, MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
      
    // -- done --
    return;
  }

    
  // Append load variation contributions to the stiffness matrix
  // <dH,Nb> Int(sin(theta) Na) - <dV,Nb> Int(cos(theta) Na)
  inline void AppendConsistentLoadVariations(TendonElastica::SNESContext& ctx, Mat& kMat)
  {
    // Allow for new nonzeros
    PetscErrorCode ierr = MatSetOption(kMat, MAT_NEW_NONZERO_ALLOCATION_ERR, PETSC_FALSE);
    CHKERRV(ierr);
	
    // Unpackage internals
    auto& elastica = *ctx.elastica;
    auto& load_params = *ctx.load_params;
    const auto& ElmArray = elastica.ElmArray;
    const auto& L2GMap = *elastica.L2GMap;
    const int& nLoads = elastica.nLoads;
    const auto& theta = elastica.theta;
    const int ndof_per_elm = L2GMap.GetNumDofs(0,0);
    const auto& loads = load_params.values;
    const auto& dirs = load_params.dirs;
      
    // Update the stiffness matrix:
    // Kab += <P_L var_Hval_L, Nb> Integral(sin(theta) Na)     // sum over L implied
    //      - <P_L var_Vval_L, Nb> Integral(cos(theta) Na)
    // The vectors
    // X_b = <P_L var_Hval_L, Nb>.. and Y_a = Integral(sin(theta) Na)
    // are preassembled. Simply perform the tensor product and add to K.

    // Restrict the load P to just the regions where it has support
    // This is equivalent to restricting the index 'a' when updating the stiffness matrix.
    double delta_Kab;
    double thetaval;
    std::vector<double> int_PsinNa(ndof_per_elm);
    std::vector<double> int_PcosNa(ndof_per_elm);
    for(int L=0; L<nLoads; ++L)
      {
	// This load P and the variation of its direction (Hval, Vval)
	const double& P = loads[L];
	const auto& var_Hval = dirs[L]->GetDirectionalDerivatives(DirCosine::Horizontal);
	const auto& var_Vval = dirs[L]->GetDirectionalDerivatives(DirCosine::Vertical);
	const auto& var_indx = dirs[L]->GetDirectionalDerivativeIndices();
	const auto& ElmIntervals = dirs[L]->GetElementIntervals();

	// Sanity checks
	const int nIntervals = static_cast<int>(ElmIntervals.size());
	assert(nIntervals==1);
	const int nVars = static_cast<int>(var_indx.size());
	assert(static_cast<int>(var_Hval.size())==nVars &&
	       static_cast<int>(var_Vval.size())==nVars);
	      
	// Integration interval
	const int estart = ElmIntervals[0].first;
	const int estop  = ElmIntervals[0].second;

	for(int e=estart; e<=estop; ++e)
	  {
	    // This element's data
	    const auto& Elm = *ElmArray[e];
	    const auto& Qwts = Elm.GetIntegrationWeights(0);
	    const int nQuad = static_cast<int>(Qwts.size());
	    const auto& Shapes = Elm.GetShape(0);
		  
	    // Update integrals: P*sin(theta)*Na and P*cos(theta)*Na
	    std::fill(int_PsinNa.begin(), int_PsinNa.end(), 0.);
	    std::fill(int_PcosNa.begin(), int_PcosNa.end(), 0.);
	    for(int q=0; q<nQuad; ++q)
	      {
		// Shape function values at this quadrature point
		const double* qShapes = &Shapes[q*ndof_per_elm];
		      
		// Evaluate theta here
		thetaval = 0.;
		for(int a=0; a<ndof_per_elm; ++a)
		  thetaval += theta[L2GMap.Map(0,a,e)]*qShapes[a];
		      
		// Update integrals
		for(int a=0; a<ndof_per_elm; ++a)
		  { int_PsinNa[a] += Qwts[q]*P*std::sin(thetaval)*qShapes[a];
		    int_PcosNa[a] += Qwts[q]*P*std::cos(thetaval)*qShapes[a]; }
	      }
		  
	    // Update the contribution from this element to the stiffness matrix
	    for(int a=0; a<ndof_per_elm; ++a)
	      {
		const int aindx = L2GMap.Map(0,a,e);
		for(int b=0; b<nVars; ++b)
		  {
		    const int bindx = var_indx[b];
		    delta_Kab = (var_Hval[b]*int_PsinNa[a]-var_Vval[b]*int_PcosNa[a]);
			  
		    // Update stiffness matrix entry (aindx, bindx)
		    ierr= MatSetValues(kMat, 1, &aindx, 1, &bindx, &delta_Kab, ADD_VALUES);
		    CHKERRV(ierr);
		  }
	      }
	  }

      }
      
    // Finish up assembly
    ierr = MatAssemblyBegin(kMat, MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
    ierr = MatAssemblyEnd(kMat, MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
      
    // done
    return;
  }

    
    
  // Function for evaluating the Jacobian for the state
  inline PetscErrorCode State_Jacobian_Func(SNES snes, Vec solVec, Mat kMat, Mat pMat, void* usr)
  {
    // Get the context
    TendonElastica::SNESContext* ctx;
    PetscErrorCode ierr = SNESGetApplicationContext(snes, &ctx); CHKERRQ(ierr);
    assert(ctx!=nullptr && "el::State_Jacobian_Func- context is null");

    // Access
    auto& elastica = *ctx->elastica;
    auto& load_params = *ctx->load_params;
    auto& bc_params = *ctx->bc_params;
    auto& solve_params = *ctx->solve_params;
    load_params.Check();
    assert(static_cast<int>(bc_params.size())>0 && static_cast<int>(bc_params.size())<=2);
    auto& theta = elastica.theta;
    auto& Asm = *elastica.state_Asm;
    auto& L2GMap = *elastica.L2GMap;
    auto& tempVec = elastica.theta_solver.tempVec;

    // Get the configuration at which to evaluate the residual (PETSc guess)
    int nDofs;
    ierr = VecGetSize(solVec, &nDofs); CHKERRQ(ierr);
    for(int n=0; n<nDofs; ++n)
      { ierr = VecGetValues(solVec, 1, &n, &theta[n]); CHKERRQ(ierr); }
      
    // Update loading direction based on the current solution guess
    elastica.UpdateTendonDirections(load_params.dirs);

    // Update moment functionals based on the current solution guess
    elastica.UpdateMomentFunctionals(load_params.values, load_params.moments);
      
    // Update state operations with new loading directions
    elastica.UpdateStateOperations(load_params);

    // Assemble
    StateConfiguration config;
    config.state = &theta;
    config.L2GMap = &L2GMap;
    Asm.Assemble(&config, tempVec, kMat);

    // Add contribution from directional derivatives of follower loads and moments
    if(solve_params.consistentLinearization==true)
      { AppendConsistentLoadVariations(*ctx, kMat);
	AppendConsistentMomentVariations(load_params.moments, kMat); }
      
    // Set Dirichlet dofs
    for(auto& it:bc_params)
      { ierr = MatZeroRows(kMat, 1, &it.first, 1., PETSC_NULLPTR, PETSC_NULLPTR);
	CHKERRQ(ierr); }
    ierr = MatAssemblyBegin(kMat, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
    ierr = MatAssemblyEnd(kMat, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
      
    // -- done --
    return 0;
  }
}

#endif
