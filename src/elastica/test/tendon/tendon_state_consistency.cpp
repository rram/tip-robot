// Sriramajayam

#include <el_TendonElastica.h>
#include <P11DElement.h>
#include <el_TendonLoadDirection.h>
#include <iostream>
#include <fstream>
#include <random>

int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULLPTR, PETSC_NULLPTR);

  // Create a uniform 1D mesh on the interval [0,1]
  const int nNodes = 10;
  const double h = 1./static_cast<double>(nNodes-1);
  std::vector<double> coordinates(nNodes);
  for(int n=0; n<nNodes; ++n)
    coordinates[n] = 0.+h*static_cast<double>(n);

  // Create strut
  const int nLoads = 2;
  std::vector<int> PNodes({nNodes/2, nNodes-1});
  const double EI = std::sqrt(5.);
  const double origin[] = {0.,0.};
  el::TendonElastica *str = new el::TendonElastica(coordinates, EI, PNodes, origin);

  // Generate a random values for state/loads/directions
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_real_distribution<> dis(-2.,2.);

  // Create routing post
  el::RoutingPost RtPost;
  RtPost.center[0] = 0.3; RtPost.center[1] = 0.4;
  RtPost.radius = 0.;
  RtPost.orientation = 1.;
    
  // Create 1 tendon load
  el::LoadParams load_params;
  load_params.values.resize(nLoads);
  load_params.dirs.resize(nLoads);
  for(int i=0; i<2; ++i)
    { load_params.dirs[i] = new el::TendonLoadDirection(PNodes[i],i,*str,RtPost);
      load_params.values[i] = dis(gen); }
  
  // Dirichlet BCs
  std::map<int,double> bc_params{};
  bc_params[0] = 0.;

  // Generate random values for the state
  std::vector<double> theta(nNodes);
  for(int i=0; i<nNodes; ++i)
    theta[i] = dis(gen);
  
  // Do a consistency test
  str->StateConsistencyTest(&theta[0], load_params, bc_params);

  // Clean up
  delete str;
  for(auto& x:load_params.dirs) delete x;
  PetscFinalize();
}
