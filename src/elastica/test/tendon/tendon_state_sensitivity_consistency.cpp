// Sriramajayam

#include <el_TendonElastica.h>
#include <P11DElement.h>
#include <el_TendonLoadDirection.h>
#include <iostream>
#include <fstream>
#include <random>

int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULLPTR, PETSC_NULLPTR);

  // Create a uniform 1D mesh on the interval [0,1]
  const int nNodes = 20;
  const double h = 1./static_cast<double>(nNodes-1);
  std::vector<double> coordinates(nNodes);
  for(int n=0; n<nNodes; ++n)
    coordinates[n] = 0.+h*static_cast<double>(n);

  // Create strut
  const int nLoads = 2;
  std::vector<int> PNodes({nNodes/2,nNodes-1});
  const double EI = 1.;
  const double origin[] = {0.,0.};
  el::TendonElastica *str = new el::TendonElastica(coordinates, EI, PNodes, origin);

  // Generate a random values for state/loads/directions
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_real_distribution<> dis(-2.,2.);
  
  // Create 2 tendon loads
  el::LoadParams load_params;
  load_params.values.resize(nLoads);
  load_params.values[0] = dis(gen);
  load_params.values[1] = dis(gen);
  load_params.dirs.resize(nLoads);
  el::RoutingPost RtPost0; RtPost0.center[0]=0.; RtPost0.center[1]=0.1; RtPost0.radius=0.; RtPost0.orientation=1.;
  el::RoutingPost RtPost1; RtPost1.center[0]=0.; RtPost1.center[1]=-0.25; RtPost1.radius=0.; RtPost1.orientation=-1.;
  //const double RtPoint0[] = {0.,0.1};
  //const double RtPoint1[] = {0.,-0.25};
  load_params.dirs[0] = new el::TendonLoadDirection(PNodes[0],0,*str,RtPost0);
  load_params.dirs[1] = new el::TendonLoadDirection(PNodes[1],1,*str,RtPost1);

  // Dirichlet BCs
  std::map<int,double> bc_params{};
  bc_params[0] = 0.;
  
  // Solve parameters
  el::Solve_Params solve_params
    ({.consistentLinearization=false,
	.EPS=1.e-10,
	.resScale=1.,
	.dofScale=1.,
	.nMaxIters=25,
	.verbose=true});

  // Compute the sensivities at the given loads
  str->ComputeStateAndSensitivities(load_params, bc_params, solve_params);
  const auto& alphaField1 = str->GetSensitivityField(0);
  const auto& alphaField2 = str->GetSensitivityField(1);
  std::vector<double> alpha1(nNodes), alpha2(nNodes);
  for(int i=0; i<nNodes; ++i)
    { alpha1[i] = alphaField1[i];
      alpha2[i] = alphaField2[i]; }
  std::vector<std::vector<double>> xy_sense = str->GetCoordinateSensitivity();
  
  // Compute the state at P0 +/- epsilon * numerical difference
  std::vector<double> alpha1Num(nNodes);
  std::vector<double> xy1Num(2*nNodes);
  const double P_EPS = 1.e-4;
  load_params.values[0] += P_EPS;
  str->ComputeState(load_params, bc_params, solve_params);
  const auto& thetaField = str->GetStateField();
  std::vector<double> xy_plus = str->GetCartesianCoordinates();
  for(int i=0; i<nNodes; ++i)
    alpha1Num[i] = thetaField[i];
  load_params.values[0] -= 2.*P_EPS;
  str->ComputeState(load_params, bc_params, solve_params);
  std::vector<double> xy_minus = str->GetCartesianCoordinates();
  for(int i=0; i<nNodes; ++i)
    { alpha1Num[i] -= thetaField[i];
      alpha1Num[i] /= (2.*P_EPS);
      xy1Num[2*i] = (xy_plus[2*i]-xy_minus[2*i])/(2.*P_EPS);
      xy1Num[2*i+1] = (xy_plus[2*i+1]-xy_minus[2*i+1])/(2.*P_EPS);
    }
  // Restore the original load
  load_params.values[0] += P_EPS;

  // Compute the state at P1 +/- epsilon & numerical difference
  std::vector<double> alpha2Num(nNodes);
  std::vector<double> xy2Num(2*nNodes);
  load_params.values[1] += P_EPS;
  str->ComputeState(load_params, bc_params, solve_params);
  xy_plus = str->GetCartesianCoordinates();
  for(int i=0; i<nNodes; ++i)
    alpha2Num[i] = thetaField[i];
  load_params.values[1] -= 2.*P_EPS;
  str->ComputeState(load_params, bc_params, solve_params);
  xy_minus = str->GetCartesianCoordinates();
  for(int i=0; i<nNodes; ++i)
    { alpha2Num[i] -= thetaField[i];
      alpha2Num[i] /= (2.*P_EPS);
      xy2Num[2*i] = (xy_plus[2*i]-xy_minus[2*i])/(2.*P_EPS);
      xy2Num[2*i+1] = (xy_plus[2*i+1]-xy_minus[2*i+1])/(2.*P_EPS);
    }
  // Restore the original load
  load_params.values[1] += P_EPS;

  for(int i=0; i<nNodes; ++i)
    {
      assert(std::abs(alpha1[i]-alpha1Num[i])<1.e-3);
      assert(std::abs(alpha2[i]-alpha2Num[i])<1.e-3);
      assert(std::abs(xy_sense[0][2*i]-xy1Num[2*i])<1.e-3);
      assert(std::abs(xy_sense[0][2*i+1]-xy1Num[2*i+1])<1.e-3);
      assert(std::abs(xy_sense[1][2*i]-xy2Num[2*i])<1.e-3);
      assert(std::abs(xy_sense[1][2*i+1]-xy2Num[2*i+1])<1.e-3);
    }
  
  // Plot the computed and numerical sensitivities
  std::fstream pfile;
  pfile.open((char*)"ss.dat", std::ios::out);
  for(int i=0; i<nNodes; ++i)
    pfile << coordinates[i]<<" "<<alpha1[i]<<" "<<alpha1Num[i]<<" "<<alpha2[i]<<" "<<alpha2Num[i]<<"\n";
  pfile.flush(); pfile.close();

  pfile.open((char*)"xy.dat", std::ios::out);
  for(int i=0; i<nNodes; ++i)
    pfile << coordinates[i]
	  <<" "<<xy_sense[0][2*i]<<" "<<xy1Num[2*i]
      	  <<" "<<xy_sense[0][2*i+1]<<" "<<xy1Num[2*i+1]
      	  <<" "<<xy_sense[1][2*i]<<" "<<xy2Num[2*i]
      	  <<" "<<xy_sense[1][2*i+1]<<" "<<xy2Num[2*i+1]<<"\n";
  pfile.flush(); pfile.close();
  
  // Clean up
  delete str;
  for(auto& x:load_params.dirs) delete x;
  PetscFinalize();
}
