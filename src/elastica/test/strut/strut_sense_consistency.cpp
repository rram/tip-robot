// Sriramajayam

#include <el_TendonElastica.h>
#include <P11DElement.h>
#include <el_ConstantLoadDirection.h>
#include <iostream>
#include <fstream>
#include <random>

int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULLPTR, PETSC_NULLPTR);

  // Create a uniform 1D mesh on the interval [0,1]
  const int nNodes = 10;
  const double h = 1./static_cast<double>(nNodes-1);
  std::vector<double> coordinates(nNodes);
  for(int n=0; n<nNodes; ++n)
    coordinates[n] = 0.+h*static_cast<double>(n);

  // Create strut
  const int nLoads = 2;
  std::vector<int> PNodes({nNodes/2,nNodes-1});
  const double EI = std::sqrt(5.);
  const double origin[] = {0.,0.};
  el::TendonElastica *str = new el::TendonElastica(coordinates, EI, PNodes, origin);

  // Generate a random values for state/sensitivities/loads/directions
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_real_distribution<> dis(-2.,2.);
  
  // Create 2 loads at fixed angles
  const double PI = 4.*std::atan(1.);
  const double theta0 = PI*dis(gen);
  const double theta1 = PI*dis(gen);
  const double dir0[] = {std::cos(theta0), std::sin(theta0)};
  const double dir1[] = {std::cos(theta1), std::sin(theta1)};
  el::LoadParams load_params;
  load_params.values.resize(nLoads);
  load_params.values[0] = dis(gen);
  load_params.values[1] = dis(gen);
  load_params.dirs.resize(nLoads);
  load_params.dirs[0] = new el::ConstantLoadDirection(PNodes[0], 0, *str, dir0);
  load_params.dirs[1] = new el::ConstantLoadDirection(PNodes[1], 1, *str, dir1);
  
  // Dirichlet BCs
  std::map<int,double> bc_params{};
  bc_params[0] = 0.;
  
  // Set random values for the state
  std::vector<double> theta(nNodes);
  for(int i=0; i<nNodes; ++i)
    theta[i] = dis(gen);
  str->SetStateField(&theta[0]);

  // Do a consistency test for the L-th sensitivity
  for(int L=0; L<nLoads; ++L)
    {
      // random values for sensitivities
      std::vector<double> alpha(nNodes);
      for(int i=0; i<nNodes; ++i)
	alpha[i] = dis(gen);

      // Check consistency
      str->SensitivityConsistencyTest(L, &alpha[0], load_params, bc_params);
    }

  // Clean up
  delete str;
  for(auto& x:load_params.dirs) delete x;
  PetscFinalize();
}
