(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[         0,          0]
NotebookDataLength[     13170,        317]
NotebookOptionsPosition[     12461,        286]
NotebookOutlinePosition[     12849,        303]
CellTagsIndexPosition[     12806,        300]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[
 RowBox[{"(*", " ", "Sriramajayam", " ", "*)"}]], "Input",
 CellChangeTimes->{{3.7397780212575417`*^9, 3.73977802594697*^9}}],

Cell[BoxData[{
 RowBox[{"Clear", "[", "\"\<Global`*\>\"", "]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"\[Psi]", "=", " ", 
   RowBox[{"60", " ", "Degree"}]}], ";", " ", 
  RowBox[{"(*", 
   RowBox[{
   "Angle", " ", "of", " ", "load", " ", "acting", " ", "on", " ", "end", " ",
     "wrt", " ", "horizontal"}], "*)"}], "\[IndentingNewLine]", 
  RowBox[{"L", "=", "1"}], ";"}], "\n", 
 RowBox[{
  RowBox[{"P", "=", 
   RowBox[{"Sqrt", "[", "2", "]"}]}], ";"}]}], "Input",
 CellChangeTimes->{{3.7397780266945457`*^9, 3.739778114718504*^9}, {
  3.7397781495998173`*^9, 3.73977815136539*^9}, {3.739778184906849*^9, 
  3.7397782132089453`*^9}, {3.739778247458004*^9, 3.739778252823863*^9}, {
  3.739778308057176*^9, 3.739778402404323*^9}, {3.739778503642188*^9, 
  3.739778505155094*^9}, {3.739778874744095*^9, 3.73977887484767*^9}, {
  3.739779431172545*^9, 3.7397794314406643`*^9}, {3.7562766588786*^9, 
  3.756276658942227*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"(*", " ", "State", " ", "*)"}], "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{"angP", "=", 
     RowBox[{"\[Theta]", "/.", 
      RowBox[{"First", "[", 
       RowBox[{"NDSolve", "[", 
        RowBox[{
         RowBox[{"{", 
          RowBox[{
           RowBox[{
            RowBox[{
             RowBox[{
              RowBox[{"\[Theta]", "''"}], "[", "s", "]"}], "+", 
             RowBox[{"P", " ", 
              RowBox[{"Sin", "[", 
               RowBox[{"\[Psi]", "-", 
                RowBox[{"\[Theta]", "[", "s", "]"}]}], "]"}]}]}], "\[Equal]", 
            "0"}], ",", 
           RowBox[{
            RowBox[{"\[Theta]", "[", "0", "]"}], "\[Equal]", "0"}], ",", 
           RowBox[{
            RowBox[{
             RowBox[{"\[Theta]", "'"}], "[", "L", "]"}], "\[Equal]", "0"}]}], 
          "}"}], ",", "\[Theta]", ",", 
         RowBox[{"{", 
          RowBox[{"s", ",", "0", ",", "L"}], "}"}]}], "]"}], "]"}]}]}], ";"}],
    "\[IndentingNewLine]", 
   RowBox[{"Plot", "[", 
    RowBox[{
     RowBox[{"angP", "[", "s", "]"}], ",", 
     RowBox[{"{", 
      RowBox[{"s", ",", "0", ",", "L"}], "}"}]}], "]"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"Export", "[", 
     RowBox[{"\"\<theta.dat\>\"", ",", 
      RowBox[{"Table", "[", 
       RowBox[{
        RowBox[{"{", 
         RowBox[{"s", ",", 
          RowBox[{"angP", "[", "s", "]"}]}], "}"}], ",", 
        RowBox[{"{", 
         RowBox[{"s", ",", "0", ",", "1", ",", "0.01"}], "}"}]}], "]"}]}], 
     "]"}], ";"}], "\[IndentingNewLine]"}]}]], "Input",
 CellChangeTimes->{{3.739778403233482*^9, 3.7397784038599586`*^9}, {
  3.739778448935717*^9, 3.739778451890539*^9}}],

Cell[BoxData[
 GraphicsBox[{{}, {}, 
   {Hue[0.67, 0.6, 0.6], LineBox[CompressedData["
1:eJwV0nk019kbB3BbRfYlhsjIEpVIMi3GeyoJRUlSskeptFEjlYowsozGj8ZP
kka08i1rDbKWRJbsvh9LZMv3+7mMShTmzh/33PM6zznPfZ7zvhqeJ3d7CwkI
CNyi57/b2nu4sXTEwUygIUD+l6E9ZiJbVE71KBtDoMtLJGPFGqy5nZico2yO
J42nnycs34Ltb9uybynvgYRg0vckXXs4ek0Wxyl7IVEzoeyZ1kF0Gfy2JUb5
DPY7H188o+qP1EU7wyOVQ6F/9OxArORVBIvmLLumHI/2C/m7a9k47Nt/hzM2
fhcHFk8YR51Ng/hcsPO2JRzMvBevEYp8DG07drgiKhdb45Iz8pOyEVC3Homf
nuPK8SCPBP0CKOXKdgsKlmBCJ+FejX0h8vu21NabliNEUSJOLrsE5UdvLd7P
q4TsohH7GH4ZPuv8LJ15vAp1YrWnq+srIDT1SGax9hvMZitkKda8hMmce759
Vi3CPALFjW5UoTbUuO7V8np8jZp62GldDfXro1MP2Ab4OcnOvV5Ug9HJnu4W
w3cQbj/gb5pbi7sfC99I/d4EsQhblXuH6iCldo98+NAMI//Ot9qCDYg+knco
VasVyZ7PnH//uwFffnn0wedyG1LXuGUmXmzEGaPKXMWKdnQcapXQW/EOPVmV
UY0anTBWZAQ2v3+Hb0ebb+qd5yJLPXv9t+gmrNi0YPZQIYNQq6hL/+g0Y9tf
GoefKnWjWXzvU52qZgSouS1979yD2Y+eLwL3tmAlz0G8zaYXucHNMgZdLZCX
HhOtnO4FG7vsW79PK0RKbnI0k9+jLqKy1HKgFceMCzvEtvehq+h+WLp3Gx5Z
eaa++NIH9x4tpdVdbfg8qiIzeKMfKZn9dV8s2pFnv0043vwDiv9Izz6X047L
lmG3cwc/4PE/w3Izch3Q6LJuTwsbwI3wWzabjnVgw0+h8cpGg6i+V6Us87ID
zkvqpwSbBjHiH25qLd+Jz29i+k4FD6F6m9X/ZDw7kSjiema31jBOWrT0zqZ3
Qr477y+hymHYDUyE7u/vxLyg4D3rT4/gmGGAvfSPXAg8cOX0yX3EZt7Ngi32
XPglcWLkyj8itc5ctSWUi+IVEaKyJ0ZRY/Su6jcOF7LWfitDpHjgb/6jX+Id
F6ll0uIbZHnQszmy509qw/WZI+PyPJDHmt4aTVzY6g5neCjzICNmfsi4mYvI
+a4am7R4+GptP+TQyoVIhZWiwAYe9OUfDoR3cvFlo4bAJW8eglvnOlP7uGD0
G5oCi3gQTB/qzx3n4vaobJBwCQ+/xLiXqP/DhecDe52YMh42f7TZFkk9otV2
LvUVD+9Vrzm7TtB+i7vVqhp4uDl9I1noM51/Ie+wwgAPataF/hu/cmExuOB7
liQfftwDV0/MciF21yp9nQwfq261s3XUtR5RtuVyfNgGvZ9vMMeFfZfUnRYl
PpZ6J5my1O7Niyy+a/DhaVn0wkeQwYVyzetWJnz4mjktsRVh8CQFWh9c+VA+
m7WzU4zBxPmJBR4efHAl5qWsWsjAxPHeaNdBWh/ZmBxCXSgtndPmw0d09Y77
euIMXl3p3lTjx4dmqPicvwQDrmeQW3Y4H1NNQfempBjMX1aYdCWLj7QO9eHX
8gyshE9emnnCx8kViWvkFRhE9yz1PJ/Dh2mIkIELtWxipN6ZZ3wY2HHWjlGr
ijk9O1zOx96WgBA5RQZGo19bbFv5sMlYq2L5AwMXjoms2iwfl4IatgaoMsh3
+irVIMBCbA1fqJhaav7fEleFWeyK7PUQUmNQ6vyz6Igoi3OHbeZFUy9daD6X
K8/ibvbOvqQlDAa97Pg79FioPfP7nPEjgxMqvtUX97C44zt69ZwW3felfpWB
IwvGYeRiNrX6aVLZt5+FcG6dEo+6ocqv1NKNxVye3RpXbTrv2cAChaMsHL9s
FTTTYTBZH57++DKLRjnbqdFlDK6E3QlmHrIwCxtxFlzBQDJggbVHJu3ve/7y
Suokn+NygxwWcoW6uxypc7avTyO5LOpfdn97RD0o21gpVMIiQfL5RbuVDLan
CIjqNbHgmftoxegzaI891JDZwqLbdEI3h9oruDbRqJ1F76ZUbjv1Ja9EPdMu
FpsiPLZrrqL/Ybnh9p1DLJTPR+zJpVYscPv97DcWTWl3JKsMGKTdf7l3eoa+
l+37aYjaMGmF+mUBAocel2uihjTfoElOxDyC6JCcYkvqi1tiG29KE8DFaNdL
6r76FwrlmgQX+jiqnNUMTpZpdVnoEJRmTw68of6eHZleo0swvfiIyyD1oht7
TVr0Cc5frd6rasTA0pl1HP6JYJlkuH8odeaw2k2pHQRju5Qctq5hEHErzL3b
liB+rCTFldprN187y45Ayel0SAC1alHRExtHAnKCKN2njow98Crak0DnoesF
EWMGh8wropy9CQ4+sE9Qod48tdxupQ+Buqa9lSH1lOc0t+Y4wdcdfpedqH1M
/j++MJBgfYGJ1kNq81GBgs4LBC1vQ6WKqdVTfS4+vEQw3NwdV0/dKrZugXUo
gbBrwZEJ6q1drarXYgmu1szuNFnL4Mc4s759cQQl3CcLLai/W2Tc000gyFI6
5eZAnfvkrNHrJIKaFMU2P+qloQqWCzIIbkctmLtPPbvuomTbfYJzd3Qi86g7
+P3vMh4RHOm355RRX3fMcbF4SmDl1FHVTu0rsVhTMZfAonJ1YT+1ZVnI8EA+
wa+ViZYs9dzy3f5hRQQ+Q3GrhUwYdPY8X+dQQtD+k06MOHV+vMasVjlBW+Dr
EAXqE7NjEZVVBMWaG0y1qa1z9tnGv6H5PBUT0KfW9imV93pLYHtp2MWYmmmM
TRFuIpiVbx7aRP0sfPJgUwvBxu+98pbU8Rvd9NLaCQLdZzpsqE+NvWL9uAQ9
Nnob7Km3p6/K3dxNkNHgZbyPWsfpRqDcewK1T5zXztRC0rNmff0Ed8sXTrpT
d1d4i2QPEnC2ni3xov773Nvq4BGCV2FEw4f6hv7aWDsezSMycMkx6tN9yXs0
CEGCk3zeceodf4qojI/T/b4VDZyk1t3h21P6iaDe79cnp6lFBJvvXp8kyKnA
In/q3ryNR92nCTzHf1A4Q110NM3AcIbg44xg5n/+U13889wcge/H6Z7//C85
9O1y
     "]]}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->True,
  AxesLabel->{None, None},
  AxesOrigin->{0, 0},
  Method->{},
  PlotRange->{{0, 1}, {0., 0.44082376359043596`}},
  PlotRangeClipping->True,
  PlotRangePadding->{
    Scaled[0.02], 
    Scaled[0.02]}]], "Output",
 CellChangeTimes->{
  3.7397785080144167`*^9, 3.739778881186955*^9, 3.739779432848531*^9, {
   3.7562766078058643`*^9, 3.756276611524667*^9}, 3.756276664181273*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"(*", " ", "Sensitivity", " ", "*)"}], "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{"alphafn", "=", 
     RowBox[{"\[Alpha]", "/.", 
      RowBox[{"First", "[", 
       RowBox[{"NDSolve", "[", 
        RowBox[{
         RowBox[{"{", 
          RowBox[{
           RowBox[{
            RowBox[{
             RowBox[{
              RowBox[{"\[Alpha]", "''"}], "[", "s", "]"}], "+", 
             RowBox[{"Sin", "[", 
              RowBox[{"\[Psi]", "-", 
               RowBox[{"angP", "[", "s", "]"}]}], "]"}], "-", 
             RowBox[{"P", " ", 
              RowBox[{"Cos", "[", 
               RowBox[{"\[Psi]", "-", 
                RowBox[{"angP", "[", "s", "]"}]}], "]"}], " ", 
              RowBox[{"\[Alpha]", "[", "s", "]"}]}]}], "==", "0"}], ",", 
           RowBox[{
            RowBox[{"\[Alpha]", "[", "0", "]"}], "\[Equal]", "0"}], ",", 
           RowBox[{
            RowBox[{
             RowBox[{"\[Alpha]", "'"}], "[", "L", "]"}], "==", "0"}]}], "}"}],
          ",", "\[Alpha]", ",", 
         RowBox[{"{", 
          RowBox[{"s", ",", "0", ",", "L"}], "}"}]}], "]"}], "]"}]}]}], ";"}],
    "\[IndentingNewLine]", 
   RowBox[{"Plot", "[", 
    RowBox[{
     RowBox[{"alphafn", "[", "s", "]"}], ",", 
     RowBox[{"{", 
      RowBox[{"s", ",", "0", ",", "L"}], "}"}]}], "]"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"Export", "[", 
     RowBox[{"\"\<alpha.dat\>\"", ",", 
      RowBox[{"Table", "[", 
       RowBox[{
        RowBox[{"{", 
         RowBox[{"s", ",", 
          RowBox[{"alphafn", "[", "s", "]"}]}], "}"}], ",", 
        RowBox[{"{", 
         RowBox[{"s", ",", "0", ",", "1", ",", "0.01"}], "}"}]}], "]"}]}], 
     "]"}], ";"}]}]}]], "Input",
 CellChangeTimes->{{3.739778372404511*^9, 3.7397784629749937`*^9}}],

Cell[BoxData[
 GraphicsBox[{{}, {}, 
   {Hue[0.67, 0.6, 0.6], LineBox[CompressedData["
1:eJwV0Xk0lukbB3BbWRqMhB8pI2/GkjKmkOJbMbI0GoSSFEkqRdIUkczQWGP6
GQwpytJmiZeIhuweFFkLLyFZ3vd5bCUKc88f97nP55zrXPd9XV9lVy+bEwJ8
fHwp5Px3W5wYba0YszPi07Y30EowNRIyVvDul9+KXdqKto5PWfjxTuKtAnkT
JGo5vuQVboVlc1d+ivwBiAUlcDULTODgNvf8prwb7tj0WLGzD6Bvyx/G0fK+
EPnDP6vhrhtSZfZfj5APgVuDZf2KaF8EixR8Hy4fB69mbt8TlxAcPJSWOzmV
juPdPpOmvXFYtRzstHd9LtY45jUm+t3GRmt6tCqSDVUtv/xz1D1cerkdibMl
YDcLy8vcuA85thSHn78c9FSL7o6uxygaNG56tbMS8/uZkzvv56HydMraQ9xq
SInd9myqzsdHVUPJ7LN1SDDce+qHh2wIzD/6du1GCnfHJt8FphdBd/lYkW1O
E+ybKkoaThejKWTry1qNV3D/EpjjJPcMSrET8w/oFjwe2tYu2lmKibl+Tof2
a8QymYni7s+RPl5KSdxoQ9aZSNHuFeWQWJfFDA+3w4HVed7QtAJRpwrdU1md
UB+YnKqarcCnXY+GPYK68NXiksnMgxfw1almy1Z1I7rAZHOmZSX6c6ojW5Xf
4rWed+7SdCW+nG5PVvfvwf29dkaRMVXQ3C285F7ai4Y4rvQbhWrsvat88okc
B92lK5uKH1bj0rqjG9459SPPJtMmSKMGm7h2q7p+HoDSa5f5mrQaSEtOilQv
DEDi8sBgqmIthMqTc1VuvcO+0aW3ZuG1OLO19I2o5SB0OrZLZX+uxSNz19R/
Pg3isLhg+/Ojdfg4ofDtSPwQ1kWHZLk+q0Oh7V7BOJNh6D/Pu9UhW48gs9A7
7JFhbPkQzK48WQ/lPovue6HvwbK/4a+eXw8DvZA4eZ0RaIs2t19ZrIfT+lfz
/G0jKI+u+bXOqAEfqehB7+APaGInKO281oBEIWdfG9Yo0hZ3hnoXN0CaU3hX
oHoUfUeOJwlMNGBFYPCB7efH8MEzIoqjSIHvgXPu4OpxtO74VbrelIJPUm70
6spx/Bj/u/YOLwrPNcNEpM5NoNz9zLaFPylIWfhs+k2Ci1zHV1l8ORRSX0iu
MpDi4kJPLc+bWHt79tiUNBdl+d3DA8RWaqOZLvJchC/Zj5TnUohY6ay8m8XF
wksJPf8nFISqzGX5DLgYHxmXGmBT+LRDme/qCS5OTY5WeJdS6NVqafMr46Iz
QSYps47CnQmpQMFyLor7GblFYtcHtqrRL7jw6fvb2raewhir63JqLRc6tW6f
l4g/reWsq2vhoi/ymzW2FPm/GPfkmvdccD7/Eko3UTAdEf6aI86Ds3Wd03Qr
BdF08wz9b3nwb9xzcNdrCk0ukVaVq3m4FtQgeYPYtk8irUOOB9205Xy1NgrH
2mVMvyrzYEeBcmyncKVSJdZcl4eQ4puG+Z0U8m6DNezMw652S+PNPRRm/GeE
XVx46JVwcvch1nXImug7zsPipl/MiohLJSULujx4EOMfNzPspVB7jbO70YcH
dV1xQ7M+Cj2ugUfzr/NgmmE/bd1PYeX3pUnXcngYl3+dtHWIgrmg19XFPPK+
lEqKN3FU/wZX/wLizBDnx8RSiRHqvsU8bHl3db3KMAVFUcfik5U8rO+48H/x
9xR0Jj53WHXyIMMLsOkcoXAkV1dq3RIP5q0PtfTGKRQ5fpZo4aNRpb7J34tY
YuWzb34XpJEj5umfRVzhZCgyJkIjTFUrXnaCwgYxk2W2NI3p5F6DKeIRN2ve
PnUaazPUDBJ5FM4peDYEHKBxoq9ZPnuSzFujVbfFgYavYPVAD7HSeaZ68BAN
Fu/uYbEpCi11PhVmR2ms6lI6606sc9Hv6ZrTNAy/11tWmKYw9+p6xuMgGtLg
DFycoXAtNC249yGN6qGStOmPFMQvCVu4ZNNY36PrKfOJQpLH2dUjuTQk62Jn
9YgLLLffY9g0NsZxjAKIR6RaqwXKaSjbzpXwz1GwvM0not5GI2q+0oLvM4Xu
GPeW7A7Sn+Mxs57YLbgpUaebxmWPuUOGxFfdEtV39tGY+a71mB9xnoa25f4P
pL4r3I4mln169MbFLzRMhdJ6Gucp3LtfY7+wSOP8baXjI8TaSZpKQXwMLkv8
eZ9vgeQbOJcbtoKBZb3V2a3EAcYxrcmSDIxLfuIkEg+++mdNpQqDGNcUZ7sv
FLxesPpMVRk4pETVnCH+mh+R0ajGINr10ngwsUy8vW6HFoOw0N3nsonNnGiH
UT0Gfz8qPML/lUL26LpkiX0MDkSH9N4iDksJPcaxYsCxMcYTYjcb3sYcawYX
JOr21xArlpXl/ezA4FSakj+XOCLmcG2UKwNNC9as7iIFd5OqSKcTDAqH5GvM
iPfMa1hv8mDww6NuY0fiedeFnsazDGLZBRYBxB66f0+J+TFYWcMNLCM2meB7
+vYKgz3T+s8biZVSPQIeXiXzz1g/fEvcKaovbBHCYMv/uNFzxD/1dSqGxzDI
FGzy11qi8N1No8GDNxkMz4arGRB/Nc3MUvuLgcBLuRBTYnbeRZ36JAb/xKeb
OBNvCFljJpzJ4KfkyIlw4iX9APGu+ySP2VbhOOI3vKHXmY8YWBePt6YQxzoU
HDF9wmBvfJxXHrHnN2tVZNkM3m9TNS4hNnvx2+j7IgYalpHVL4iXNWwuhJYx
oOpbWlqJ3/aX6NuVM4hKf3L4DXFRnPISq5JBTa973ADxuaXJsOo6Bn9p75ek
iS0KDlrFUQyCs3+zniXe6FEh7dZM9mceuXuBuLc15rZgG4PJMEXNFcsUiq/P
HW/rYBBxJYMlRhy346j6vW4GbzSFmiSIvSdraZ8eBu9K9ZWliS0zNrP3cBi4
Whqz5IhVHeP9Vr9j0Den0qFALCC5ZDQ4xCCu9+2P64k5VSeE8kcY/LHaY5cy
8bPLzQ3BYwwu5TUuqBDHa22LseYyGKFWHFclPj9464Ayw0DGV9ZPjXhfgpDC
1BQD4bLFnRrEavs8+ytmGQTlleZoEgvxt6fHzjEQPPLLy03EA4U7Th9bYNBF
lSZrEZedvrdFe5EhdUuym4kTlFZ9XF4m+QjJ7frP/wL0HSz7
     "]]}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->True,
  AxesLabel->{None, None},
  AxesOrigin->{0, 0},
  Method->{},
  PlotRange->{{0, 1}, {0., 0.21091249589338415`}},
  PlotRangeClipping->True,
  PlotRangePadding->{
    Scaled[0.02], 
    Scaled[0.02]}]], "Output",
 CellChangeTimes->{3.739778511394713*^9, 3.7397788833052483`*^9, 
  3.756276614140657*^9, 3.756276665713353*^9}]
}, Open  ]]
},
WindowSize->{1405, 1045},
WindowMargins->{{Automatic, 60}, {119, Automatic}},
Magnification:>2. Inherited,
FrontEndVersion->"9.0 for Mac OS X x86 (32-bit, 64-bit Kernel) (January 25, \
2013)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[400, 13, 139, 2, 56, "Input"],
Cell[542, 17, 933, 19, 156, "Input"],
Cell[CellGroupData[{
Cell[1500, 40, 1703, 48, 255, "Input"],
Cell[3206, 90, 3730, 69, 504, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[6973, 164, 1808, 50, 255, "Input"],
Cell[8784, 216, 3661, 67, 532, "Output"]
}, Open  ]]
}
]
*)

