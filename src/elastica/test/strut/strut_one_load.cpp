// Sriramajayam

#include <el_TendonElastica.h>
#include <el_ConstantLoadDirection.h>
#include <P11DElement.h>
#include <el_Strut_State_Op.h>
#include <random>
#include <iostream>
#include <fstream>

int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULLPTR, PETSC_NULLPTR);

  // Create a uniform 1D mesh on the interval [0,1]
  // ------------------------------------------------
  const int nNodes = 40;
  const double h = 1./static_cast<double>(nNodes-1);
  std::vector<double> coordinates(nNodes);
  for(int n=0; n<nNodes; ++n)
    coordinates[n] = 0.+h*static_cast<double>(n);

  // Create strut
  // --------------
  const int nLoads = 1;
  std::vector<int> PNodes({nNodes-1});
  const double EI = 1.;
  const double origin[] = {0.,0.};
  el::TendonElastica* str = new el::TendonElastica(coordinates, EI, PNodes, origin);
  
  // Create one load of the specified type
  // --------------------------------------
  el::LoadParams load_params;
  load_params.values.resize(nLoads);
  load_params.dirs.resize(nLoads);
  load_params.values[0] = std::sqrt(2.); // Set the value of the load here
  const double angle = M_PI/3.;
  const double dir[] = {std::cos(angle), std::sin(angle)};
  load_params.dirs[0] = new el::ConstantLoadDirection(PNodes[0],0,*str,dir);

  // Dirichlet BCs: clamp the left end
  // ----------------------------------
  std::map<int,double> bc_params{};
  bc_params[0] = 0.;
  
  // Solve parameters
  // ------------------
  el::Solve_Params solve_params({
      .consistentLinearization = false, // Automatically consistent
	.EPS = 1.e-10,
	.resScale = 1.,
	.dofScale = 1.,
	.nMaxIters = 25,
	.verbose = true});

 
  // Compute the state and sensitivities
  // -----------------------------------
  str->ComputeStateAndSensitivities(load_params, bc_params, solve_params);

  // Compute xy coordinates
  // -----------------------
  const auto& xy = str->GetCartesianCoordinates();
  
  // Plot the state and sensitivity
  // -----------------------------------
  const auto& state = str->GetStateField();
  const auto& sense = str->GetSensitivityField(0);
  std::fstream stream;
  stream.open("ss.dat", std::ios::out);
  for(int n=0; n<nNodes; ++n)
    stream << coordinates[n] << " "<<state[n]<<" "<<sense[n]
	   <<" "<<xy[2*n]<<" "<<xy[2*n+1]<<"\n";
  stream.close();

  // Compute the strain energy
  const double SE = str->GetStrainEnergy(); 
  std::cout<<"\n\n\n";
  // Compute the strain energy in an alternate way
  double SE2 = 0.;
  const auto& ElmArray = str->GetElementArray();
  const auto& theta = str->GetStateField();
  el::StateConfiguration config;
  config.L2GMap = &str->GetLocalToGlobalMap();
  config.state = &theta;
  for(int e=0; e<nNodes-1; ++e)
    {
      el::Strut_State_Op op(e, ElmArray[e], EI, 0., 0.);
      SE2 += op.GetEnergy(&config);
    }
  std::cout<<"\nTotal strain energy: "<<SE<<" (should be "<<SE2<<") "<<std::flush;
  
  // Clean up
  delete str;
  for(auto&x: load_params.dirs) delete x;
  PetscFinalize();
}
