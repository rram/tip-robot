(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 10.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[      7730,        190]
NotebookOptionsPosition[      7368,        172]
NotebookOutlinePosition[      7757,        189]
CellTagsIndexPosition[      7714,        186]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[
 RowBox[{
  RowBox[{"(*", " ", "Sriramajayam", " ", "*)"}], "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{"Clear", "[", "\"\<Global`*\>\"", "]"}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{"(*", " ", 
    RowBox[{"Do", " ", "not", " ", "change", " ", "parameters"}], " ", "*)"}],
    "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"\[Lambda]", "=", "2.1755"}], ";"}], 
   RowBox[{"(*", 
    RowBox[{
     RowBox[{"PL", "^", "2"}], "/", "EI"}], "*)"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"\[Alpha]", "=", 
     RowBox[{"60", " ", "Degree"}]}], ";"}], 
   RowBox[{"(*", 
    RowBox[{"end", " ", "angle"}], "*)"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"L", "=", "1"}], ";"}], 
   RowBox[{"(*", 
    RowBox[{"Length", " ", "of", " ", "domain"}], "*)"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"angP", "=", 
     RowBox[{"\[Theta]", "/.", 
      RowBox[{"First", "[", 
       RowBox[{"NDSolve", "[", 
        RowBox[{
         RowBox[{"{", 
          RowBox[{
           RowBox[{
            RowBox[{
             RowBox[{
              RowBox[{"\[Theta]", "''"}], "[", "s", "]"}], "+", 
             RowBox[{"\[Lambda]", " ", 
              RowBox[{"Cos", "[", 
               RowBox[{"\[Alpha]", "-", 
                RowBox[{"\[Theta]", "[", "s", "]"}]}], "]"}]}]}], "\[Equal]", 
            "0"}], ",", 
           RowBox[{
            RowBox[{"\[Theta]", "[", "0", "]"}], "\[Equal]", "0"}], ",", 
           RowBox[{
            RowBox[{
             RowBox[{"\[Theta]", "'"}], "[", "L", "]"}], "\[Equal]", "0"}]}], 
          "}"}], ",", "\[Theta]", ",", 
         RowBox[{"{", 
          RowBox[{"s", ",", "0", ",", "L"}], "}"}]}], "]"}], "]"}]}]}], ";"}],
    "\[IndentingNewLine]", 
   RowBox[{"Plot", "[", 
    RowBox[{
     RowBox[{"angP", "[", "s", "]"}], ",", 
     RowBox[{"{", 
      RowBox[{"s", ",", "0", ",", "L"}], "}"}]}], "]"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"Export", "[", 
     RowBox[{"\"\<theta.dat\>\"", ",", 
      RowBox[{"Table", "[", 
       RowBox[{
        RowBox[{"{", 
         RowBox[{"s", ",", 
          RowBox[{"angP", "[", "s", "]"}]}], "}"}], ",", 
        RowBox[{"{", 
         RowBox[{"s", ",", "0", ",", "L", ",", "0.01"}], "}"}]}], "]"}]}], 
     "]"}], ";"}]}]}]], "Input",
 CellChangeTimes->{{3.7396859862586374`*^9, 3.739686129063095*^9}, {
  3.7396861808263755`*^9, 3.739686202801566*^9}, {3.7396876467442436`*^9, 
  3.739687914578061*^9}, {3.739688688545763*^9, 3.7396886992493324`*^9}, {
  3.7396888452024713`*^9, 3.7396888593710403`*^9}, {3.739691921891057*^9, 
  3.7396919324120083`*^9}, {3.739691979361345*^9, 3.7396920817299695`*^9}, {
  3.7396921442251277`*^9, 3.7396921462871614`*^9}, {3.7397806827962217`*^9, 
  3.7397806891398706`*^9}, {3.7397807366329813`*^9, 3.7397807544808817`*^9}, {
  3.739780812995573*^9, 3.739780920621389*^9}, {3.7397811671628942`*^9, 
  3.7397811777816687`*^9}, {3.7397812695846443`*^9, 3.739781320953101*^9}, {
  3.739781372693273*^9, 3.739781392342733*^9}, {3.739784428020081*^9, 
  3.739784430297534*^9}, {3.739784504252068*^9, 3.739784538673229*^9}, {
  3.7397846246850977`*^9, 3.739784634461445*^9}}],

Cell[BoxData[
 GraphicsBox[{{}, {}, 
   {Hue[0.67, 0.6, 0.6], LineBox[CompressedData["
1:eJwV1Hk019kbB3BbWZrIkn5IxpJoNWqQyjvxq1BkKS1arKlESTOJSNEI0ZQk
hLIlyb5ll5iRQtayf+18v5/70a9GUvrd+eOee17n3Oe5zz3nea6yg4eVswAf
H99Duv7dTZ3HW6om9hvw7Y7i1hk7GwgZyZ/rl9uExF/3pUhfM8LGhOi4PDlj
vDV8t9g/wBpmbzpzH8rZwNlgbjb8qiNsnWbK78g5waVftO+13wX0bvjD6Jac
F973XJ7e53MdiUstboTIBcK63sbdxusuAkTyVt2Ui8R0mFbTSpckHDz0KIud
TsadAU7S5NJ8LPoRYLdrRRbMmpRlY9LKsdKSGX8Zmo868VQjK6+X+P3tZkR/
KgH7NEZ+mctfWJYv2cfPX4nS9J6KB2wjCjlGjU1ba/AL9++A0KFm1Jx+qHCI
WwuHVz6Wu/Ra8Vl9m0Tm2XpYGK/WuXm4HQKzGUsUVjZAZHZJ0caETuj8OFFo
/bwRlwSy7gjkvkdj4Ka3daubYPvCe1VwVjeUbk/NpjPNaP5YJkhe92Jqpr+v
XesdGNMjOduL+5E8WdogHt4KrUadZ1YDAxBXTCPDw23Q7VA9y+saRNipApdE
tQ58KSwSv/uCg3+2Zwy7+nfiRZvyf84/GYKXdm2+7MsuhDeNPeP6D6P/eW1o
i/IHkAvp7zY6jGDudFus5uVuRJVqTX7VG8UaQ+F5l9IeaIWI6Wd/HcWux8on
c5b14do513L7V2P4XfG4yqBdP35z9fiY4zOOtdz9izr3DqBj05Q0Z9UEpCVY
kdqvA+gfXCph0T0BocrYLNW4QSQ5nTJq8JnEmU2l70XNOBA93ecnpzKFDBOH
xIp/OFjvuyglsXwKn6fkl4xGDUErV+tj5S4uCqx3CUYaD6Mtd3s1286F/+6g
hPzRYYht0EsMsuZBude0KyloBGtO6mVP1POgrxsYKac9Cpbz412GAQO7FU2z
/K2jyBk+kuWYyuBzwy3OuYAxMArms7ISBNFCx7ys1MZRP/lO6d4pAum+gscC
teN4YJvw3buUYMGVAJvN5yeQNNNSbrOQBV/6sSyO1CTqg1/tTdRg4RmTdUuq
ZhJqZ4bUBkxYlK8JFpF0n8KSB6Eu4S4sJE09114T56LEblTv1RUWidUSi/Ql
uXAsrjym4sdCa3PmxLQ0F7JSFsb+1OYa46n2clycSIsw1/NnEbLwmLKhGhe+
efePp11lIfTSRJZPn4tUq4oL3tdZ/LNFmc/PmYusuO1OvGAWPeuaW73LuIgW
kHeWucsiYUryimAlF++SJezsqB3SrdVvVXPRfmlOPpl6Qq3zUmIdF0UZBax2
JM2n0KdY38xFxae8ub33aP1i3JMyI1xc5LRHeN9nsXNU+NvzxTy034jPTo1l
IZpskqK3hIeLqftTx6gb7UPNa6R4cHkm4KQRx8K6V/xR+zIeMh0MdNOpT7Qt
3flNmQevbVu3PXnIwqdG9baJDg+7Oy3qYxNYZMdDbfgYD8EGf7UeTGLxv8v/
E7a358H0k2zKn9Q6tmlTvY40X9fN7Q3UpRISeZ2uPJiv7mnXT2ZRd7XP8LUn
Dx8/aF+WS2HR7XDleO4NHjwL0tIbU1ksXFUac/U5D0pvnySJPWVhIujh9z2b
h+WPfUq2Uof1qzhczuOh+IbnA3dqyegQTa9iHoKuNBa1UC8XPVx8soYHD8Ff
3O9lsNCe+tJu3sFDlV9ptVQmi6NZOpKK8zzMPX8rOJLFovDwF/FmPgYbyk+L
yWSzEF/44qfrggw6ShJbdlBX2W0TmRBhcN37w6MEahUx4x/50gy02UCTAzks
Rp0seXs0GZQW5hcU5bJwl3f729eGgXPQJX6zAvreV+vqN9gyMDLbp+BJrXSe
1HIOMRgaqP4cTd1c71m1+ziDHd6e3BFq7YveRTKnGWyyFB30LWQx03Qj5Zk/
A4N9i8fii1hcDXoU0POUgb6cu0teCYvFvwub2mcyaC6r8W+mjnE9KzWaxcDd
asieR51ntjmJ5DPYbhl8Vf0FrVeypVagkoFaiP2qKGqzeD4RzVYGoSf3KZwp
ZdEV4dKc2c5gZsixOpDaKaAxWruLwYSJkVY8tZ9TtObWXgayLc6uzdTZq7XM
LMYYtNVu3qFdxkK26Hj4xTkG65cqxk1RJz15deDrdwb3xMlb/nI6TzFrlPz5
CCqZsIZl1CZXZrKCFxCYOK5dbkTtaxTREkvnvlDxq8Z9ak5ThUyNKsG5sebx
TRUsPKrVeneqEwxY2T/ZRf0tNyTltQbB+dRyrcPUS6MO6LSvI8hc+CHWj3q3
HWM7rkuwNedRdy115rhirPgegrZBN/5dlSyCHwad6DMnuKwaL3mQ2smKt/K5
JY1ftKDXlXp5WVn2XlsC45YLKTepQyKO1IU5EMxuaf9WT+1i/DLUzpmgpSxv
tIN6x+xqy7WuBHvG6kNHqGcdvna/PktQoP+nsEAVC1edB9Ni3gSdI2mvdKmN
p/iKPvgQWFVsvGlMrZTo6vvUj8DB6LOwJXWHqJ6waSDBQxs53VPU/+3tWH4z
guBY3MiRKOqf7xhwDt4huMRZMZtI/W1naprGPYLIzluWGdT52Re1/4ohaGz6
aVMltUqgzG7hVIK18+GVQ9Tzer6LO58Q2Ndc1OFSv+cNvUvNIFihe8f1E/Vt
27yjO3PoP6ztNi9YzcLtJwVV2XwC3xJNh0XUu6uvjY8UEuhMLw+Qov6x2upC
UBmB/oVkVon6Q3+J3v5Ker5cz1CdujBSeV6thuB6scDetdTu82xwbT1BaJVu
lC61ad5B88gGAvM3j1u3Uq90rZJ2ekNgF2LUaEjd0xIRL9hKEC6jTkypi2/M
OLa20/sGT8pZUEduOa6Z1EWw/Wj/vBX1ObaO8ewm2BcclnCA2ixlff6OPgJJ
B4+5Q9Tqh6O8pQYJVCb/kDpKLSAxb8AZIpD/uWP4OHXfS2eh3FGCIMHDng7U
Ly69+TtggmDsrnSpE3XUul8jLLkE5c0ClS7U5zlxNsqE4LeSVX6u1HvuC8lP
TxP47L325RS1xh63/qpPBJphUuvPUAvxtyXfnqH96NW1wo16oGDL6RNfCeoW
tjX867LTSRu0vhMYGC7QPkt9X2nR5x8/CDxU3Wz+9f8BDsQq3g==
     "]]}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->True,
  AxesLabel->{None, None},
  AxesOrigin->{0, 0},
  Method->{},
  PlotRange->{{0, 1}, {0., 1.0472234056234944`}},
  PlotRangeClipping->True,
  PlotRangePadding->{
    Scaled[0.02], 
    Scaled[0.02]}]], "Output",
 CellChangeTimes->{3.756277325295496*^9}]
}, Open  ]]
},
WindowSize->{1539, 1255},
WindowMargins->{{Automatic, 114}, {-21, Automatic}},
Magnification:>2. Inherited,
FrontEndVersion->"9.0 for Mac OS X x86 (32-bit, 64-bit Kernel) (January 25, \
2013)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 3176, 79, 354, "Input"],
Cell[3759, 103, 3593, 66, 516, "Output"]
}, Open  ]]
}
]
*)

(* End of internal cache information *)
