// Sriramajayam

#include <el_TendonElastica.h>
#include <P11DElement.h>
#include <el_FollowerLoadDirection.h>
#include <iostream>
#include <fstream>
#include <random>

int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULLPTR, PETSC_NULLPTR);

  // Create a uniform 1D mesh on the interval [0,1]
  const int nNodes = 10;
  const double h = 1./static_cast<double>(nNodes-1);
  std::vector<double> coordinates(nNodes);
  for(int n=0; n<nNodes; ++n)
    coordinates[n] = 0.+h*static_cast<double>(n);

  // Create strut
  const int nLoads = 2;
  std::vector<int> PNodes({nNodes/2, nNodes-1});
  const double EI = std::sqrt(5.);
  const double origin[] = {0.,0.};
  el::TendonElastica *str = new el::TendonElastica(coordinates, EI, PNodes, origin);
  
  // Generate a random values for state/loads/directions/sensitivities
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_real_distribution<> dis(-2.,2.);
  
  // Create 2 load at fixed angles
  
  el::LoadParams load_params;
  load_params.values.resize(nLoads);
  load_params.dirs.resize(nLoads);
  for(int i=0; i<nLoads; ++i)
    { load_params.values[i] = dis(gen);
      load_params.dirs[i] = new el::FollowerLoadDirection(PNodes[i],i,*str); }
  
  // Dirichlet BCs
  std::map<int,double> bc_params{};
  bc_params[0] = 0.;

  // Set random values for the state
  std::vector<double> theta(nNodes);
  for(int i=0; i<nNodes; ++i)
    theta[i] = dis(gen);
  str->SetStateField(&theta[0]);
  
  // Do a consistency test for sensitivities
  for(int L=0; L<nLoads; ++L)
    {
      // Random values for sensitivity
      std::vector<double> alpha(nNodes);
      for(int i=0; i<nNodes; ++i)
	alpha[i] = dis(gen);

      // Consistency test
      str->SensitivityConsistencyTest(L, &alpha[0], load_params, bc_params);
    }

  // Clean up
  delete str;
  for(auto& x:load_params.dirs) delete x;
  PetscFinalize();
}
