// Sriramajayam

#include <el_TendonElastica.h>
#include <el_FollowerLoadDirection.h>
#include <P11DElement.h>
#include <random>
#include <iostream>
#include <fstream>

int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULLPTR, PETSC_NULLPTR);

  // Create a uniform 1D mesh on the interval [0,1]
  // ------------------------------------------------
  const int nNodes = 40;
  const double h = 1./static_cast<double>(nNodes-1);
  std::vector<double> coordinates(nNodes);
  for(int n=0; n<nNodes; ++n)
    coordinates[n] = 0.+h*static_cast<double>(n);

  // Create strut
  // --------------
  const int nLoads = 1;
  std::vector<int> PNodes({nNodes-1});
  const double EI = 1.;
  const double origin[] = {0.,0.};
  el::TendonElastica* str = new el::TendonElastica(coordinates, EI, PNodes, origin);
  
  // Create one load of the specified type
  // --------------------------------------
  el::LoadParams load_params;
  load_params.values.resize(nLoads);
  load_params.dirs.resize(nLoads);
  load_params.values[0] = 2.1755; // Do not change. 13.75 gives a rotation of 180degrees.
  load_params.dirs[0] = new el::FollowerLoadDirection(PNodes[0],0,*str);

  // Dirichlet BCs: clamp the left end
  // ----------------------------------
  std::map<int,double> bc_params{};
  bc_params[0] = 0.;
  
  // Solve parameters
  // ------------------
  el::Solve_Params solve_params({
      .consistentLinearization=true, // Make consistent
	.EPS = 1.e-10,
	.resScale = 1.,
	.dofScale = 1.,
	.nMaxIters = 25,
	.verbose = true});
 
  
  // Compute the state and sensitivities
  // -----------------------------------
  str->ComputeState(load_params, bc_params, solve_params);

  // Plot the state and sensitivity
  // -----------------------------------
  const auto& state = str->GetStateField();
  std::fstream stream;
  stream.open("ss.dat", std::ios::out);
  for(int n=0; n<nNodes; ++n)
    stream << coordinates[n] << " "<<state[n]<<"\n";
  stream.close();

  // Clean up
  delete str;
  for(auto&x: load_params.dirs) delete x;
  PetscFinalize();
}
