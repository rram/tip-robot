// Sriramajayam

#include <el_TendonElastica.h>
#include <el_ConstantLoadDirection.h>
#include <el_ConstantMoment.h>
#include <P11DElement.h>
#include <random>
#include <iostream>
#include <fstream>

int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULLPTR, PETSC_NULLPTR);

  // Create a uniform 1D mesh on the interval [0,1]
  // ------------------------------------------------
  const int nNodes = 20;
  const double h = 1./static_cast<double>(nNodes-1);
  std::vector<double> coordinates(nNodes);
  for(int n=0; n<nNodes; ++n)
    coordinates[n] = 0.+h*static_cast<double>(n);

  // Create strut
  // --------------
  const int nLoads = 1;
  std::vector<int> PNodes({nNodes-1});
  const double EI = 1.;
  const double origin[] = {0.,0.};
  el::TendonElastica* str = new el::TendonElastica(coordinates, EI, PNodes, origin);

  // Create one load dummy load
  // --------------------------------------
  el::LoadParams load_params;
  load_params.values.resize(nLoads);
  load_params.dirs.resize(nLoads);
  load_params.values[0] = 0.;
  const double angle = M_PI/3.;
  const double dir[] = {std::cos(angle), std::sin(angle)};
  load_params.dirs[0] = new el::ConstantLoadDirection(PNodes[0],0,*str,dir);

  // Create one moment load
  load_params.moments.resize(1);
  load_params.moments[0] = new el::ConstantMoment(PNodes[0],*str, 2.*M_PI);
  
  // Dirichlet BCs: clamp the left end
  // ----------------------------------
  std::map<int,double> bc_params{};
  bc_params[0] = 0.;
    
  // Solve parameters
  // ------------------
  el::Solve_Params solve_params({
      .consistentLinearization = true, // Automatically consistent
	.EPS = 1.e-10,
	.resScale = 1.,
	.dofScale = 1.,
	.nMaxIters = 25,
	.verbose = true});
  
  // Compute the state
  // -----------------------------------
  str->ComputeState(load_params, bc_params, solve_params);
  const auto& xy = str->GetCartesianCoordinates();
  
  // Plot the state
  // -----------------------------------
  const auto& state = str->GetStateField();
  std::fstream stream;
  stream.open("ss.dat", std::ios::out);
  for(int n=0; n<nNodes; ++n)
    stream << coordinates[n] << " "<<state[n]<<" "<<xy[2*n]<<" "<<xy[2*n+1]<<"\n";
  stream.close();

  // Clean up
  delete str;
  for(auto&x: load_params.dirs) delete x;
  for(auto&x: load_params.moments) delete x;
  PetscFinalize();
}
