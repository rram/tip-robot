// Sriramajayam

#include <el_TendonElastica.h>
#include <cassert>
#include <iostream>

namespace el
{
  // Main functionality
  void TendonElastica::ComputeState(LoadParams& load_params,
				    const std::map<int,double>& bc_params,
				    const Solve_Params& solve_params)
  {
    // Check parameters
    load_params.Check();
    assert(static_cast<int>(bc_params.size())>0 && static_cast<int>(bc_params.size())<=2);
    solve_params.Check();
    
    if(solve_params.verbose)
      std::cout<<"\nel::TendonElastica::Computing state..."<<std::flush;
    
    // Set Dirichlet BCs for the state
    for(auto& it:bc_params)
      theta[it.first] = it.second;
    
    // Set the current theta as the initial guess
    const double* init_guess = &theta[0];

    // Setup the solver context
    SNESContext ctx;
    ctx.elastica = this;
    ctx.load_params = &load_params;
    ctx.bc_params = &bc_params;
    ctx.solve_params = &solve_params;
    ctx.LoadNum = -1;
        
    // Solve
    theta_solver.Solve(init_guess, &ctx, solve_params.verbose);
    
    // Get the solution
    double* dofValues;
    PetscErrorCode ierr = VecGetArray(theta_solver.solVec, &dofValues); CHKERRV(ierr);
    const int nDofs = L2GMap->GetTotalNumDof();
    for(int i=0; i<nDofs; ++i)
      theta[i] = dofValues[i];
    ierr = VecRestoreArray(theta_solver.solVec, &dofValues); CHKERRV(ierr);

    // Mark state as not dirty and sensitivities as dirty
    state_is_dirty = false;
    sense_is_dirty = true;

    // Compute nodal Cartesian coordinates
    ComputeCartesianCoordinates();
    
    // -- done --
    return;
  }

  // Computes the sensitivity at a given set of loads
  void TendonElastica::ComputeStateAndSensitivities(LoadParams& load_params,
						    const std::map<int,double>& bc_params,
						    const Solve_Params& solve_params)
  {
    // Compute the state
    ComputeState(load_params, bc_params, solve_params);
    assert(state_is_dirty==false && sense_is_dirty==true);

    if(solve_params.verbose)
      std::cout<<"\nel::TendonElastica Computing sensitivities..."<<std::flush;

    // Solver context
    SNESContext ctx;
    ctx.elastica = this;
    ctx.load_params = &load_params;
    ctx.bc_params = &bc_params;
    ctx.solve_params = &solve_params;
    ctx.LoadNum = -1;
	
    // Compute sensitivity wrt each load
    for(int loadnum=0; loadnum<nLoads; ++loadnum)
      {
	ctx.LoadNum = loadnum;
	
	// Initialize sensitivity Dirichlet bcs to 0
	auto& alpha = alphaVec[loadnum];
	const int nDofs = L2GMap->GetTotalNumDof();
	for(auto& it:bc_params)
	  alpha[it.first] = 0.;
	
	// Initial guess
	const double* init_guess = &alpha[0];

	// Solve
	alpha_solver.Solve(init_guess, &ctx, solve_params.verbose);

	// Get the solution
	double* dofValues;
	PetscErrorCode ierr = VecGetArray(alpha_solver.solVec, &dofValues); CHKERRV(ierr);
	for(int i=0; i<nDofs; ++i)
	  alpha[i] = dofValues[i];
	ierr = VecRestoreArray(alpha_solver.solVec, &dofValues); CHKERRV(ierr);

	// calcs completed for this load
      }
    
    // Mark sensitivity as clean
    sense_is_dirty = false;

    // Compute sensitivities of nodal Cartesian coordinates
    ComputeCartesianCoordinateSensitivities();
    
    return;
  }
  
}
