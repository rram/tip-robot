// Sriramajayam

#include <el_NLopt_LoadOptimizer.h>
#include <iostream>

namespace el
{
  // Constructor
  NLopt_Params::NLopt_Params()
    :set_lb(false), lower_bounds({}),
     set_ub(false), upper_bounds({}),
     set_abs_f(false), set_rel_f(false),
     set_abs_x(false), set_rel_x(false) {}

  // Function evaluating the function and objective
  double NLopt_Objective_Function(const std::vector<double>& x,
				  std::vector<double>& grad,
				  void* params)
  {
    // Access
    assert(params!=nullptr);
    auto& opt = *static_cast<NLopt_LoadOptimizer*>(params);
    auto& elastica = opt.elastica;
    auto& objective = opt.objective;
    auto& bc_params = opt.bc_params;
    auto& load_params = opt.load_params;
    auto& solve_params = opt.solve_params;
      
    // Copy loads
    const int nLoads = static_cast<int>(x.size());
    for(int L=0; L<nLoads; ++L)
      load_params.values[L] = x[L];
      
    double Jval = 0.;
    if(grad.empty()) // only function evaluation
      {
	elastica.ComputeState(load_params, bc_params, solve_params);
	objective.Evaluate(elastica, Jval);
      }
    else // objective + gradient
      {
	elastica.ComputeStateAndSensitivities(load_params, bc_params, solve_params);
	objective.Evaluate(elastica, Jval, grad);
      }
      
    // done
    return Jval;
  }


  
  // Constructor
  NLopt_LoadOptimizer::NLopt_LoadOptimizer(TendonElastica& istr,
					   ObjectiveFunctional& obj,
					   LoadParams& lp,
					   const std::map<int,double>& bp,
					   const Solve_Params& sp)
    :elastica(istr),
     objective(obj),
     load_params(lp),
     bc_params(bp),
     solve_params(sp),
     nLoads(istr.GetNumLoads()),
     loads(istr.GetNumLoads())
  {
    // Create the optimizer
    optimizer = new nlopt::opt(nlopt::LD_SLSQP, nLoads);
    
    // Set the objective function evaluator
    optimizer->set_min_objective(NLopt_Objective_Function, this);
  }
     

  // Destructor
  NLopt_LoadOptimizer::~NLopt_LoadOptimizer()
  { delete optimizer; }

  
  // Main functionality: optimize
  void NLopt_LoadOptimizer::Optimize(const NLopt_Params& opt_params)
  {
    // Set optimization parameters
    
    if(opt_params.set_lb) // lower bounds
      {
	assert(static_cast<int>(opt_params.lower_bounds.size())==nLoads);
	optimizer->set_lower_bounds(opt_params.lower_bounds);
      }

    if(opt_params.set_ub) // upper bounds
      {
	assert(static_cast<int>(opt_params.upper_bounds.size())==nLoads);
	optimizer->set_upper_bounds(opt_params.upper_bounds);
      }

    if(opt_params.set_abs_f) // absolute tolerance for objective
      {
	assert(opt_params.abs_ftol>0.);
	optimizer->set_ftol_abs(opt_params.abs_ftol);
      }

    if(opt_params.set_rel_f)
      {
	assert(opt_params.rel_ftol>0.);
	optimizer->set_ftol_rel(opt_params.rel_ftol);
      }

    if(opt_params.set_abs_x)
      {
	assert(opt_params.abs_xtol>0.);
	optimizer->set_ftol_abs(opt_params.abs_xtol);
      }

    if(opt_params.set_rel_x)
      {
	assert(opt_params.rel_xtol>0.);
	optimizer->set_ftol_rel(opt_params.rel_xtol);
      }
    
    // Optimize
    for(int L=0; L<nLoads; ++L)
      loads[L] = load_params.values[L];
    double Jval = 0.;

    try
      {
	auto result = optimizer->optimize(loads, Jval); }
    catch(std::exception &e)
      {
	std::cout<<"\n nlopt failed: "<<e.what() <<"\n"; }
    
    // done
    return;
  }

}
