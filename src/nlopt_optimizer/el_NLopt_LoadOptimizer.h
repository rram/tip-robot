// Sriramajayam

#ifndef TR_NLOPT_LOAD_OPTIMIZER_H
#define TR_NLOPT_LOAD_OPTIMIZER_H

#include <el_TendonElastica.h>
#include <el_ObjectiveFunctional.h>
#include <nlopt.hpp>

namespace el
{
  //! Helper struct for optimization options
  struct NLopt_Params
  {
    bool set_lb;
    std::vector<double> lower_bounds;

    bool set_ub;
    std::vector<double> upper_bounds;

    bool set_abs_f;
    double abs_ftol;

    bool set_rel_f;
    double rel_ftol;

    bool set_rel_x;
    double rel_xtol;

    bool set_abs_x;
    double abs_xtol;

    //! Constructor
    NLopt_Params();
  };


  //! Wrapper class for optimization with NLopt
  class NLopt_LoadOptimizer
  {
  public:

    //! Constructor
    NLopt_LoadOptimizer(TendonElastica& istr,
			ObjectiveFunctional& obj,
			LoadParams& lp,
			const std::map<int,double>& bp,
			const Solve_Params& sp);
    
    //! Destructor
    virtual ~NLopt_LoadOptimizer();

    // Disable copy and assignment
    NLopt_LoadOptimizer(const NLopt_LoadOptimizer&) = delete;
    NLopt_LoadOptimizer operator=(const NLopt_LoadOptimizer&) = delete;

    //! Main functionality: optimize
    void Optimize(const NLopt_Params& opt_params);

    //! Access the solver object
    inline nlopt::opt& GetSolver()
    { return *optimizer; }

  private:
    TendonElastica& elastica;
    ObjectiveFunctional& objective;
    LoadParams& load_params;
    const std::map<int,double>& bc_params;
    const Solve_Params& solve_params;
    const int nLoads;
    nlopt::opt* optimizer;
    std::vector<double> loads;

    // friend
    friend double NLopt_Objective_Function(const std::vector<double>& x,
					   std::vector<double>& grad,
					   void* params);
  };
}

#endif
