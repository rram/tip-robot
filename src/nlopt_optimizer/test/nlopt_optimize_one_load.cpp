// Sriramajayam

#include <el_NLopt_LoadOptimizer.h>
#include <el_ConstantLoadDirection.h>
#include <el_Objective_AngleProfile.h>
#include <P11DElement.h>
#include <iostream>
#include <fstream>
#include <random>


// By default: (each can be changed)
// -----------------------------------
// Loads are uniformly spaced on 'PNodes'
// Target objective is hard coded in 'ObjFunc'
//------------------------------------

// Objective function
void ObjFunc(const double& x, const double& theta, double* fval, double* dfval, void*);

int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULLPTR, PETSC_NULLPTR);

  // Create a uniform 1D mesh on the interval [0,1]
  // -----------------------------------------------
  const int nNodes = 20;
  const double h = 1./static_cast<double>(nNodes-1);
  std::vector<double> coordinates(nNodes);
  for(int n=0; n<nNodes; ++n)
    coordinates[n] = 0.+h*static_cast<double>(n);
  
  // Create one load of the specified type
  // --------------------------------------
  const int nLoads = 1;
  std::vector<int> PNodes({nNodes-1});

  // Create strut
  // -------------
  const double EI = 1.;
  const double origin[] = {0.,0.};
  el::TendonElastica *str = new el::TendonElastica(coordinates, EI, PNodes, origin);
  
  // Initialize load to 0
  el::LoadParams load_params;
  load_params.values.resize(nLoads);
  load_params.dirs.resize(nLoads);
  load_params.values[0] = 0.;

  // Constant load direction
  {
    std::random_device rd; 
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> dir_dis(-1.,1.);
    double dir[] = {dir_dis(gen), dir_dis(gen)};
    double norm = std::sqrt(dir[0]*dir[0]+dir[1]*dir[1]);
    dir[0] /= norm; dir[1] /= norm;
    std::cout<<"\nLoad type: constant, along the direction ("<<dir[0]<<", "<<dir[1]<<") "<<std::flush;
    load_params.dirs[0] = new el::ConstantLoadDirection(PNodes[0],0,*str,dir);
  }

  // Dirichlet BCs: clamp the left end
  // ----------------------------------
  std::map<int,double> bc_params{};
  bc_params[0] = 0.;

  // Solve parameters
  // ------------------
  el::Solve_Params solve_params({
      .consistentLinearization = true,
	.EPS = 1.e-10,
	.resScale = 1.,
	.dofScale = 1.,
	.nMaxIters = 25,
	.verbose = true});

  // Objective function and parameters
  // ----------------------------------
  std::function<decltype(ObjFunc)> obj_func_ptr = ObjFunc;
  el::Objective_AngleProfile objective(obj_func_ptr);
  
  // Create load optimizer
  // ----------------------
  el::NLopt_LoadOptimizer Opt(*str, objective, load_params, bc_params, solve_params);

  // Optimization parameters: use relative tolerances to check convergence
  el::NLopt_Params opt_params;
  opt_params.set_lb = false;
  opt_params.set_ub = false;
  opt_params.set_abs_f = false;
  opt_params.set_rel_f = true; opt_params.rel_ftol = 1.e-6;
  opt_params.set_abs_x = false;
  opt_params.set_rel_x = true; opt_params.rel_xtol = 1.e-6;
    

  // Optimize
  // ---------
  Opt.Optimize(opt_params);

  // Print the optimal load and the corresponding solution
  // ------------------------------------------------------
  std::cout<<"\n\nOptimal load: "<<load_params.values[0];
  const auto& state = str->GetStateField();
  const auto& xy = str->GetCartesianCoordinates();

  std::fstream stream;
  stream.open("optstate.dat", std::ios::out);
  for(int n=0; n<nNodes; ++n)
    stream << coordinates[n] << " "<<state[n]<<" "<<xy[2*n]<<" "<<xy[2*n+1]<<"\n";
  stream.close();
  
  // Clean up
  delete str;
  for(auto& x:load_params.dirs) delete x;
  PetscFinalize();
}


// Objective function
void ObjFunc(const double& s, const double& theta, double* fval, double* dfval, void* params)
{
  // target function: 0.1*s*(s-2)
  double target = 0.1*s*(s-2.);
  
  if(fval!=nullptr)
    (*fval) = 0.5*(theta-target)*(theta-target);
  if(dfval!=nullptr)
    (*dfval) = (theta-target);
  return;
}
