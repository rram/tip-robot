// Sriramajayam

#include <el_NLopt_LoadOptimizer.h>
#include <el_TendonElastica.h>
#include <el_ConstantLoadDirection.h>
#include <el_FollowerLoadDirection.h>
#include <el_TendonLoadDirection.h>
#include <el_Objective_AngleProfile.h>
#include <P11DElement.h>
#include <iostream>
#include <fstream>
#include <random>

// By default: (each can be changed)
// -----------------------------------
// Number of loads is hard coded in 'nLoads'
// Loads are uniformly spaced on 'PNodes'
// Type of loading is randomly picked in 'LT' from one constant, follower and tendon
// Loading direction and routing points for constant and tendon loads are randomly picked
// Target objective is hard coded in 'ObjFunc'
//------------------------------------

// Objective function
void ObjFunc(const double& x, const double& theta, double* fval, double* dfval, void*);

int main(int argc, char** argv)
{
  // Set the number of loads
  // ------------------------
  const int nLoads = 2;
  
  std::random_device rd; 
  std::mt19937 gen(rd());
  std::uniform_int_distribution<> lt_dis(0, 2);
    
  // Choose the type of loading (randomly select one among three)
  // ------------------------------------------------------------
  enum class LoadType {Constant, Follower, Tendon};
  const LoadType LT = LoadType::Tendon; //(static_cast<LoadType>(lt_dis(gen)));

  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULLPTR, PETSC_NULLPTR);

  // Create a uniform 1D mesh on the interval [0,1]
  // -----------------------------------------------
  const int nNodes = 40;
  const double h = 1./static_cast<double>(nNodes-1);
  std::vector<double> coordinates(nNodes);
  for(int n=0; n<nNodes; ++n)
    coordinates[n] = 0.+h*static_cast<double>(n);
  
  // Create one load of the specified type
  // --------------------------------------
  std::vector<int> PNodes(nLoads);
  for(int i=0; i<nLoads; ++i)
    PNodes[i] = ((nNodes-1)*(i+1))/nLoads;
  std::cout<<"\nLoading nodes: ";
  for(int i=0; i<nLoads; ++i) std::cout<<PNodes[i]<<" "<<std::flush;

  // Create strut
  // -------------
  const double EI = 1.;
  const double origin[] = {0.,0.};
  el::TendonElastica *str = new el::TendonElastica(coordinates, EI, PNodes, origin);
  
  // Initialize all loads to 0
  el::LoadParams load_params;
  load_params.values.resize(nLoads);
  std::fill(load_params.values.begin(), load_params.values.end(), 0.);
  // Set loading directions
  load_params.dirs.resize(nLoads);
  switch(LT)
    {
      // Constant load direction
    case LoadType::Constant:
      { std::uniform_real_distribution<> dir_dis(-1.,1.);
	std::cout<<"\nLoad type: constant";
	for(int i=0; i<nLoads; ++i)
	  { double dir[] = {dir_dis(gen), dir_dis(gen)};
	    double norm = std::sqrt(dir[0]*dir[0]+dir[1]*dir[1]);
	    dir[0] /= norm; dir[1] /= norm;
	    std::cout<<"\nDirection "<<i<<": ("<<dir[0]<<", "<<dir[1]<<") "<<std::flush;
	    load_params.dirs[i] = new el::ConstantLoadDirection(PNodes[i],i,*str,dir); }
	break; }
      
      // Follower load direction
    case LoadType::Follower:
      { std::cout<<"\nLoad type: follower. "<<std::flush;
	for(int i=0; i<nLoads; ++i)
	  load_params.dirs[i] = new el::FollowerLoadDirection(PNodes[i],i,*str); 
	break; }
      
      // Tendon load direction
    case LoadType::Tendon:
      { std::uniform_real_distribution<> pos_dis(0.1,0.5);
	std::cout<<"\nLoad type: tendon";
	for(int i=0; i<nLoads; ++i)
	  {
	    el::RoutingPost RtPost;
	    RtPost.center[0] = 0.; RtPost.center[1] = pos_dis(gen);
	    RtPost.radius = 0.; RtPost.orientation = 0.;
	    std::cout<<"\nRouting point "<<i<<": ("<<RtPost.center[0]<<", "<<RtPost.center[1]<<") "<<std::flush;
	    load_params.dirs[i] = new el::TendonLoadDirection(PNodes[i],i,*str,RtPost); }
	break;}

    default: assert(false && "Unexpected loading type");
    }
      
  // Dirichlet BCs: clamp the left end
  // ----------------------------------
  std::map<int,double> bc_params{};
  bc_params[0] = 0.;

  // Solve parameters
  // ------------------
  el::Solve_Params solve_params({
      .consistentLinearization = false, // By default, switch on if necessary
	.EPS = 1.e-10,
	.resScale = 1.,
	.dofScale = 1.,
	.nMaxIters = 25,
	.verbose = true});

  // Objective function and parameters
  // ----------------------------------
  std::function<decltype(ObjFunc)> obj_func_ptr = ObjFunc;
  el::Objective_AngleProfile objective(obj_func_ptr);
  
  // Create load optimizer
  // ----------------------
  auto* Opt = new el::NLopt_LoadOptimizer(*str, objective, load_params, bc_params, solve_params);

  // Optimization parameters: use relative tolerances to check convergence
  el::NLopt_Params opt_params;
  opt_params.set_lb = false;
  opt_params.set_ub = false;
  opt_params.set_abs_f = false;
  opt_params.set_rel_f = true; opt_params.rel_ftol = 1.e-6;
  opt_params.set_abs_x = false;
  opt_params.set_rel_x = true; opt_params.rel_xtol = 1.e-6;

  // Optimize
  // ---------
  Opt->Optimize(opt_params);

  // Print the optimal load and the corresponding solution
  // ------------------------------------------------------
  std::cout<<"\n\nOptimal loads: ";
  for(int i=0; i<nLoads; ++i) std::cout<<load_params.values[i]<<" "<<std::flush;
  const auto& state = str->GetStateField();
  const auto& xy = str->GetCartesianCoordinates();

  std::fstream stream;
  stream.open("optstate.dat", std::ios::out);
  for(int n=0; n<nNodes; ++n)
    stream << coordinates[n] << " "<<state[n]<<" "<<xy[2*n]<<" "<<xy[2*n+1]<<"\n";
  stream.close();
  
  // Clean up
  delete str;
  delete Opt;
  for(auto& x:load_params.dirs) delete x;
  PetscFinalize();
}


// Objective function
void ObjFunc(const double& s, const double& theta, double* fval, double* dfval, void* params)
{
  // target function: 0.1*s*(s-2)*s
  double target = 0.1*s*(s-2.)*s;
  
  if(fval!=nullptr)
    (*fval) = 0.5*(theta-target)*(theta-target);
  if(dfval!=nullptr)
    (*dfval) = (theta-target);
  return;
}
