// Sriramajayam

#include <el_Utils.h>
#include <cassert>
#include <cmath>
#include <P11DElement.h>

namespace el
{
  // Computes the nodal Cartesian coordinates given the angle theta
  void AngleToCartesianMap(const std::vector<double>& dofvals,
			   const std::vector<Element*>& ElmArray,
			   const LocalToGlobalMap& L2GMap,
			   std::vector<double>& xy)
  {
    // Resize outputs
    const int nDofs = L2GMap.GetTotalNumDof();
    xy.resize(nDofs*2);
    xy[0] = 0.;
    xy[1] = 0.;
    const int nElements = static_cast<int>(ElmArray.size());
    assert(L2GMap.GetNumElements()==nElements);
    for(int e=0; e<nElements; ++e)
      {
	assert(static_cast<const P11DElement<1>*>(ElmArray[e])!=nullptr);
	const auto& Qwts = ElmArray[e]->GetIntegrationWeights(0);
	const int nQuad = static_cast<int>(Qwts.size());
	const int nDof = ElmArray[e]->GetDof(0);
	assert(nDof==2);
	double dx = 0.;
	double dy = 0.;
	for(int q=0; q<nQuad; ++q)
	  {
	    // theta value here
	    double thetaval = 0.;
	    for(int a=0; a<nDof; ++a)
	      thetaval += dofvals[L2GMap.Map(0,a,e)]*ElmArray[e]->GetShape(0,q,a);

	    // Integrate
	    dx += Qwts[q]*std::cos(thetaval);
	    dy += Qwts[q]*std::sin(thetaval);
	  }
	xy[2*(e+1)+0] = xy[2*e+0] + dx;
	xy[2*(e+1)+1] = xy[2*e+1] + dy;
      }
    return;
  }
  
}

