// Sriramajayam

#ifndef ELASTICA_UTILS_H
#define ELASTICA_UTILS_H

#include <Element.h>
#include <LocalToGlobalMap.h>
#include <StressWorkUtils.h>
#include <cassert>

namespace el
{
  struct StateConfiguration
  {
    const std::vector<double>* state;
    const LocalToGlobalMap* L2GMap;
  };

  struct SensitivityConfiguration
  {
    const std::vector<double>* state;
    const std::vector<double>* sensitivity;
    const LocalToGlobalMap* L2GMap;
  };
  

  //! Computes the nodal Cartesian coordinates given the angle theta
  //! Assumes that elements are numbered sequentially. 
  void AngleToCartesianMap(const std::vector<double>& theta,
			   const std::vector<Element*>& ElmArray,
			   const LocalToGlobalMap& L2GMap,
			   std::vector<double>& xy);

}

#endif
