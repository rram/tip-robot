// Sriramajayam

#ifndef EL_STRUT_SENSITIVITY_OP_H
#define EL_STRUT_SENSITIVITY_OP_H

#include <ElementalOperation.h>
#include <utility>
#include <el_Utils.h>

namespace el
{
  
  class Strut_Sensitivity_Op: public DResidue
  {
  public:
    //! Constructor
    //! \param[in] elm Element for this operation. Same interpolation for all fields
    //! \param[in] fieldnum Field number for dof angle
    //! \param[in] str Configuration access
    inline Strut_Sensitivity_Op(const int elmnum, const Element* elm, 
				const double ei,
				const double lambda_H, const double lambda_V,
				const double dlambda_H, const double dlambda_V)
      :DResidue(), ElmNum(elmnum), Elm(elm),   Fields({0}), EI(ei),
      LambdaH(lambda_H), LambdaV(lambda_V),
      dLambdaH(dlambda_H), dLambdaV(dlambda_V) {}
    
    
    //! Destructor. Nothing to do
    inline virtual ~Strut_Sensitivity_Op() {}
    
    //! Disable copy and assignment
    Strut_Sensitivity_Op(const Strut_Sensitivity_Op&) = delete;
    Strut_Sensitivity_Op& operator=(const Strut_Sensitivity_Op&) = delete;
    
    //! Disable cloning
    inline virtual Strut_Sensitivity_Op* Clone() const override
    { assert(false && "Cannot clone");
      return nullptr; }
    
    //! Returns the element being used
    inline const Element* GetElement() const
    { return Elm; }

    //! Returns the fields used
    inline const std::vector<int>& GetField() const override
    { return Fields; }

    //! Returns the number of dofs for a given field
    inline int GetFieldDof(int fieldnumber) const override
    { return Elm->GetDof(Fields[fieldnumber]); }

    //! Returns the modulus
    inline double GetModulus() const
    { return EI; }

    //! Returns the load
    inline void GetLoadValues(double* lambda, double* dlambda) const
    { lambda[0]  = LambdaH;   lambda[1] = LambdaV;
      dlambda[0] = dLambdaH; dlambda[1] = dLambdaV; }

    //! Set the load
    inline void SetLoadValues(const double* lambda, const double* dlambda)
    { LambdaH = lambda[0];    LambdaV = lambda[1];
      dLambdaH = dlambda[0]; dLambdaV = dlambda[1]; }

    //! Computes the energy functional
    //! \param[in] Config Configuration at which to compute the energy
    double GetEnergy(const void* Config) const;
    
    //! Residual calculation
    //! \param[in] Config Configuration at which to compute the residual
    //! \param[in] resval Computed local residual
    inline void GetVal(const void* Config,
		std::vector<std::vector<double>>* resval) const override
    { return GetDVal(Config, resval, nullptr); }

    //! Residual and stiffness calculation
    //! \param[in] Config Configuration at which to compute the residual
    //! \param[in] resval Computed local residual
    //! \param[in] dresval Computed local stiffness
    void GetDVal(const void* Config, 
		 std::vector<std::vector<double>>* resval,
		 std::vector<std::vector<std::vector<std::vector<double>>>>* dresval) const override;

    //! Consistency test for this operation
    //! \param[in] Config Configuration at which to perform the consistency test
    //! \param[in] pertEPS Tolerance to use for perturbations
    //! \param[in] tolEPS Tolerance to use for examining outputs
    bool ConsistencyTest(const void* Config,
				 const double pertEPS, const double tolEPS) const override;

  private:
    const int ElmNum;
    const Element* Elm; //!< Element for this operation
    std::vector<int> Fields; //!< Field number for this operation
    const double EI; //!< Bending modulus
    double LambdaH, LambdaV;  //!< Load value multiplying sin(theta), cos(theta) terms
    double dLambdaH, dLambdaV; //!< dLoad 
  };

}

#endif
