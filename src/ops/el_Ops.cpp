// Sriramajayam

#include <el_Ops.h>
#include <cassert>
#include <cmath>

using namespace el;

// Compute the bending eenrgy at a given state
double Ops::GetEnergy(const void* arg) const
{
  // Get this state
  assert(arg!=nullptr);
  const auto* config = static_cast<const StateConfiguration*>(arg);
  assert(config!=nullptr);
  const auto& state = *config->state;
  const auto& L2GMap = *config->L2GMap;

  // Fields, ndofs, shape function derivatives
  const int field = Fields[0];
  assert(field==0);
  const int nDof = GetFieldDof(0);
  const int nDeriv = Elm->GetNumDerivatives(field);
  assert(nDeriv==1);
  const auto& DShapes = Elm->GetDShape(field);

  // Quadrature
  const auto& Qwts = Elm->GetIntegrationWeights(field);
  const int nQuad = static_cast<int>(Qwts.size());

  // Integrate
  double Energy = 0.;
  double dtheta = 0.; //  = theta'
  for(int q=0; q<nQuad; ++q)
    {
      // Derivatives of shape functions at this quadrature point
      const double* qDShapes = &DShapes[q*nDof];
      dtheta = 0.;
      for(int a=0; a<nDof; ++a)
	dtheta += state[L2GMap.Map(field,a,ElmNum)]*qDShapes[a];

      // Update energy
      Energy += 0.5*EI*Qwts[q]*dtheta*dtheta;
    }
  return Energy;
}


// Residual and stiffness calculation
void Ops::GetDVal(const void *arg, 
		  std::vector<std::vector<double>>* funcval, 
		  std::vector<std::vector<std::vector<std::vector<double>>>>* dfuncval) const
{
  // get this state
  assert(arg!=nullptr);
  const auto* config = static_cast<const StateConfiguration*>(arg);
  assert(config!=nullptr && "el::Ops::GetDVal- invalid pointer for configuration");
  const auto& state = *config->state;
  const auto& L2GMap = *config->L2GMap;

  // Zero the outputs
  SetZero(funcval, dfuncval);

  // Fields, ndofs, num of derivatives
  const int field = Fields[0];
  assert(field==0);
  const int nDof = GetFieldDof(field);
  const int nDeriv = Elm->GetNumDerivatives(field);
  assert(nDeriv==1 && "el::Ops::GetDVal- unexpected number of derivatives");
  const auto& DShapes = Elm->GetDShape(field);
  
  // Quadrature
  const auto& Qwts = Elm->GetIntegrationWeights(field);
  const int nQuad = static_cast<int>(Qwts.size());

  // Integrate
  double dtheta;
  for(int q=0; q<nQuad; ++q)
    {
      // Derivatices of shape functions at this quadrature point
      const double* qDShapes = &DShapes[q*nDof*1];

      // Compute theta' here
      dtheta = 0.;
      for(int a=0; a<nDof; ++a)
	dtheta += state[L2GMap.Map(field,a,ElmNum)]*qDShapes[a*1+0]; // 1D

      // Udpate the force vector
      if(funcval!=nullptr)
	for(int a=0; a<nDof; ++a)
	  // This variation delta F = N_a
	  (*funcval)[0][a] += EI*Qwts[q]*dtheta*qDShapes[a*1+0];

      // Update the stiffness
      if(dfuncval!=nullptr)
	for(int a=0; a<nDof; ++a)
	  // This variation delta F = N_a
	  for(int b=0; b<nDof; ++b)
	    // This variation DELTA F = N_b
	    (*dfuncval)[0][a][0][b] += EI*Qwts[q]*qDShapes[a]*qDShapes[b];
    }
  return;
}


// Consistency test
bool Ops::ConsistencyTest(const void* arg,
			  const double pertEPS,
			  const double tolEPS) const
{
  assert(arg!=nullptr);
  const auto* config = static_cast<const StateConfiguration*>(arg);
  assert(config!=nullptr);
  const auto& state = *config->state;
  const auto& L2GMap = *config->L2GMap;

  // Fields and dofs
  const int field = Fields[0];
  assert(field==0);
  const int nDof = GetFieldDof(field);

  // Size arrays
  std::vector<std::vector<double>> res(1), resplus(1), resminus(1);
  res[0].resize(nDof);
  resplus[0].resize(nDof);
  resminus[0].resize(nDof);

  std::vector<std::vector<std::vector<std::vector<double>>>> dres(1);
  dres[0].resize(nDof);
  for(int a=0; a<nDof; ++a)
    {
      dres[0][a].resize(1);
      dres[0][a][0].resize(nDof);
    }

  // Implemented residual and stiffness
  GetDVal(arg, &res, &dres);

  // Perturbed state
  std::vector<double> pertState(state);
  StateConfiguration pertConfig;
  pertConfig.state = &pertState;
  pertConfig.L2GMap = &L2GMap;
  
  // Consistency tests
  for(int a=0; a<nDof; ++a)
    {
      const int node = L2GMap.Map(field,a,ElmNum);
      
      // Positive perturbation
      pertState[node] += pertEPS;
      double Eplus = GetEnergy(&pertConfig);
      GetVal(&pertConfig, &resplus);

      // Negative perturbation
      pertState[node] -= 2.*pertEPS;
      double Eminus = GetEnergy(&pertConfig);
      GetVal(&pertConfig, &resminus);

      // undo perturbation
      pertState[node] += pertEPS;
      
      // numerical derivatives
      double nres = (Eplus-Eminus)/(2.*pertEPS);
      assert(std::abs(nres-res[0][a])<tolEPS && "el::Ops::Consistency of residuals failed");

      for(int b=0; b<nDof; ++b)
	{
	  double ndres = (resplus[0][b]-resminus[0][b])/(2.*pertEPS);
	  assert(std::abs(ndres-dres[0][a][0][b])<tolEPS && "el::Ops::Consistency of residuals failed");
	}
    }
  return true;
}
  
  
  
      
      
  
  


      
