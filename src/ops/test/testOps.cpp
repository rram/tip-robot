// Sriramajayam

#include <el_Ops.h>
#include <P11DElement.h>
#include <random>
#include <cassert>
#include <cmath>

using namespace el;

int main()
{
  // Create one segment
  std::vector<double> coordinates({std::sqrt(2.), std::sqrt(10.)});
  Segment<1>::SetGlobalCoordinatesArray(coordinates);
  std::vector<Element*> ElmArray(1);
  ElmArray[0] = new P11DElement<1>({1,2});
  StandardP11DMap L2GMap(ElmArray);
  
  // Random number generator
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_real_distribution<double> dist(-1.,1.);

  // State a random state
  const int nNodes = 2;
  std::vector<double> state(nNodes);
  for(int a=0; a<2; ++a)
    state[a] = dist(gen);

  // Tolerances
  const double pertEPS = 1.e-5;
  const double tolEPS = 1.e-5;

  // Test class
  Ops Op(0, ElmArray[0], std::sqrt(2.));
  assert(Op.GetField().size()==1 && "Unexpected number of fields");
  assert(Op.GetField()[0]==0 && "Unexpected field number");
  assert(Op.GetFieldDof(0)==2 && "Unexpected number of dofs");
  assert(Op.GetElement()==ElmArray[0] && "Unexpected element returned");
  StateConfiguration config;
  config.state = &state;
  config.L2GMap = &L2GMap;
  assert(Op.ConsistencyTest(&config, pertEPS, tolEPS) && "Failed consistency test");

  delete ElmArray[0];
}

