// Sriramajayam

#include <el_Strut_Sensitivity_Op.h>
#include <P11DElement.h>
#include <random>
#include <cassert>
#include <cmath>

using namespace el;

int main()
{
  // Create one segment
  std::vector<double> coordinates({std::sqrt(2.), std::sqrt(10.)});
  Segment<1>::SetGlobalCoordinatesArray(coordinates);
  std::vector<Element*> ElmArray(1);
  ElmArray[0] = new P11DElement<1>({1,2});
  StandardP11DMap L2GMap(ElmArray);

  // Random number generator
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_real_distribution<double> dist(-1.,1.);

  // State a random state
  const int nNodes = 2;
  std::vector<double> state(nNodes);
  for(int a=0; a<2; ++a)
    state[a] = dist(gen);

  // Set a random sensitivity field
  std::vector<double> sensitivity(nNodes);
  for(int a=0; a<2; ++a)
    sensitivity[a] = dist(gen);
  
  // Create a configuration
  SensitivityConfiguration config;
  config.state = &state;
  config.sensitivity = &sensitivity;
  config.L2GMap = &L2GMap;
  
  // Tolerances
  const double pertEPS = 1.e-5;
  const double tolEPS = 1.e-5;
  
  // Test class
  const double EI = std::sqrt(10.)+dist(gen);
  const double Lambda[] = {std::sqrt(5.)+dist(gen), std::sqrt(5.)-dist(gen)};
  const double dLambda[] = {std::sqrt(2.)+dist(gen), std::sqrt(2.)-dist(gen)};
  Strut_Sensitivity_Op Op(0, ElmArray[0], EI, Lambda[0], Lambda[1], dLambda[0], dLambda[1]);
  assert(Op.GetField().size()==1 && "Unexpected number of fields");
  assert(Op.GetField()[0]==0 && "Unexpected field number");
  assert(Op.GetFieldDof(0)==2 && "Unexpected number of dofs");
  assert(Op.GetElement()==ElmArray[0] && "Unexpected element returned");
  assert(Op.ConsistencyTest(&config, pertEPS, tolEPS) && "Failed consistency test");
  assert(std::abs(Op.GetModulus()-EI)<1.e-10);
  double lambda[2], dlambda[2];
  Op.GetLoadValues(lambda, dlambda);
  assert(std::abs(lambda[0]-Lambda[0])+std::abs(lambda[1]-Lambda[1]) +
	 std::abs(dlambda[0]-dLambda[0])+std::abs(dlambda[1]-dLambda[1]) < 1.e-10);
  
  delete ElmArray[0];
}

