// Sriramajayam

#include <el_Strut_Sensitivity_Op.h>
#include <cassert>
#include <cmath>
#include <iostream>

namespace el
{
  // Compute the energy functional
  double Strut_Sensitivity_Op::GetEnergy(const void* arg) const
  {
    // Get this state
    assert(arg!=nullptr);
    const auto* config = static_cast<const SensitivityConfiguration*>(arg);
    assert(config!=nullptr);
    const auto& state = *config->state;
    const auto& sensitivity = *config->sensitivity;
    const auto& L2GMap = *config->L2GMap;
  
    // Fields, ndofs, shape function derivatives
    const int field = Fields[0];
    assert(field==0);
    const int nDof = GetFieldDof(0);
    const int nDeriv = Elm->GetNumDerivatives(field);
    assert(nDeriv==1 && "el::Strut_Sensitivity_Op::GetEnergy- unexpected number of derivatives");
    const auto& Shapes = Elm->GetShape(field);
    const auto& DShapes = Elm->GetDShape(field);

    // Quadrature
    const auto& Qwts = Elm->GetIntegrationWeights(field);
    const int nQuad = static_cast<int>(Qwts.size());

    // Integrate
    double Energy = 0.;
    double theta; 
    double alpha;
    double dalpha;
    for(int q=0; q<nQuad; ++q)
      {
	// Shape functions and derivatives  at this quadrature point
	const double* qShapes = &Shapes[q*nDof];
	const double* qDShapes = &DShapes[q*nDof];
	theta = 0.;
	alpha = 0.;
	dalpha = 0.;
	for(int a=0; a<nDof; ++a)
	  {
	    const int node = L2GMap.Map(field,a,ElmNum);
	    theta += state[node]*qShapes[a];
	    alpha += sensitivity[node]*qShapes[a];
	    dalpha += sensitivity[node]*qDShapes[a];
	  }

	// Update energy
	Energy += Qwts[q]*( 0.5*EI*dalpha*dalpha +
			    0.5*alpha*alpha*(LambdaH*std::cos(theta) + LambdaV*std::sin(theta)) +
			    alpha*(dLambdaH*std::sin(theta) - dLambdaV*std::cos(theta)) );
      }
    return Energy;
  }


  // Residual and stiffness calculation
  void Strut_Sensitivity_Op::GetDVal(const void *arg, 
				    std::vector<std::vector<double>>* funcval, 
				    std::vector<std::vector<std::vector<std::vector<double>>>>* dfuncval) const
  {
    // get this state
    assert(arg!=nullptr);
    const auto* config = static_cast<const SensitivityConfiguration*>(arg);
    assert(config!=nullptr);
    const auto& state = *config->state;
    const auto& sensitivity = *config->sensitivity;
    const auto& L2GMap = *config->L2GMap;
    
    // Zero the outputs
    SetZero(funcval, dfuncval);
    
    // Fields, ndofs, num of derivatives
    const int field = Fields[0];
    assert(field==0);
    const int nDof = GetFieldDof(field);
    const int nDeriv = Elm->GetNumDerivatives(field);
    assert(nDeriv==1 && "el::Strut_Sensitivity_Op::GetDVal- unexpected number of derivatives");
    const auto& Shapes = Elm->GetShape(field);
    const auto& DShapes = Elm->GetDShape(field);
  
    // Quadrature
    const auto& Qwts = Elm->GetIntegrationWeights(field);
    const int nQuad = static_cast<int>(Qwts.size());

    // Integrate
    double theta;
    double alpha;
    double dalpha;
    for(int q=0; q<nQuad; ++q)
      {
	// Shape functions and derivatices  at this quadrature point
	const double* qShapes = &Shapes[q*nDof];
	const double* qDShapes = &DShapes[q*nDof];

	// Compute theta, alpha & alpha' here
	theta = 0.;
	alpha = 0.;
	dalpha = 0.;
	for(int a=0; a<nDof; ++a)
	  {
	    const int node = L2GMap.Map(field,a,ElmNum);
	    theta += state[node]*qShapes[a];
	    alpha += sensitivity[node]*qShapes[a];
	    dalpha += sensitivity[node]*qDShapes[a]; // 1D
	  }

	// Udpate the force vector
	if(funcval!=nullptr)
	  for(int a=0; a<nDof; ++a)
	    // This variation delta F = N_a
	    (*funcval)[0][a] += Qwts[q]*(EI*dalpha*qDShapes[a] +
					 alpha*(LambdaH*std::cos(theta)+LambdaV*std::sin(theta))*qShapes[a] +
					 (dLambdaH*std::sin(theta)-dLambdaV*std::cos(theta))*qShapes[a]);
	
	// Update the stiffness
	if(dfuncval!=nullptr)
	  for(int a=0; a<nDof; ++a)
	    // This variation delta F = N_a
	    for(int b=0; b<nDof; ++b)
	      // This variation DELTA F = N_b
	      (*dfuncval)[0][a][0][b] += Qwts[q]*
		( EI*qDShapes[a]*qDShapes[b] +
		  (LambdaH*std::cos(theta)+LambdaV*std::sin(theta))*qShapes[a]*qShapes[b] );
      }
    return;
  }
  
  
  // Consistency test
  bool Strut_Sensitivity_Op::ConsistencyTest(const void* arg,
					     const double pertEPS,
					     const double tolEPS) const
  {
    assert(arg!=nullptr);
    const auto* config = static_cast<const SensitivityConfiguration*>(arg);
    assert(config!=nullptr);
    const auto& state = *config->state;
    const auto& sensitivity = *config->sensitivity;
    const auto& L2GMap = *config->L2GMap;
    
    // Fields and dofs
    const int field = Fields[0];
    const int nDof = GetFieldDof(field);

    // Size arrays
    std::vector<std::vector<double>> res(1),resplus(1), resminus(1);
    res[0].resize(nDof);
    resplus[0].resize(nDof);
    resminus[0].resize(nDof);

    std::vector<std::vector<std::vector<std::vector<double>>>> dres(1);
    dres[0].resize(nDof);
    for(int a=0; a<nDof; ++a)
      {
	dres[0][a].resize(1);
	dres[0][a][0].resize(nDof);
      }

    // Implemented residual and stiffness
    GetDVal(arg, &res, &dres);

    // Perturbed configuration
    std::vector<double> pertSense(sensitivity);
    SensitivityConfiguration pertConfig;
    pertConfig.state = &state;
    pertConfig.sensitivity = &pertSense;
    pertConfig.L2GMap = &L2GMap;
    
    // Consistency tests
    for(int a=0; a<nDof; ++a)
      {
	const int node = L2GMap.Map(field,a,ElmNum);
	
	// Positive perturbations
	pertSense[node] += pertEPS;
	double Eplus = GetEnergy(&pertConfig);
	GetVal(&pertConfig, &resplus);

	// Negative perturbations
	pertSense[node] -= 2.*pertEPS;
	double Eminus = GetEnergy(&pertConfig);
	GetVal(&pertConfig, &resminus);

	// Undo perturbations
	pertSense[node] +=  pertEPS;

	// Numerical derivatives
	double nres = (Eplus-Eminus)/(2.*pertEPS);
	//std::cout<<"\n"<<res[0][a]<<" should be "<<nres<<std::flush;
	assert(std::abs(nres-res[0][a])<tolEPS && "el::Strut_Sensitivity_Op::Consistency of residuals failed");
	
	// Compute perturbed residuals
	for(int b=0; b<nDof; ++b)
	  {
	    double ndres = (resplus[0][b]-resminus[0][b])/(2.*pertEPS);
	    //std::cout<<"\n"<<dres[0][a][0][b]<<" should be "<<ndres<<std::flush;
	    assert(std::abs(ndres-dres[0][a][0][b])<tolEPS && "el::Strut_Sensitivity_Op::Consistency of residuals failed");
	  }
      }
    return true;
  }
  
}
