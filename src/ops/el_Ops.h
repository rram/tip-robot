// Sriramajayam

#ifndef ELASTICA_OPS_H
#define ELASTICA_OPS_H

#include <ElementalOperation.h>
#include <el_Utils.h>

namespace el
{
  //! Class for computing energies, weak forms and Hessian for an
  //! initially straight beam modeled as a 2D elastica.
  //! Dofs are assumed to be theta-values
  //! Bending modulus is assumed to be 1.
  class Ops: public DResidue
  {
  public:
    //! Constructor
    //! \param[in] elm Element for this operation. Same interpolation for all fields
    //! \param[in] fieldnum Field number for dof angle
    //! \param[in] str Configuration access
    inline Ops(const int elmnum, const Element* elm, const double ei)
      :DResidue(), ElmNum(elmnum), Elm(elm),  EI(ei), Fields({0}) {}


    //! Destructor. Nothing to do
    inline virtual ~Ops() {}

    //! Disable copy and assignment
    Ops(const Ops&) = delete;
    Ops operator=(const Ops&) = delete;
    
    //! Disable cloning
    inline virtual Ops* Clone() const override
    { assert(false && "Cannot clone");
      return nullptr; }

    //! Returns the element being used
    inline const Element* GetElement() const
    { return Elm; }

    //! Returns the fields used
    inline const std::vector<int>& GetField() const override
    { return Fields; }

    //! Returns the number of dofs for a given field
    inline int GetFieldDof(int fieldnumber) const override
    { return Elm->GetDof(Fields[fieldnumber]); }

    //! Returns the modulus
    inline double GetModulus() const
    { return EI; }
      
    //! Computes the energy functional
    //! \param[in] Config Configuration at which to compute the energy
    double GetEnergy(const void* Config) const;

    //! Residual calculation
    //! \param[in] Config Configuration at which to compute the residual
    //! \param[in] resval Computed local residual
    inline void GetVal(const void* Config,
		std::vector<std::vector<double>>* resval) const override
    { return GetDVal(Config, resval, nullptr); }

    //! Residual and stiffness calculation
    //! \param[in] Config Configuration at which to compute the residual
    //! \param[in] resval Computed local residual
    //! \param[in] dresval Computed local stiffness
    void GetDVal(const void* Config, 
		 std::vector<std::vector<double>>* resval,
		 std::vector<std::vector<std::vector<std::vector<double>>>>* dresval) const override;

    //! Consistency test for this operation
    //! \param[in] Config Configuration at which to perform the consistency test
    //! \param[in] pertEPS Tolerance to use for perturbations
    //! \param[in] tolEPS Tolerance to use for examining outputs
    bool ConsistencyTest(const void* Config,
			 const double pertEPS, const double tolEPS) const override;

  private:
    const int ElmNum;
    const Element* Elm; //!< Element for this operation
    const double EI;
		      
    std::vector<int> Fields; //!< Field number for this operation
  };
    
}

#endif
