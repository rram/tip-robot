# Sriramajayam

add_library(tr_ops STATIC
  el_Ops.cpp
  el_Strut_Sensitivity_Op.cpp
  el_Strut_State_Op.cpp)

# headers
target_include_directories(tr_ops PUBLIC
  ${DGPP_INCLUDE_DIRS}
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/../utils>)

# Add required flags
target_compile_features(tr_ops PUBLIC ${tr_COMPILE_FEATURES})

install(FILES
  el_Ops.h
  el_Strut_Sensitivity_Op.h
  el_Strut_State_Op.h
  DESTINATION ${PROJECT_NAME}/include)
