// Sriramajayam

#include <el_Tao_LoadOptimizer.h>
#include <iostream>

namespace el
{
  // Function and gradient evaluation
  PetscErrorCode Tao_Objective_Function(Tao tao, Vec solVec,
					PetscReal* J, Vec dJ, void* usr)
  {
    // Get the application context
    Tao_LoadOptimizer* opt;
    PetscErrorCode ierr = TaoGetApplicationContext(tao, &opt); CHKERRQ(ierr);
    assert(opt!=nullptr && "el::Tao_LoadOptimizer::FormFunctionGradient- user context is null");
    
    // Unpackage
    auto& elastica = opt->elastica;
    auto& objective = opt->objective;
    auto& load_params = opt->load_params;
    auto& bc_params = opt->bc_params;
    auto& solve_params = opt->solve_params;
      
    // Loads at which to evaluate state and sensitivities
    double* loads;
    ierr = VecGetArray(solVec, &loads); CHKERRQ(ierr);
    int nLoads;
    ierr = VecGetSize(solVec, &nLoads); CHKERRQ(ierr);
    for(int i=0; i<nLoads; ++i)
      load_params.values[i] = loads[i];
    ierr = VecRestoreArray(solVec, &loads); CHKERRQ(ierr);

    // Compute the state and sensitivities
    elastica.ComputeStateAndSensitivities(load_params, bc_params, solve_params);

    // Evaluate the objective and its gradient
    double Jval = 0.;
    std::vector<double> dJvals(nLoads);
    std::fill(dJvals.begin(), dJvals.end(), 0.);
    objective.Evaluate(elastica, Jval, dJvals);
      
    // Return J and dJ by accessing them from the solution results
    (*J) = Jval;
    double* dJarray;
    ierr = VecGetArray(dJ, &dJarray); CHKERRQ(ierr);
    for(int p=0; p<nLoads; ++p)
      dJarray[p] = dJvals[p];
    ierr = VecRestoreArray(dJ, &dJarray); CHKERRQ(ierr);

    // Print solution details:
    if(solve_params.verbose==true)
      {
	int iter;
	ierr = TaoGetIterationNumber(tao, &iter); CHKERRQ(ierr);
	std::cout<<"\n\nIteration "<<iter<<": ";
	std::cout<<"\nComputed loads: ";
	for(int i=0; i<nLoads; ++i)
	  std::cout<<load_params.values[i]<<" "<<std::flush;
	std::cout<<"\nJ: "<<Jval;
	std::cout<<"\n"<<"dJ: ";
	for(int i=0; i<nLoads; ++i)
	  std::cout<<dJvals[i]<<" "<<std::flush;
	std::cout<<"\n\n"<<std::flush;
      }
	   
    // -- done--
    return 0;
  }


  // Constructor
  Tao_LoadOptimizer::Tao_LoadOptimizer(TendonElastica& istr, ObjectiveFunctional& obj,
				       LoadParams& lp, const std::map<int,double>& bp,
				       const Solve_Params& sp)
    :elastica(istr),
     objective(obj),
     load_params(lp),
     bc_params(bp),
     solve_params(sp),
     nLoads(istr.GetNumLoads())
  {
    // Initialize tao-data structures
    PetscBool flag;
    PetscErrorCode ierr;
    ierr = PetscInitialized(&flag); CHKERRV(ierr);
    assert(flag==PETSC_TRUE && "el::Tao_LoadOptimizer: Petsc not initialized");
    
    // Allocate vectors for the solution and the gradient
    ierr = VecCreateSeq(PETSC_COMM_SELF, nLoads, &x); CHKERRV(ierr);

    // Create the Tao solver
    ierr = TaoCreate(PETSC_COMM_SELF, &tao); CHKERRV(ierr);
    ierr = TaoSetType(tao, TAOLMVM); CHKERRV(ierr);

    // Set the solution vector and initial guess
    ierr = VecSet(x, 0.); CHKERRV(ierr);
    ierr = TaoSetInitialVector(tao, x); CHKERRV(ierr);

    // Set routines for function and gradient evaluations
    ierr = TaoSetObjectiveAndGradientRoutine(tao, Tao_Objective_Function, PETSC_NULLPTR);
    CHKERRV(ierr);

    // Set the application
    ierr = TaoSetApplicationContext(tao, this); CHKERRV(ierr);

    // Check for command line options
    ierr = TaoSetFromOptions(tao); CHKERRV(ierr);
    
    // --- done setting up --- //
  }


  // Destructor
  Tao_LoadOptimizer::~Tao_LoadOptimizer()
  { PetscErrorCode ierr;
    ierr = TaoDestroy(&tao); CHKERRV(ierr);
    ierr = VecDestroy(&x); CHKERRV(ierr);  }

  
  // Main functionality: optimize for a given objective functional
  void Tao_LoadOptimizer::Optimize()
  {
    // Set the initial guess for loads
    PetscErrorCode ierr;
    std::vector<int> indx(nLoads);
    for(int i=0; i<nLoads; ++i) indx[i] = i;
    ierr = VecSetValues(x, nLoads, &indx[0], &load_params.values[0], INSERT_VALUES); CHKERRV(ierr);
    ierr = TaoSetInitialVector(tao, x); CHKERRV(ierr);

    // Optimize
    ierr = TaoSolve(tao); CHKERRV(ierr);

    // Check convergence
    TaoConvergedReason reason;
    ierr = TaoGetConvergedReason(tao, &reason); CHKERRV(ierr);
    if( !(reason==TAO_CONVERGED_GATOL || // Absolute function tolerance
	  reason==TAO_CONVERGED_GRTOL || // Relative function tolerance
	  reason==TAO_CONVERGED_GTTOL || // Relative function tolerance
	  reason==TAO_CONVERGED_STEPTOL || // Step size is small
	  reason==TAO_CONVERGED_MINF ) )    // Threshold value for function reached
      {
	// Solution has diverged
	assert(reason!=TAO_DIVERGED_MAXITS && "Load_Optimize::Optimize- Exceeded max iterations");
	assert(reason!=TAO_DIVERGED_NAN && "Load_Optimize::Optimize- Numerical problems");
	assert(reason!=TAO_DIVERGED_MAXFCN && "Load_Optimize::Optimize- Exceeded max function evaluations");
	assert(reason!=TAO_DIVERGED_LS_FAILURE && "Load_Optimize::Optimize- line search failed");
	assert(reason!=TAO_DIVERGED_TR_REDUCTION && "Load_Optimize::Optimize- trust region failed");
      }
		    
    // Access the solution
    ierr = TaoGetSolutionVector(tao, &x); CHKERRV(ierr);
    ierr = VecGetValues(x, nLoads, &indx[0], &load_params.values[0]); CHKERRV(ierr);
    
    // -- done -- //
    return;
  }
}
