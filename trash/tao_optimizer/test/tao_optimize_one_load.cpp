// Sriramajayam

#include <el_Tao_LoadOptimizer.h>
#include <el_TendonElastica.h>
#include <el_ConstantLoadDirection.h>
#include <el_FollowerLoadDirection.h>
#include <el_TendonLoadDirection.h>
#include <el_Objective_AngleProfile.h>
#include <P11DElement.h>
#include <iostream>
#include <fstream>
#include <random>


// By default: (each can be changed)
// -----------------------------------
// Loads are uniformly spaced on 'PNodes'
// Type of loading is randomly picked in 'LT' from one constant, follower and tendon
// Loading direction and routing points for constant and tendon loads are randomly picked
// Target objective is hard coded in 'ObjFunc'
//------------------------------------

// Objective function
void ObjFunc(const double& x, const double& theta, double* fval, double* dfval, void*);

int main(int argc, char** argv)
{
  std::random_device rd; 
  std::mt19937 gen(rd());
  std::uniform_int_distribution<> lt_dis(0, 2);
    
  // Choose the type of loading (randomly select one among three)
  // ------------------------------------------------------------
  enum class LoadType {Constant, Follower, Tendon};
  const LoadType LT(static_cast<LoadType>(lt_dis(gen)));

  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);

  // Create a uniform 1D mesh on the interval [0,1]
  // -----------------------------------------------
  const int nNodes = 20;
  const double h = 1./static_cast<double>(nNodes-1);
  std::vector<double> coordinates(nNodes);
  for(int n=0; n<nNodes; ++n)
    coordinates[n] = 0.+h*static_cast<double>(n);
  
  // Create one load of the specified type
  // --------------------------------------
  const int nLoads = 1;
  std::vector<int> PNodes({nNodes-1});

  // Create strut
  // -------------
  const double EI = 1.;
  const double origin[] = {0.,0.};
  el::TendonElastica *str = new el::TendonElastica(coordinates, EI, PNodes, origin);
  
  // Initialize load to 0
  el::LoadParams load_params;
  load_params.values.resize(nLoads);
  load_params.dirs.resize(nLoads);
  load_params.values[0] = 0.;
  switch(LT)
    {
      // Constant load direction
    case LoadType::Constant:
      { std::uniform_real_distribution<> dir_dis(-1.,1.);
	double dir[] = {dir_dis(gen), dir_dis(gen)};
	double norm = std::sqrt(dir[0]*dir[0]+dir[1]*dir[1]);
	dir[0] /= norm; dir[1] /= norm;
	std::cout<<"\nLoad type: constant, along the direction ("<<dir[0]<<", "<<dir[1]<<") "<<std::flush;
	load_params.dirs[0] = new el::ConstantLoadDirection(PNodes[0],0,*str,dir);
	break; }

      // Follower load direction
    case LoadType::Follower:
      { std::cout<<"\nLoad type: follower. "<<std::flush;
	load_params.dirs[0] = new el::FollowerLoadDirection(PNodes[0],0,*str); 
	break; }

      // Tendon load direction
    case LoadType::Tendon:
      { std::uniform_real_distribution<> pos_dis(0.1,0.5);
	el::RoutingPost RtPost;
	RtPost.center[0] = 0.;
	RtPost.center[1] = pos_dis(gen);
	RtPost.radius = 0.; RtPost.orientation = 0.;
	std::cout<<"\nLoad type: tendon, with routing point: ("<<RtPost.center[0]<<", "
		 <<RtPost.center[1]<<") "<<std::flush;
	load_params.dirs[0] = new el::TendonLoadDirection(PNodes[0],0,*str,RtPost);
	break;}

    default: assert(false && "Unexpected loading type");
    }
      
  // Dirichlet BCs: clamp the left end
  // ----------------------------------
  std::map<int,double> bc_params{};
  bc_params[0] = 0.;

  // Solve parameters
  // ------------------
  el::Solve_Params solve_params({
      .consistentLinearization = false, // By default, switch on if necessary
	.EPS = 1.e-10,
	.resScale = 1.,
	.dofScale = 1.,
	.nMaxIters = 25,
	.verbose = true});

  // Objective function and parameters
  // ----------------------------------
  std::function<decltype(ObjFunc)> obj_func_ptr = ObjFunc;
  el::Objective_AngleProfile objective(obj_func_ptr);
  
  // Create load optimizer
  // ----------------------
  auto* Opt = new el::Tao_LoadOptimizer(*str, objective, load_params, bc_params, solve_params);

  
  // Optimize
  // ---------
  Opt->Optimize();

  // Print the optimal load and the corresponding solution
  // ------------------------------------------------------
  std::cout<<"\n\nOptimal load: "<<load_params.values[0];
  const auto& state = str->GetStateField();
  const auto& xy = str->GetCartesianCoordinates();

  std::fstream stream;
  stream.open("optstate.dat", std::ios::out);
  for(int n=0; n<nNodes; ++n)
    stream << coordinates[n] << " "<<state[n]<<" "<<xy[2*n]<<" "<<xy[2*n+1]<<"\n";
  stream.close();
  
  // Clean up
  delete str;
  delete Opt;
  for(auto& x:load_params.dirs) delete x;
  PetscFinalize();
}


// Objective function
void ObjFunc(const double& s, const double& theta, double* fval, double* dfval, void* params)
{
  // target function: 0.1*s*(s-2)
  double target = 0.1*s*(s-2.);
  
  if(fval!=nullptr)
    (*fval) = 0.5*(theta-target)*(theta-target);
  if(dfval!=nullptr)
    (*dfval) = (theta-target);
  return;
}
