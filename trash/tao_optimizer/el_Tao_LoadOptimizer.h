// Sriramajayam

#ifndef EL_LOAD_OPTIMIZER_H
#define EL_LOAD_OPTIMIZER_H

#include <el_TendonElastica.h>
#include <el_ObjectiveFunctional.h>
#include <petsctao.h>

namespace el
{
  //! Optimize loads for a given shape
  class Tao_LoadOptimizer
  {
  public:
    //! Constructor
    //! \param[in] istr Structure object. Referred to.
    Tao_LoadOptimizer(TendonElastica& istr, ObjectiveFunctional& obj,
		      LoadParams& lp, const std::map<int,double>& bp,
		      const Solve_Params& sp);
    
    //! Destructor
    virtual ~Tao_LoadOptimizer();

    //! Disable copy and assignment
    Tao_LoadOptimizer(const Tao_LoadOptimizer&) = delete;
    Tao_LoadOptimizer& operator=(const Tao_LoadOptimizer&) = delete;

    //! Access the structure
    TendonElastica* GetStructure() const;

    //! Access the TAO structure
    inline Tao& GetTaoContext()
    { return tao; }
    
    //! Optimize for a given configuration
    //! \param[in,out] specs Specification for loads & bcs
    //! \param[in] Obj Objective functional
    //! \param[in] solve_params Solver parameters
    //! \param[out] result Optimized solution details
    void Optimize();
    
  private:
    TendonElastica& elastica; //!< Pointer to the structure
    ObjectiveFunctional& objective; //!< Objective functional
    LoadParams& load_params; //!< Load values and directions
    const std::map<int,double>& bc_params; //!< Dirichlet BCs
    const Solve_Params& solve_params; //!< Parameters for state and sensitivity calcs
    const int nLoads; //!< Number of design variables
    Tao tao; //!< Tao context
    Vec x; //!< Solution vector (loads)

    // friend
    friend PetscErrorCode Tao_Objective_Function(Tao, Vec, PetscReal*, Vec, void*);
  };
}

#endif  
