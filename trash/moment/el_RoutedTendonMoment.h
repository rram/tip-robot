// Sriramajayam

#ifndef EL_ROUTED_TENDON_MOMENT_H
#define EL_ROUTED_TENDON_MOMENT_H

#include  <el_MomentFunctional.h>
#include <el_RoutedTendonDirection.h>

namespace el
{
  //! Class defining moments arising from a routed tendon
  //! Functional is of the form P*M[theta]
  class RoutedTendonMoment: public MomentFunctional
  {
  public:
    //! Constructor
    //! \param[in] ld Load direction functional
    RoutedTendonMoment(const LoadDirection* ld);

    //! Destructor
    inline virtual ~RoutedTendonMoment() {}

    //! Copy constructor
    //! \param[in] obj Object to be copied
    inline RoutedTendonMoment(const RoutedTendonMoment& obj)
      :MomentFunctional(obj),
      tendon_dir(obj.tendon_dir),
      ElmIndex(obj.ElmIndex),
      Hvals(obj.Hvals),
      Vvals(obj.Vvals),
      dHvals(obj.dHvals),
      dVvals(obj.dVvals) {}

    //! Returns the tendon direction
    inline const RoutedTendonDirection* GetTendonDirection() const
    { return tendon_dir; }

    //! Main functionality
    //! Update the moment functional, its sensitivities and directional derivatives
    virtual void UpdateFunctional(const LoadDirectionParams* obj, const double load_value) override;

  private:
    const RoutedTendonDirection* tendon_dir;
    std::vector<int> ElmIndex; //!< Element indices to access variations
    mutable std::vector<double> Hvals, Vvals; //!< Computing direction cosines
    mutable std::vector<std::vector<double>> dHvals, dVvals; //!< Sensitivities of direction cosines
  };
  
}

#endif
