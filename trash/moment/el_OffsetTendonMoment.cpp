// Sriramajayam

#include <el_OffsetTendonMoment.h>
#include <el_Utils.h>
#include <el_TendonElastica_Structs.h>

namespace el
{
  
  // Main functionality
  // Update the moment functional, its sensitivities and
  // directional derivatives
  void OffsetTendonMoment::
  UpdateFunctional(const LoadDirectionParams* params, const double PVal)
  { 
    // Moment = r[theta] x F[theta]
    // F[theta] = P(H[theta] Ex + V[theta] Ey)
    // r[theta] = offset(-sin(thetaN) Ex + cos(thetaN) Ey) -> moment arm

    // Access the elastica
    const auto& theta = *(params->theta);
    const auto& alphaVec = *(params->alphaVec);
    const int nLoads = tendon_dir->GetNumLoads();
    
    // Sanity checks
    assert(static_cast<int>(alphaVec.size())==nLoads);
    assert(static_cast<int>(FuncVal.size())==1 &&
	   FuncVal.begin()->first==MNode && 
	   static_cast<int>(dFuncVal.size())==1 &&
	   dFuncVal.begin()->first==MNode && 
	   static_cast<int>(dFuncVal.begin()->second.size())==nLoads);
    
	   // Load number corresponding to this moment
    const int LoadNum = tendon_dir->GetLoadNumber();
    
    // Consistency of node number corresponding to this load
    const int PNode = tendon_dir->GetLoadingNode();
    
    // Theta at the loaded node
    const double ThetaN = theta.Get(PNode)[0];

    // Moment arm
    const double offset = tendon_dir->GetOffset();
    const double rvec[] = {-offset*std::sin(ThetaN),
			   offset*std::cos(ThetaN) };

    // Tendon force functional: elm = PNode-1
    const double HVal = tendon_dir->GetDirection(PNode-1, DirCosine::Horizontal);
    const double VVal = tendon_dir->GetDirection(PNode-1, DirCosine::Vertical);

    // Moment functional: rvec X F
    double& FuncValue = FuncVal[MNode];
    FuncValue = PVal*(rvec[0]*VVal - rvec[1]*HVal);

    // Sensitivity of moment to loads
    // dFuncVal/dPL = drvec/dPL x F + rvec x dF/dPL
    const auto& dHVal = tendon_dir->GetDirectionSensitivity(PNode-1, DirCosine::Horizontal);
    const auto& dVVal = tendon_dir->GetDirectionSensitivity(PNode-1, DirCosine::Vertical);
    std::vector<double>& dFuncValue = dFuncVal[MNode];
    for(int L=0; L<nLoads; ++L)
      {
	const double alphaVal = alphaVec[L]->Get(PNode)[0];
	const double drvec[] = {-offset*std::cos(ThetaN)*alphaVal,
				-offset*std::sin(ThetaN)*alphaVal};
	dFuncValue[L] =
	  PVal*(drvec[0]*VVal + rvec[0]*dVVal[L]) -
	  PVal*(drvec[1]*HVal + rvec[1]*dHVal[L]);
      }
    dFuncValue[LoadNum] += rvec[0]*VVal-rvec[1]*HVal; // Contribution from the 'P' factor multiplying the moment

    // Directional derivatives of moments
    // var_FuncVal = var_rvec x F + rvec x var_F
    const auto& var_HVal = tendon_dir->GetDirectionalDerivatives(DirCosine::Horizontal)[0];
    const auto& var_VVal = tendon_dir->GetDirectionalDerivatives(DirCosine::Vertical)[0];
    const auto& var_indices = tendon_dir->GetDirectionalDerivativeIndices()[0];
    const int nVars = static_cast<int>(var_indices.size());
    assert(nVars==PNode+1);
    std::vector<double>& dir_deriv_FuncValue = dir_deriv_FuncVal[PNode];
    std::vector<int>& dir_deriv_indices = dir_deriv_index[PNode];
    if(static_cast<int>(dir_deriv_FuncValue.size())<nVars)
      dir_deriv_FuncValue.resize(nVars);
    std::fill(dir_deriv_FuncValue.begin(), dir_deriv_FuncValue.end(), 0.);
    if(static_cast<int>(dir_deriv_indices.size())<nVars)
      dir_deriv_indices.resize(nVars);
    for(int i=0; i<nVars; ++i)
      dir_deriv_indices[i] = var_indices[i];
    
    // Terms of the form rvec x var_F
    for(int a=0; a<PNode+1; ++a)
      dir_deriv_FuncValue[a] = PVal*(rvec[0]*var_VVal[a] - rvec[1]*var_HVal[a]);
    
    // Terms of the form var_rvec x F
    // var_rvec is nonzero only for PNode
    { const double var_rvec[] = {-offset*std::cos(ThetaN),
				 -offset*std::sin(ThetaN)};
      dir_deriv_FuncValue[PNode] += PVal*(var_rvec[0]*VVal - var_rvec[1]*HVal); }

    // Scale moment values, sensitivities and variations by -1 to shift terms to LHS
    FuncValue *= -1.;
    for(auto& val:dFuncValue) val *= -1.;
    for(auto& val:dir_deriv_FuncValue) val *= -1.;
    
    // -- done --
    return;
  }
}
