// Sriramajayam

#include <el_RoutedTendonMoment.h>
#include <el_Utils.h>
#include <el_TendonElastica_Structs.h>
#include <cmath>

namespace el
{
  // Constructor
  RoutedTendonMoment::RoutedTendonMoment(const LoadDirection* ld)
    :MomentFunctional(ld->GetNumLoads()),
     tendon_dir(static_cast<const RoutedTendonDirection*>(ld))
  {
    assert(tendon_dir!=nullptr && "el::RoutedTendonMoment-could not case load direction.");

    // Outputs funtional and sensitivities
    const auto& RtNodes = tendon_dir->GetRoutingNodes();
    const int nRtNodes = static_cast<int>(RtNodes.size());
    const int nLoads = tendon_dir->GetNumLoads();
    FuncVal.clear();
    dFuncVal.clear();
    for(auto& n:RtNodes)
      {
	FuncVal[n] = 0.;
	dFuncVal[n] = std::vector<double>(nLoads);
      }

    // Output directional derivatives
    const auto& var_indices = tendon_dir->GetDirectionalDerivativeIndices();
    assert(static_cast<int>(var_indices.size())==nRtNodes);
    dir_deriv_FuncVal.clear();
    dir_deriv_index.clear();
    for(int i=0; i<nRtNodes; ++i)
      { dir_deriv_index[RtNodes[i]] = var_indices[i];
	dir_deriv_FuncVal[RtNodes[i]] = std::vector<double>(var_indices[i].size());  }

    // Element indices to access variations
    ElmIndex.resize(nRtNodes);
    const auto& ElmIntervals = tendon_dir->GetElementIntervals();
    for(int i=0; i<nRtNodes; ++i)
      { assert(ElmIntervals[i].second==RtNodes[i]-1);
	ElmIndex[i] = i; }

    // Resize temporaries
    Hvals.resize(nRtNodes);
    Vvals.resize(nRtNodes);
    dHvals.resize(nRtNodes);
    dVvals.resize(nRtNodes);
    for(auto& x:dHvals) x.resize(nLoads);
    for(auto& x:dVvals) x.resize(nLoads);
    
    // done
  }

  
  // Update the moment functional, its sensitivities and directional derivatives
  void RoutedTendonMoment::
  UpdateFunctional(const LoadDirectionParams* params, const double Pval)
  {
    // Moments = rxF, where r -> moment arm, F = P(H,V)

    // Access the elastica
    assert(params!=nullptr && "el::RoutedTendonMoment::UpdateFunctional- parameters are null");
    const auto& theta = *(params->theta);
    const auto& alphaVec = *(params->alphaVec);
    const int nLoads = tendon_dir->GetNumLoads();
    assert(static_cast<int>(alphaVec.size())==nLoads);

    // Load number corresponding to this moment
    const int LoadNum = tendon_dir->GetLoadNumber();

    // Routing nodes
    const auto& RtNodes = tendon_dir->GetRoutingNodes();
    const int nRtNodes = static_cast<int>(RtNodes.size());

    // Offsets
    const auto& Offsets = tendon_dir->GetOffsets();

    // Access direction cosine variations (vector<vectors>)
    const auto& var_indices = tendon_dir->GetDirectionalDerivativeIndices();
    const auto& var_Hvals = tendon_dir->GetDirectionalDerivatives(DirCosine::Horizontal);
    const auto& var_Vvals = tendon_dir->GetDirectionalDerivatives(DirCosine::Vertical);

    // Sanity checks on sizes
    assert(static_cast<int>(FuncVal.size())==nRtNodes);
    assert(static_cast<int>(dFuncVal.size())==nRtNodes);
    assert(static_cast<int>(dir_deriv_index.size())==nRtNodes);
    assert(static_cast<int>(dir_deriv_FuncVal.size())==nRtNodes);
    for(auto& it:dFuncVal)
      assert(static_cast<int>(it.second.size())==nLoads);
    assert(static_cast<int>(dir_deriv_index.size())==nRtNodes);
    for(int i=0; i<nRtNodes; ++i)
      {
	const int& elmIndex = ElmIndex[i];

	// Size of indices
	auto it = dir_deriv_index.find(RtNodes[i]);
	assert(it!=dir_deriv_index.end());
	assert(it->second.size()==var_indices[elmIndex].size());

	// Size of variations
	auto jt = dir_deriv_FuncVal.find(RtNodes[i]);
	assert(jt!=dir_deriv_FuncVal.end());
	assert(jt->second.size()==var_indices[elmIndex].size());
      }
    assert(static_cast<int>(Hvals.size())==nRtNodes && static_cast<int>(Vvals.size())==nRtNodes);
    assert(static_cast<int>(dHvals.size())==nRtNodes && static_cast<int>(dVvals.size())==nRtNodes);
    for(int i=0; i<nRtNodes; ++i)
      { assert(static_cast<int>(dHvals[i].size())==nLoads);
	assert(static_cast<int>(dVvals[i].size())==nLoads); }

    // Initialize variations to 0.
    for(auto& it:dir_deriv_FuncVal)
      std::fill(it.second.begin(), it.second.end(), 0.);

    // Copy indices of variations
    for(int i=0; i<nRtNodes; ++i)
      {
	// Indices from tendon loading
	const auto& indxlist = var_indices[ElmIndex[i]];
	const int nVars = static_cast<int>(indxlist.size());

	auto it = dir_deriv_index.find(RtNodes[i]);
	assert(it!=dir_deriv_index.end());
	for(int j=0; j<nVars; ++j)
	  it->second[j] = indxlist[j];
      }

    // Direction cosines (Hvals, Vvals)
    // RoutedTendonDirection computes
    // |---0---(H0+H1+H2+..,V0+V1+V2+..)---1---P(H1+H2+..,V1+V2+..)---2--...---n---(Hn,Vn)---
    // Moments are based on
    // |---0---(H0,V0)---1---P(H1,V1)---2--...---n---(Hn,Vn)---
    // Do a telescopic subtraction
    for(int i=0; i<nRtNodes; ++i)
      {
	Hvals[i] = tendon_dir->GetDirection(RtNodes[i]-1,DirCosine::Horizontal);
	Vvals[i] = tendon_dir->GetDirection(RtNodes[i]-1,DirCosine::Vertical);
      }
    // Subtract
    for(int i=0; i<nRtNodes-1; ++i)
      {
	Hvals[i] -= tendon_dir->GetDirection(RtNodes[i+1]-1,DirCosine::Horizontal);
	Vvals[i] -= tendon_dir->GetDirection(RtNodes[i+1]-1,DirCosine::Vertical);
      }

    // Telescopic subtraction for sensitivities
    for(int i=0; i<nRtNodes; ++i)
      {
	const auto& nodal_dHvals = tendon_dir->GetDirectionSensitivity(RtNodes[i]-1, DirCosine::Horizontal);
	const auto& nodal_dVvals = tendon_dir->GetDirectionSensitivity(RtNodes[i]-1, DirCosine::Vertical);
	for(int L=0; L<nLoads; ++L)
	  {
	    dHvals[i][L] = nodal_dHvals[L];
	    dVvals[i][L] = nodal_dVvals[L];
	  }
      }
    for(int i=0; i<nRtNodes-1; ++i)
      {
	const auto& nodal_dHvals = tendon_dir->GetDirectionSensitivity(RtNodes[i+1]-1, DirCosine::Horizontal);
	const auto& nodal_dVvals = tendon_dir->GetDirectionSensitivity(RtNodes[i+1]-1, DirCosine::Vertical);
	for(int L=0; L<nLoads; ++L)
	  {
	    dHvals[i][L] -= nodal_dHvals[L];
	    dVvals[i][L] -= nodal_dVvals[L];
	  }
      }
    
    
    // Update functional and sensitivities
    for(int i=0; i<nRtNodes; ++i)
      {
	// This node
	const int& node = RtNodes[i];
	
	// Theta here
	const double& thetaN = theta.Get(node)[0];
	// Offset here
	const double& offset = Offsets[i];
	
	// Moment arm
	const double rvec[] = {-offset*std::sin(thetaN), offset*std::cos(thetaN)};

	// Direction cosines: use element = node-1
	const double& Hval = Hvals[i];
	const double& Vval = Vvals[i];

	// Moment value
	double& Mval = FuncVal.find(node)->second;
	Mval = Pval*(rvec[0]*Vval - rvec[1]*Hval);

	// Sensitivity of moments to loads
	// dMval/dPL = drvec/dPL x F + rvec x dF/dPL
	const auto& dHval = dHvals[i];
	const auto& dVval = dVvals[i];

	auto& dMval = dFuncVal.find(node)->second;
	for(int L=0; L<nLoads; ++L)
	  {
	    const double& alphaVal = alphaVec[L]->Get(node)[0];
	    const double drvec[] = {-offset*std::cos(thetaN)*alphaVal,
				    -offset*std::sin(thetaN)*alphaVal};
	    dMval[L] =
	      Pval*(drvec[0]*Vval    - drvec[1]*Hval) +   // dr x F
	      Pval*(rvec[0]*dVval[L] - rvec[1]*dHval[L]); // r x dF
	  }
	dMval[LoadNum] += rvec[0]*Vval-rvec[1]*Hval; // Contribution from 'P' factor in F

	// Compute variations
	auto ft = dir_deriv_FuncVal.find(node);
	assert(ft!=dir_deriv_FuncVal.end());
	auto& var_Func = ft->second;
	
	// var(r x F) = var(r) x F +  r x var(F)
	const auto& var_Hval = var_Hvals[ElmIndex[i]];
	const auto& var_Vval = var_Vvals[ElmIndex[i]];
	const auto& var_indx = var_indices[ElmIndex[i]];
	const int nVars = static_cast<int>(var_indx.size());
	for(int a=0; a<nVars; ++a)
	  var_Func[a] += Pval*(rvec[0]*var_Vval[a]-rvec[1]*var_Hval[a]);
	
	// Add the term var(r) X F, which is non-zero only for dof = "node"
	assert(var_indx[nVars-1]==node);
	const double var_rvec[] = {-offset*std::cos(thetaN), -offset*std::sin(thetaN)};
	var_Func[nVars-1] += Pval*(var_rvec[0]*Vval-var_rvec[1]*Hval);
      }

    // Scale moment values, sensitivities and variations by -1 to shift terms to LHS
    for(auto& it:FuncVal) it.second *= -1.;
    for(auto& it:dFuncVal)
      for(auto& val:it.second)
	val *= -1.;
    for(auto& it:dir_deriv_FuncVal)
      for(auto& val:it.second)
	val *= -1.;
    
    // done
    return;
  }

}
