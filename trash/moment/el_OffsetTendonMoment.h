// Sriramajayam

#ifndef EL_OFFSET_TENDON_MOMENT_H
#define EL_OFFSET_TENDON_MOMENT_H

#include <el_MomentFunctional.h>
#include <el_OffsetTendonLoadDirection.h>

namespace el
{
  //! Class defining moments arising from offset tendons
  //! Functional is of the form P*M[theta].
  class OffsetTendonMoment: public MomentFunctional
  {
  public:
    //! Constructor
    //! \param[in] ld Load direction functional
    inline OffsetTendonMoment(const LoadDirection* ld)
      :MomentFunctional(ld->GetNumLoads()),
      tendon_dir(static_cast<const OffsetTendonLoadDirection*>(ld)),
      MNode(tendon_dir->GetLoadingNode())
	{
	  assert(MNode>=0 && "el::ConstantMoment: Unexpected moment node");
	  assert(tendon_dir!=nullptr &&
		 "el::OffsetTendonMoment- could not cast load direction to type OffsetTendonLoadDirection");
	  FuncVal.clear();
	  FuncVal[MNode] = 0.;
	  dFuncVal.clear();
	  dFuncVal[MNode] = std::vector<double>(nLoads,0.);
	  dir_deriv_FuncVal.clear();
	  dir_deriv_FuncVal[MNode] = std::vector<double>({});
	  dir_deriv_index.clear();
	  dir_deriv_index[MNode] = std::vector<int>({});
	}
    
    //! Destructor
    inline virtual ~OffsetTendonMoment() {}
    
    //! Copy constructor
    //! \param[in] obj Object to be copied
    inline OffsetTendonMoment(const OffsetTendonMoment& obj)
      :MomentFunctional(obj),
      tendon_dir(obj.tendon_dir),
      MNode(obj.MNode) {}

    //! Returns the tendon direction
    inline const OffsetTendonLoadDirection* GetTendonDirection() const
    { return tendon_dir; }

    //! Returns the node at which moment is applied
    inline int GetMomentNode() const
    { return MNode; }
    
    //! Main functionality
    //! Update the moment functional, its sensitivities and
    //! directional derivatives
    virtual void UpdateFunctional(const LoadDirectionParams* obj, const double load_value) override;
    
  private:
    const OffsetTendonLoadDirection* tendon_dir; //! Underlying offset tendon direction object
    const int MNode; //!< Node at which moment is applied
  };
}

#endif
