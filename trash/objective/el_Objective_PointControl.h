// Sriramajayam

#ifndef EL_OBJECTIVE_POINT_CONTROL_H
#define EL_OBJECTIVE_POINT_CONTROL_H

#include <el_ObjectiveFunctional.h>

namespace el
{
  // Objective of the form:
  //2J = Coef_x (x_offset-x_tgt)^2 + Coef_y (y_offset-y_tgt)^2 + Coef_theta (theta-theta_tgt)^2 +
  //     + Coef_P[0](P[0]-P_tgt[0])^2 + ..

  //! Helper struct to specify weights and targets
  struct Objective_PointControlSpec
  {
    int nodenum; // Node to control
    double offset_rad, offset_angle; // Offset radius and angle of point to control from the attachment node
    double x_tgt, y_tgt;  // Targeted X, Y
    double Coef_x; // Coefficient multiplying x
    double Coef_y; // Coefficient multiplying y
    double theta_tgt; // Targeted theta
    double Coef_theta; // Coefficient multiplying theta
    std::vector<double> P_tgt; // Reference loads
    std::vector<double> Coef_P; // Coefficient multiplying loads
    inline Objective_PointControlSpec()
      :nodenum(-1),
      offset_rad(0.), offset_angle(0.),
      x_tgt(0.), y_tgt(0.), Coef_x(0.), Coef_y(0.),
      theta_tgt(0.), Coef_theta(0.),
      P_tgt({}), Coef_P({}) {}
  };
  
  //! Objective functional that enforces control over a node
  class Objective_PointControl: public ObjectiveFunctional
  {
  public:
    //! Constructor
    inline Objective_PointControl()
      :ObjectiveFunctional(), is_set(false) {}

    //! Destructor
    inline virtual ~Objective_PointControl() {}
    
    // Disable copy and assignment
    Objective_PointControl(const Objective_PointControl&) = delete;
    Objective_PointControl operator=(const Objective_PointControl&) = delete;
      
    //! Set the objective parameters
    //! \param[in] node Node to control
    void Set(const Objective_PointControlSpec& newspec);
    
    //! Main functionality: implement objective evaluation
    //! \param[in] theta State at which to evaluate functional
    //! \param[in] params Parameters to aid in evaluation
    //! \param[out[ Jval Computed functional value
    virtual void Evaluate(const TendonElastica& elastica, double& Jval, void* params=nullptr);

    //! Main functionality:
    //! Evaluate the objective and its sensitivities wrt each load
    //! \param[in] theta State at which to evaluate functional
    //! \param[in] alphaVec Sensitivities
    //! \param[in] params Parameters to aid in evaluation
    //! \param[out] Jval Computed functional value
    //! \param[out] dJval Computed functional gradient
    virtual void Evaluate(const TendonElastica& elastica, 
			  double& Jval, std::vector<double>& dJvals, void* params=nullptr);
    
  private:
    mutable bool is_set;
    Objective_PointControlSpec spec;
  };
}

#endif
