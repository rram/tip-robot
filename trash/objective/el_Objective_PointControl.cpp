// Sriramajayam

#include <el_Objective_PointControl.h>
#include <cassert>
#include <cmath>
#include <iostream>

namespace el
{
  // Set the objective parameters
  void Objective_PointControl::Set(const Objective_PointControlSpec& newspec)
  {
    // Copy the objective specifications
    spec.nodenum = newspec.nodenum;
    spec.offset_rad = newspec.offset_rad;
    spec.offset_angle = newspec.offset_angle;
    spec.x_tgt = newspec.x_tgt;
    spec.y_tgt = newspec.y_tgt;
    spec.theta_tgt = newspec.theta_tgt;
    spec.P_tgt = newspec.P_tgt;
    spec.Coef_x = newspec.Coef_x;
    spec.Coef_y = newspec.Coef_y;
    spec.Coef_theta = newspec.Coef_theta;
    spec.Coef_P = newspec.Coef_P;

    // Sanity checks
    assert(spec.nodenum>=0 && "el::Objective_PointControl: Unspecified control node");
    //assert(spec.Coef_x>0. || spec.Coef_y>0. || spec.Coef_theta>0.);
    assert(spec.Coef_P.size()==spec.P_tgt.size());

    is_set = true;
    return;
  }
    
  // Main functionality: implement objective evaluation
  void Objective_PointControl::Evaluate(const TendonElastica& elastica, double& Jval, void* params)
  {
    assert(params==nullptr);
    
    // Check that the objective specs have been set
    assert(is_set && "el::Objective_PointControl::Evaluate- Specs not set.");
    
    // Access
    const auto& theta = elastica.GetStateField();
    const auto& xy = elastica.GetCartesianCoordinates();
    
    // Values of x, y, theta at the specified node
    const int node = spec.nodenum;
    const double x_val = xy[2*node];
    const double y_val = xy[2*node+1];
    const double theta_val = theta[node];

    // Values of x and y at offset point
    const double x_offset = x_val + spec.offset_rad*std::cos(theta_val+spec.offset_angle);
    const double y_offset = y_val + spec.offset_rad*std::sin(theta_val+spec.offset_angle);

    // Evaluate objective
    Jval = 0.5*( spec.Coef_x*(x_offset-spec.x_tgt)*(x_offset-spec.x_tgt) +
		 spec.Coef_y*(y_offset-spec.y_tgt)*(y_offset-spec.y_tgt) +
		 spec.Coef_theta*(theta_val-spec.theta_tgt)*(theta_val-spec.theta_tgt) );

    // Regularization
    const int nLoads = static_cast<int>(loads.size());
    assert(static_cast<int>(spec.P_tgt.size())==nLoads && static_cast<int>(spec.Coef_P.size())==nLoads);
    for(int L=0; L<nLoads; ++L)
      Jval += 0.5*spec.Coef_P[L]*(loads[L]-spec.P_tgt[L])*(loads[L]-spec.P_tgt[L]);

    // Done
    return;
  }


  // Main functionality: evaluate objective and derivatives
   // Main functionality: implement objective evaluation
  void Objective_PointControl::Evaluate(const TendonElastica& elastic,
					double& Jval, std::vector<double>& dJvals, void* params)
  {
    assert(params==nullptr);
    
    // Check that the objective specs have been set
    assert(is_set==true && "el::Objective_PointControl:: Specs not set");

    // Initialize
    Jval = 0.;
    const int nLoads = static_cast<int>(alphaVec.size());
    assert(nLoads>0);
    if(static_cast<int>(dJvals.size())<nLoads) dJvals.resize(nLoads);
    std::fill(dJvals.begin(), dJvals.end(), 0.);
    assert(static_cast<int>(spec.P_tgt.size())==nLoads &&
	   static_cast<int>(spec.Coef_P.size())==nLoads);

    // Access
    const auto& theta = elastica.GetStateField();
    const auto& xy = elastica.GetCartesianCoordinates();
    const auto& alphaVec = elastica.GetSensitivityFields();
    const auto& xy_sensitivity = elastica.GetCoordinateSensitivities();

    params.Check();
    const auto& load_params = *params.load_params;
    const auto& loads = load_params.values;
    const int node = spec.nodenum;

    // Compute Cartesian coordinates
    std::vector<double> xy(2*nTotalDof);
    std::fill(xy.begin(), xy.end(), 0.);
    AngleToCartesianMap(theta, ElmArray, L2GMap, xy);

    // Evaluate sensitivities of (x,y) coordinates of the specified node
    std::vector<double> int_cos_alpha(nLoads), int_sin_alpha(nLoads);
    std::fill(int_cos_alpha.begin(), int_cos_alpha.end(), 0.);
    std::fill(int_sin_alpha.begin(), int_sin_alpha.end(), 0.);
    for(int e=0; e<node; ++e)
      {
	const auto& Qwts = ElmArray[e]->GetIntegrationWeights(0);
	const int nQuad = static_cast<int>(Qwts.size());
	const int nDof = ElmArray[e]->GetDof(0);
	const auto& ShpVals = ElmArray[e]->GetShape(0);

	for(int q=0; q<nQuad; ++q)
	  {
	    // Theta value here
	    double thetaval = 0.;
	    for(int a=0; a<nDof; ++a)
	      thetaval += theta[L2GMap.Map(0,a,e)]*ShpVals[q*nDof+a];
	    
	    // Update integrals for each load
	    for(int L=0; L<nLoads; ++L)
	      {
		// alpha value here
		double alphaval = 0.;
		for(int a=0; a<nDof; ++a)
		  alphaval += alphaVec[L][L2GMap.Map(0,a,e)]*ShpVals[q*nDof+a];

		int_cos_alpha[L] += Qwts[q]*std::cos(thetaval)*alphaval;
		int_sin_alpha[L] += Qwts[q]*std::sin(thetaval)*alphaval;
	      }
	  }
      }

    // Compute the functional
    const double x_val = xy[2*node];
    const double y_val = xy[2*node+1];
    const double theta_val = theta[node];
    const double x_offset = x_val + spec.offset_rad*std::cos(theta_val+spec.offset_angle);
    const double y_offset = y_val + spec.offset_rad*std::sin(theta_val+spec.offset_angle);
    Jval = 0.5*( spec.Coef_x*(x_offset-spec.x_tgt)*(x_offset-spec.x_tgt) +
		 spec.Coef_y*(y_offset-spec.y_tgt)*(y_offset-spec.y_tgt) +
    		 spec.Coef_theta*(theta_val-spec.theta_tgt)*(theta_val-spec.theta_tgt) );

    // Regularization
    for(int L=0; L<nLoads; ++L)
      Jval += 0.5*spec.Coef_P[L]*(loads[L]-spec.P_tgt[L])*(loads[L]-spec.P_tgt[L]);
    

    // Sensitivities of the functional
    for(int L=0; L<nLoads; ++L)
      {
	dJvals[L] -= spec.Coef_x*(x_offset-spec.x_tgt)*(int_sin_alpha[L]-spec.offset_rad*std::sin(theta_val+spec.offset_angle)*alphaVec[L][node]); // x
	dJvals[L] += spec.Coef_y*(y_offset-spec.y_tgt)*(int_cos_alpha[L]+spec.offset_rad*std::cos(theta_val+spec.offset_angle)*alphaVec[L][node]); // y
	dJvals[L] += spec.Coef_theta*(theta_val-spec.theta_tgt)*alphaVec[L][node]; // theta
	dJvals[L] += spec.Coef_P[L]*(loads[L]-spec.P_tgt[L]); // regularization
      }
    
    // done
    return;
  }
    
}
