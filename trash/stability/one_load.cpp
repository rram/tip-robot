// Sriramajayam

#include <el_TendonLoadDirection.h>
#include <el_TendonElastica.h>
#include <el_ConstantLoadDirection.h>
#include <el_P11DElement.h>
#include <el_TendonElastica_State_SNESFuncs.h>
#include <el_OffsetTendonLoadDirection.h>
#include <el_OffsetTendonMoment.h>
#include <random>
#include <iostream>
#include <fstream>
#include <slepceps.h>
#include <el_EigenSolver.h>

int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);

  // Create a uniform 1D mesh on the interval [0,1]
  // ------------------------------------------------
  const int nNodes = 181;
  const double h = 0.6/static_cast<double>(nNodes-1);
  std::vector<double> coordinates(nNodes);
  for(int n=0; n<nNodes; ++n)
    coordinates[n] = 0.+h*static_cast<double>(n);
  const int nElements = nNodes-1;
  std::vector<int> connectivity(2*nElements);
  for(int e=0; e<nElements; ++e)
    { connectivity[2*e] = e+1;
      connectivity[2*e+1] = e+2; }
  Segment<1>::SetGlobalCoordinatesArray(coordinates);
  std::cout<<"\nNumber of elements: "<<nElements<<std::flush;

  // Create routing posts
  const int nLoads = 2;
  const double RtPoints[] = {0.046334, -0.097126, 0.02611, 0.051106};
  std::vector<el::RoutingPost> RtPosts(nLoads);
  for(int i=0; i<nLoads; ++i)
    { RtPosts[i].center[0] = RtPoints[nLoads*i+0];
      RtPosts[i].center[1] = RtPoints[nLoads*i+1];
      RtPosts[i].radius = 0.;
      RtPosts[i].orientation = 0.;
    }
  std::vector<double> offsets({-0.0,0.0});

  
  // Create load of the specified type
  // --------------------------------------
  std::vector<int> PNodes(nLoads);
  PNodes[0] = 0.2/h;
  PNodes[1] = nNodes-1;
  el::LoadParams load_params;
  load_params.values.resize(nLoads);
  load_params.dirs.resize(nLoads);
  load_params.moments.resize(nLoads);
    // Create loading directions & moments
  for(int i=0; i<nLoads; ++i)
    {
      load_params.dirs[i] = new el::OffsetTendonLoadDirection(PNodes[i], i, nLoads, offsets[i], RtPosts[i]);
      load_params.moments[i] = new el::OffsetTendonMoment(load_params.dirs[i]);
    }

  
  // Dirichlet BCs: clamp the left end
  // ----------------------------------
  el::DirichletBCs bc_params;
  bc_params.dofs = std::vector<int>({0});
  bc_params.values = std::vector<double>({0.});
  
  
  // Solve parameters
  // ------------------
  el::Solve_Params solve_params({
      .consistentLinearization = true, // Automatically consistent
	.EPS = 1.e-10,
	.resScale = 1.,
	.dofScale = 1.,
	.nMaxIters = 25,
	.verbose = true});

  // Create strut
  // --------------
  const double EI = 1.;
  el::TendonElastica* str = new el::TendonElastica(coordinates, connectivity, EI, PNodes);

  // Load up to a point
  load_params.values[0] = 6.;
  load_params.values[1] = 22.;

  // Compute the state 
  // ------------------
  str->ComputeState(load_params, bc_params, solve_params);
  const double* theta = &(str->GetStateField().Get()[0]);

  // Get access to the stiffness matrix at the converged state
  el::detail::TendonElasticaInternals internals;
  str->PackageInternals(internals);
  std::vector<int> nnz;
  internals.state_Asm->CountNonzeros(nnz);

  // Create solver
  el::SNESSolver solver;
  solver.Initialize(nnz, el::detail::State_Residual_Func, el::detail::State_Jacobian_Func);

  // Tendon elastica context
  el::detail::TendonElasticaContext ctx;
  ctx.internals = &internals;
  ctx.load_params = &load_params;
  ctx.bc_params = &bc_params;
  ctx.solve_params = &solve_params;
  SNESSetApplicationContext(solver.snes, &ctx);
    
  // Assemble the stiffness matrix at the converged configuration
  std::vector<int> indx(nNodes);
  for(int i=0; i<nNodes; ++i) indx[i] = i;
  PetscErrorCode ierr;
  ierr = VecSetValues(solver.solVec, nNodes, &indx[0], theta, INSERT_VALUES); CHKERRQ(ierr);
  el::detail::State_Jacobian_Func(solver.snes, solver.solVec, solver.kMat, solver.kMat, nullptr);
  
  // Compute eigenvalues
  SlepcInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);
  EPS eps;
  PetscScalar kr, ki;
  Vec xr, xi;
  PetscInt nconv;
  PetscReal error;
  ierr = EPSCreate(PETSC_COMM_WORLD, &eps); CHKERRQ(ierr);
  ierr = EPSSetProblemType(eps,EPS_NHEP); CHKERRQ(ierr);
  ierr = EPSSetFromOptions(eps); CHKERRQ(ierr);
  ierr = MatCreateVecs(solver.kMat,PETSC_NULL,&xr); CHKERRQ(ierr);
  ierr = MatCreateVecs(solver.kMat,PETSC_NULL,&xi); CHKERRQ(ierr);
  EPSSetWhichEigenpairs(eps, EPS_SMALLEST_MAGNITUDE);
  
  ierr = EPSSetOperators(eps,solver.kMat, PETSC_NULL); CHKERRQ(ierr);
  EPSSolve(eps);
  EPSGetConverged(eps, &nconv);
  std::cout<<"\nNumber of converged values: "<<nconv;
  for(int i=0; i<nconv; ++i)
    {
      EPSGetEigenpair(eps, i, &kr, &ki, xr, xi);
      EPSComputeError(eps, i, EPS_ERROR_RELATIVE, &error);
      std::cout<<"\n"<<i<<": "<<kr<<", "<<ki<<" --> "<<error<<std::flush;
    }


  // Get the jacobian in an alternate way
  Mat kMat;
  str->GetJacobian(&kMat);
  ierr = EPSSetOperators(eps, kMat, PETSC_NULL); CHKERRQ(ierr);
  EPSSolve(eps);
  EPSGetConverged(eps, &nconv);
  std::cout<<"\nNumber of converged values: "<<nconv;
  for(int i=0; i<nconv; ++i)
    {
      EPSGetEigenpair(eps, i, &kr, &ki, xr, xi);
      EPSComputeError(eps, i, EPS_ERROR_RELATIVE, &error);
      std::cout<<"\n"<<i<<": "<<kr<<", "<<ki<<" --> "<<error<<std::flush;
    }

  // Get the eigenvalues in an alternative way
  std::cout<<"\n\n";
  el::EigenSolver es(nNodes, 2, EPS_SMALLEST_MAGNITUDE);
  std::vector<double> rparts, iparts, errors;
  bool evFlag = es.Solve(kMat, rparts, iparts, errors);
  assert(evFlag==true);
  int numev = static_cast<int>(rparts.size());
  for(int i=0; i<numev; ++i)
    std::cout<<"\n"<<rparts[i]<<", "<<iparts[i]<<": "<<errors[i]<<std::flush;
  
  // Get the eigenvalues in an alternative way
  std::cout<<"\n\n"<<std::flush;
  Mat k2Mat;
  str->GetJacobian(&k2Mat, load_params, bc_params, solve_params); 
  evFlag = es.Solve(kMat, rparts, iparts, errors); 
  assert(evFlag==true);
  numev = static_cast<int>(rparts.size());
  for(int i=0; i<numev; ++i)
    std::cout<<"\n"<<rparts[i]<<", "<<iparts[i]<<": "<<errors[i]<<std::flush;


  es.Destroy();
  solver.Destroy();

  // Compute xy coordinates
  // -----------------------
  std::vector<double> xy;
  el::AngleToCartesianMap(str->GetStateField(),
			  str->GetElementArray(),
			  str->GetLocalToGlobalMap(),
			  xy);
  
  // Plot the state and sensitivity
  // -----------------------------------
  const auto& state = str->GetStateField().Get();
  std::fstream stream;
  stream.open("ss.dat", std::ios::out);
  for(int n=0; n<nNodes; ++n)
    stream << coordinates[n] << " "<<state[n]
	   <<" "<<xy[2*n]<<" "<<xy[2*n+1]<<"\n";
  stream.close();

  // Clean up
  delete str;
  for(auto&x: load_params.dirs) delete x;
  EPSDestroy(&eps);
  VecDestroy(&xi);
  VecDestroy(&xr);
  SlepcFinalize();
  PetscFinalize();
}
