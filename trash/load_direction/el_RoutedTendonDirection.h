// Sriramajayam

#ifndef EL_ROUTED_TENDON_DIRECTION_H
#define EL_ROUTED_TENDON_DIRECTION_H

#include <el_LoadDirection.h>
#include <el_TendonLoadPoints.h>

namespace el
{
  //! Class defining a routed tendon that passes through
  //! a fixed point, is routed through a series of nodes
  //! while being offset from the centerline, and terminates
  //! at a final offset point from the terminal node
  class RoutedTendonDirection: public LoadDirection
  {
  public:
    //! Constructor
    //! \param[in] pnum Load number corresponding to this direction
    //! \param[in] nloads Total number of loads
    //! \param[in] rtpost Routing post, copied
    //! \param[in] rtnodes Routing nodes, should be in sequence, copied
    //! \param[in] deltas Offset values along the normal to the centerline
    RoutedTendonDirection(const int pnum, const int nloads,
			  const RoutingPost& rtpost,
			  const std::vector<int>& rtnodes,
			  const std::vector<double>& deltas);
    
    //! Destructor
    inline virtual ~RoutedTendonDirection() {}

    //! Copy constructor
    //! \param[in] obj Object to be copied from
    RoutedTendonDirection(const RoutedTendonDirection& obj);

    //! Returns the location of the routing point
    inline const RoutingPost* GetRoutingPoint() const
    { return &RtPost; }

    //! Returns the list of routing nodes
    inline const std::vector<int>& GetRoutingNodes() const
    { return RtNodes; }

    //! Returns the list of offsets at the routing nodes
    inline const std::vector<double>& GetOffsets() const
    { return Offsets; }

    //! Main functionality
    //! Update the load direction and its sensitivities
    virtual void UpdateDirection(const LoadDirectionParams* obj) override;

  protected:
    //! Returns the index in the vector of load directions/sensitivities for a
    //! given element number
    //! \param[in] elm Element number for which to fetch the index
    virtual int GetIndex(const int elm) const override;

  private:
    const RoutingPost RtPost; //!< Routing post
    const detail::TendonLoadPoints lpCalcObj;
    const std::vector<int> RtNodes; //!< Routing nodes
    const std::vector<double> Offsets; //! Offsets at routing points

    mutable detail::TendonCoordsStruct rtStruct; //!< Coordinates, sensitivities and variations of routing nodes. 
    mutable detail::TendonCoordsStruct lpStruct; //!< Coordinates, sensitivities and variations of loading points.
    mutable detail::TendonCoordsStruct rpStruct; //!< Coordinates, sensitivites and variations of routing point
  };
}

#endif
