// Sriramajayam

#include <el_TendonLoadPoints.h>
#include <Element.h>
#include <el_TendonElastica.h>
#include <LocalToGlobalMap.h>
#include <el_Utils.h>
#include <array>
#include <algorithm>
#include <cmath>
#include <cassert>
#include <utility>

namespace el
{
  namespace detail
  {    
    // Constructor
    TendonLoadPoints::TendonLoadPoints(const RoutingPost& rtpost,
				       const std::vector<int> rtnodes,
				       const std::vector<double> deltas,
				       const int nloads)
      :RtPost(&rtpost),
       RtNodes(rtnodes),
       Offsets(deltas),
       nLoads(nloads) {}

    // Copy constructor
    TendonLoadPoints::TendonLoadPoints(const TendonLoadPoints& obj)
      :RtPost(obj.RtPost),
       RtNodes(obj.RtNodes),
       Offsets(obj.Offsets),
       nLoads(obj.nLoads) {}

    // Compute the coordinates, sensitivities and variations of locations of routing nodes
    void TendonLoadPoints::
    ComputeRoutingNodeDetails(const TendonElastica& elastica,
			      const std::vector<std::pair<int,int>>& ElmIntervals,
			      TendonCoordsStruct& rtStruct) const
    {
      // Access the elastica
      const auto& ElmArray = elastica.GetElementArray();
      const auto& L2GMap = elastica.GetLocalToGlobalMap();
      const auto& theta = elastica.GetStateField();
      const auto& alphaVec = elastica.GetSensitivityFields();

      // Aliases
      auto& RtNodeCoords = rtStruct.Coords;
      auto& dRtNodeCoords = rtStruct.dCoords;
      auto& var_RtNodeCoords = rtStruct.var_Coords;
      
      // Sanity checks on sizes
      const int nRtNodes = static_cast<int>(RtNodes.size());
      const int nIntervals = nRtNodes;
      const int nVars = RtNodes[nRtNodes-1]+1;
      assert(static_cast<int>(alphaVec.size())==nLoads);
      rtStruct.CheckSizes(nRtNodes, nLoads, nVars);
      
      // Initialize to zero
      for(int p=0; p<nRtNodes; ++p)
	{
	  RtNodeCoords[p].first = 0.;
	  RtNodeCoords[p].second = 0.;
	  for(int L=0; L<nLoads; ++L)
	    { dRtNodeCoords[p][L].first = 0.;
	      dRtNodeCoords[p][L].second = 0.; }
	  for(int a=0; a<nVars; ++a)
	    { var_RtNodeCoords[p][a].first = 0.;
	      var_RtNodeCoords[p][a].second = 0.; }
	}
      
      // Temporaries: angle and sensitivities at quadrature points
      double q_theta;
      std::vector<double> q_alphaVec(nLoads);
      
      // Integrate over element intervals. 
      for(int ival=0; ival<nIntervals; ++ival)
	{
	  const int estart = ElmIntervals[ival].first;
	  const int efinal = ElmIntervals[ival].second;

	  for(int e=estart; e<=efinal; ++e)
	    {
	      const auto& Qwts = ElmArray[e]->GetIntegrationWeights(0);
	      const int nQuad = static_cast<int>(Qwts.size());
	      const int nDof = ElmArray[e]->GetDof(0);
	      for(int q=0; q<nQuad; ++q)
		{
		  // theta and alpha at this quadrature point
		  q_theta = 0.;
		  std::fill(q_alphaVec.begin(), q_alphaVec.end(), 0.);
		  for(int a=0; a<nDof; ++a)
		    { const int n = L2GMap.Map(0,a,e);
		      const double Na = ElmArray[e]->GetShape(0,q,a);
		      q_theta += theta[n]*Na;
		      for(int L=0; L<nLoads; ++L)
			q_alphaVec[L] += alphaVec[L][n]*Na; }

		
		  for(int p=ival; p<nRtNodes; ++p)
		    {
		      // Update Cartesian coordinates of routing points
		      RtNodeCoords[p].first  += Qwts[q]*std::cos(q_theta);
		      RtNodeCoords[p].second += Qwts[q]*std::sin(q_theta);

		      // Update the sensitivites of Cartesian coordinates wrt each load
		      for(int L=0; L<nLoads; ++L)
			{
			  dRtNodeCoords[p][L].first  -= Qwts[q]*std::sin(q_theta)*q_alphaVec[L];
			  dRtNodeCoords[p][L].second += Qwts[q]*std::cos(q_theta)*q_alphaVec[L];
			}

		      // Variations of nodal coordinates
		      for(int a=0; a<nDof; ++a)
			{
			  const int n = L2GMap.Map(0,a,e);
			  const double& Na = ElmArray[e]->GetShape(0,q,a);
			  var_RtNodeCoords[p][n].first  -= Qwts[q]*std::sin(q_theta)*Na;
			  var_RtNodeCoords[p][n].second += Qwts[q]*std::cos(q_theta)*Na;
			}
		    }
		}
	    }
	}
    
    
      // -- done --
      return;
    }



    // Compute the coordinates, sensitivities and variations of loading points
    void TendonLoadPoints::
    ComputeLoadingPointDetails(const TendonElastica& elastica,
			       const std::vector<std::pair<int,int>>& ElmIntervals,
			       const TendonCoordsStruct& rtStruct,
			       TendonCoordsStruct& lpStruct) const
    {
      // Aliases
      const auto& RtNodeCoords = rtStruct.Coords;
      const auto& dRtNodeCoords = rtStruct.dCoords;
      const auto& var_RtNodeCoords = rtStruct.var_Coords;
      auto& LoadPts = lpStruct.Coords;
      auto& dLoadPts = lpStruct.dCoords;
      auto& var_LoadPts = lpStruct.var_Coords;

      // Sanity checks
      const int nRtNodes = static_cast<int>(RtNodes.size());
      const int nVars = RtNodes[nRtNodes-1]+1;
      rtStruct.CheckSizes(nRtNodes, nLoads, nVars);
      lpStruct.CheckSizes(nRtNodes, nLoads, nVars);

      // Initialize
      for(auto& x:LoadPts) { x.first=0.; x.second=0.; }
      for(auto& x:dLoadPts)
	{ for(auto& y:x) { y.first=0.; y.second=0.; } }
      for(auto& x:var_LoadPts)
	{ for(auto& y:x) { y.first=0.; y.second=0.; } }
    
      // Access the state
      const auto& theta = elastica.GetStateField();

      // Access the sensitivities vector
      const auto& alphaVec = elastica.GetSensitivityFields();
    
      // Coordinates and sensitivities of loading points
      for(int i=0; i<nRtNodes; ++i)
	{
	  const int n = RtNodes[i];
	  const double& thetaVal = theta[n];
	  const double& offset = Offsets[i];

	  // Coordinates
	  LoadPts[i].first = RtNodeCoords[i].first - offset*std::sin(thetaVal);
	  LoadPts[i].second = RtNodeCoords[i].second + offset*std::cos(thetaVal);

	  // Sensitivities
	  for(int L=0; L<nLoads; ++L)
	    {
	      const double& alphaVal = alphaVec[L][n];
	      dLoadPts[i][L].first  = dRtNodeCoords[i][L].first  - offset*std::cos(thetaVal)*alphaVal;
	      dLoadPts[i][L].second = dRtNodeCoords[i][L].second - offset*std::sin(thetaVal)*alphaVal;
	    }

	  // Variations
	  for(int a=0; a<nVars; ++a)
	    {
	      var_LoadPts[i][a].first = var_RtNodeCoords[i][a].first;
	      var_LoadPts[i][a].second = var_RtNodeCoords[i][a].second;
	    }
	  var_LoadPts[i][n].first -= offset*std::cos(thetaVal);
	  var_LoadPts[i][n].second -= offset*std::sin(thetaVal);
	}

      // -- done --
      return;
    }

    // Compute the coordinates, sensitivities and variations of the routing point
    void TendonLoadPoints::
    ComputeRoutingPostDetails(const TendonCoordsStruct& lpStruct,
			      TendonCoordsStruct& rpStruct) const
    {
      // Sanity checks
      const int nRtNodes = static_cast<int>(RtNodes.size());
      const int nVars = RtNodes[nRtNodes-1]+1;
      lpStruct.CheckSizes(nRtNodes, nLoads, nVars);
      rpStruct.CheckSizes(1, nLoads, nVars);

      // Routing post is connected only to the 0th loading point
      const auto& LoadPt = lpStruct.Coords[0];
      const auto& dLoadPt = lpStruct.dCoords[0];
      const auto& var_LoadPt = lpStruct.var_Coords[0];

      // Routing point
      auto& RoutePt = rpStruct.Coords[0];
      auto& dRoutePt = rpStruct.dCoords[0];
      auto& var_RoutePt = rpStruct.var_Coords[0];

      // Shift the origin to the center of the post.
      // Sensitivities and variations remain unaffected
      // Shifted load point: (p,q)
      const double p = LoadPt.first-RtPost->center[0];
      const double q = LoadPt.second-RtPost->center[1];

      // Radius of the post
      const double R = RtPost->radius;

      // Orientation
      const double sign = RtPost->orientation;

      // Coordinates of the routing point
      const double hyp = p*p+q*q;
      const double det = std::sqrt(hyp-R*R);
      RoutePt.first  = RtPost->center[0] + R*(p*R-sign*q*det)/hyp;
      RoutePt.second = RtPost->center[1] + R*(q*R+sign*p*det)/hyp;

      // Sensitivities of the routing point
      for(int L=0; L<nLoads; ++L)
	{
	  const double dp = dLoadPt[L].first;
	  const double dq = dLoadPt[L].second;
	  const double ddet = (p*dp+q*dq)/det;
	  const double dhyp = 2.*(p*dp+q*dq);
	  
	  dRoutePt[L].first  = (R/hyp)*(dp*R-sign*dq*det-sign*q*ddet) - (dhyp/hyp)*(RoutePt.first-RtPost->center[0]);
	  dRoutePt[L].second = (R/hyp)*(dq*R+sign*dp*det+sign*p*ddet) - (dhyp/hyp)*(RoutePt.second-RtPost->center[1]);
	}


      // Variations of the routing point
      for(int a=0; a<nVars; ++a)
	{
	  const double dp = var_LoadPt[a].first;
	  const double dq = var_LoadPt[a].second;
	  const double ddet = (p*dp+q*dq)/det;
	  const double dhyp = 2.*(p*dp+q*dq);

	  var_RoutePt[a].first  = (R/hyp)*(dp*R-sign*dq*det-sign*q*ddet) - (dhyp/hyp)*(RoutePt.first-RtPost->center[0]);
	  var_RoutePt[a].second = (R/hyp)*(dq*R+sign*dp*det+sign*p*ddet) - (dhyp/hyp)*(RoutePt.second-RtPost->center[1]);
	}

      // -- done --
      return;
    }

    
    // Implementation of helper struct

    // Constructor
    TendonCoordsStruct::TendonCoordsStruct()
      :Coords({}), dCoords({}), var_Coords({}) {}

    // Disable copy
    TendonCoordsStruct::TendonCoordsStruct(const TendonCoordsStruct& obj)
      :Coords(obj.Coords),
       dCoords(obj.dCoords),
       var_Coords(obj.var_Coords) {}
    
    // Resize vectors
    void TendonCoordsStruct::Resize(const int nPoints,
				    const int nLoads,
				    const int nVars)
    {
      Coords.resize(nPoints);
      dCoords.resize(nPoints);
      var_Coords.resize(nPoints);
      for(auto& x:dCoords)
	x.resize(nLoads);
      for(auto& x:var_Coords)
	x.resize(nVars);
      return;
    }

    // Check sizes of vectors
    void TendonCoordsStruct::CheckSizes(const int nPoints,
					const int nLoads,
					const int nVars) const
    {
      assert(static_cast<int>(Coords.size())==nPoints &&
	     static_cast<int>(dCoords.size())==nPoints &&
	     static_cast<int>(var_Coords.size())==nPoints);

      for(auto& x:dCoords)
	assert(static_cast<int>(x.size())==nLoads);

      for(auto& x:var_Coords)
	assert(static_cast<int>(x.size())==nVars);

      return;
    }
    
  }
}
