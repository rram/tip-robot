// Sriramajayam

#include <el_OffsetTendonLoadDirection.h>
#include <cmath>
#include <Element.h>
#include <LocalToGlobalMap.h>
#include <el_Utils.h>
#include <array>
#include <algorithm>

namespace el
{
  // Constructor
  OffsetTendonLoadDirection::OffsetTendonLoadDirection(const int pnode,
						       const int pnum, const int nloads,
						       const double delta,
						       const RoutingPost& rtpost)
    :LoadDirection(pnum, nloads),
     PNode(pnode),
     offset(delta),
     RtPost(rtpost),
     lpCalcObj(RtPost, std::vector<int>({pnode}), std::vector<double>({delta}), nloads)
  {
    // Element intervals
    ElmIntervals.resize(1);
    ElmIntervals[0].first = 0;
    ElmIntervals[0].second = PNode-1;

    // Direction cosines
    Hval.resize(1);
    Vval.resize(1);

    // Sensitivities
    dHval.resize(1); dVval.resize(1);
    dHval[0].resize(nLoads);
    dVval[0].resize(nLoads);

    // Sensitivities
    const int nVars = PNode+1;
    dir_deriv_index.resize(1);
    dir_deriv_index[0].resize(nVars);
    for(int i=0; i<nVars; ++i)
      dir_deriv_index[0][i] = i;

    dir_deriv_Hval.resize(1);
    dir_deriv_Vval.resize(1);
    dir_deriv_Hval[0].resize(nVars);
    dir_deriv_Vval[0].resize(nVars);

    // Size routing node, loading point and routing point structs
    rtStruct.Resize(1,nLoads,nVars); 
    lpStruct.Resize(1,nLoads,nVars);
    rpStruct.Resize(1,nLoads,nVars);
    
    return;
  }


  // Copy constructor
  OffsetTendonLoadDirection::
  OffsetTendonLoadDirection(const OffsetTendonLoadDirection& obj)
    :LoadDirection(obj),
     PNode(obj.PNode),
     offset(obj.offset),
     RtPost(obj.RtPost),
     lpCalcObj(obj.lpCalcObj),
     rtStruct(obj.rtStruct),
     lpStruct(obj.lpStruct),
     rpStruct(obj.rpStruct) {}
  

  // Main functionality: update state-dependent directions
  void OffsetTendonLoadDirection::UpdateDirection(const LoadDirectionParams* params)
  {
    // Sanity checks
    assert(params!=nullptr && "el::TendonLoadDirection::UpdateDirection()- parameters not provided");
    assert(static_cast<int>(ElmIntervals.size())==1 &&
	   ElmIntervals[0].first==0 &&
	   ElmIntervals[0].second==PNode-1);
    assert(static_cast<int>(Hval.size())==1 && static_cast<int>(Vval.size())==1 &&
	   static_cast<int>(dHval.size())==1 && static_cast<int>(dVval.size())==1 &&
	   static_cast<int>(dHval[0].size())==nLoads && static_cast<int>(dVval[0].size())==nLoads);
    const int nVars = PNode+1;
    assert(static_cast<int>(dir_deriv_index.size())==1 &&
	   static_cast<int>(dir_deriv_index[0].size())==nVars);
    assert(static_cast<int>(dir_deriv_Hval.size())==1 &&
	   static_cast<int>(dir_deriv_Hval[0].size())==nVars);
    assert(static_cast<int>(dir_deriv_Vval.size())==1 &&
	   static_cast<int>(dir_deriv_Vval[0].size())==nVars);
    
    // Initialize
    std::fill(dir_deriv_Hval[0].begin(), dir_deriv_Hval[0].end(), 0.);
    std::fill(dir_deriv_Vval[0].begin(), dir_deriv_Vval[0].end(), 0.);

    // Compute routing node coordiantes + sensitivities + variations
    lpCalcObj.ComputeRoutingNodeDetails(params, ElmIntervals, rtStruct);

    // Compute loading points + sensitivities + variations
    lpCalcObj.ComputeLoadingPointDetails(params, ElmIntervals, rtStruct, lpStruct);

    // Compute routing point + sensitivities + variations
    lpCalcObj.ComputeRoutingPostDetails(lpStruct, rpStruct);

    // Cartesian coordinates of the post
    const double X0 = rpStruct.Coords[0].first;
    const double Y0 = rpStruct.Coords[0].second;
    
    // Location of the loading point (PNode)
    const double X = lpStruct.Coords[0].first;
    const double Y = lpStruct.Coords[0].second;
    
    // Compute the loading direction (x,y)-->rt post
    const double norm = std::sqrt( (X0-X)*(X0-X) + (Y0-Y)*(Y0-Y) );
    assert(norm>1.e-4 && "el::OffsetTendonLoadingDirection::UpdateDirection- tendon length is zero");
    Hval[0] = (X0-X)/norm;
    Vval[0] = (Y0-Y)/norm;
    
    // Compute the sensitivities of the loading direction (XL,YL) ---> fixed point
    for(int k=0; k<nLoads; ++k)
      {
	// Sensitivity of the routing post
	const double& dX0 = rpStruct.dCoords[0][k].first;
	const double& dY0 = rpStruct.dCoords[0][k].second;

	// Sensitivity of the loading point
	const double& dX = lpStruct.dCoords[0][k].first;
	const double& dY = lpStruct.dCoords[0][k].second;

	// Sensitivity of the norm
	const double dnorm = ((X0-X)*(dX0-dX) + (Y0-Y)*(dY0-dY))/norm;

	// Sensitivities of direction cosines
	dHval[0][k] = (dX0-dX)/norm - (X0-X)*dnorm/(norm*norm);
	dVval[0][k] = (dY0-dY)/norm - (Y0-Y)*dnorm/(norm*norm);
      }

    // Variations of direction cosines
    for(int n=0; n<=PNode; ++n) 
      {
	// Variations of the routing post
	const double& dX0 = rpStruct.var_Coords[0][n].first;
	const double& dY0 = rpStruct.var_Coords[0][n].second;
	
	// Variations of the loading point
	const double& dX = lpStruct.var_Coords[0][n].first;
	const double& dY = lpStruct.var_Coords[0][n].second;

	// Variation of the norm
	const double dnorm = ((X0-X)*(dX0-dX) + (Y0-Y)*(dY0-dY))/norm;
	
	// Variations of the direction cosines Hval and Vval
	dir_deriv_Hval[0][n] = (dX0-dX)/norm - (X0-X)*dnorm/(norm*norm);
	dir_deriv_Vval[0][n] = (dY0-dY)/norm - (Y0-Y)*dnorm/(norm*norm);
      }

    // done
    return;
  }

  
}
