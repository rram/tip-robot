// Sriramajayam

#include <el_TendonElastica.h>
#include <el_P11DElement.h>
#include <el_OffsetTendonLoadDirection.h>
#include <iostream>
#include <fstream>
#include <random>

int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);

  // Create a uniform 1D mesh on the interval [0,1]
  const int nNodes = 20;
  const double h = 1./static_cast<double>(nNodes-1);
  std::vector<double> coordinates(nNodes);
  for(int n=0; n<nNodes; ++n)
    coordinates[n] = 0.+h*static_cast<double>(n);
  const int nElements = nNodes-1;
  std::vector<int> connectivity(2*nElements);
  for(int e=0; e<nElements; ++e)
    { connectivity[2*e] = e+1;
      connectivity[2*e+1] = e+2; }
  Segment<1>::SetGlobalCoordinatesArray(coordinates);
  
  // Generate a random values for state/loads/directions
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_real_distribution<> dis(-2.,2.);
  
  // Create 2 tendon loads
  const int nLoads = 2;
  std::vector<int> PNodes({nNodes/2,nNodes-1});
  el::LoadParams load_params;
  load_params.values.resize(nLoads);
  load_params.values[0] = dis(gen);
  load_params.values[1] = dis(gen);
  load_params.dirs.resize(nLoads);
  const double RtPoint0[] = {0.,0.1};
  const double RtPoint1[] = {0.,-0.25};
  std::vector<el::RoutingPost> RtPosts(2);
  RtPosts[0].center[0]=RtPoint0[0]; RtPosts[0].center[1]=RtPoint0[1]; RtPosts[0].radius=0.125; RtPosts[0].orientation=-1;
  RtPosts[1].center[0]=RtPoint1[0]; RtPosts[1].center[1]=RtPoint1[1]; RtPosts[1].radius=0.075; RtPosts[1].orientation=1.;
  const double offsets[] = {-0.2, 0.3};
  load_params.dirs[0] = new el::OffsetTendonLoadDirection(PNodes[0],0,nLoads,offsets[0],RtPosts[0]);
  load_params.dirs[1] = new el::OffsetTendonLoadDirection(PNodes[1],1,nLoads,offsets[1],RtPosts[1]);

  // Dirichlet BCs
  el::DirichletBCs bc_params;
  bc_params.dofs = std::vector<int>({0});
  bc_params.values = std::vector<double>({0.});

  // Create strut
  const double EI = 1.;
  el::TendonElastica *str = new el::TendonElastica(coordinates, connectivity, EI, PNodes);

  // Solve parameters
  el::Solve_Params solve_params
    ({.consistentLinearization=false,
	.EPS=1.e-10,
	.resScale=1.,
	.dofScale=1.,
	.nMaxIters=25,
	.verbose=false});

  // Compute the sensivities at the given loads
  str->ComputeStateAndSensitivities(load_params, bc_params, solve_params);
  const auto& alphaField1 = str->GetSensitivityField(0).Get();
  const auto& alphaField2 = str->GetSensitivityField(1).Get();
  std::vector<double> alpha1(nNodes), alpha2(nNodes);
  for(int i=0; i<nNodes; ++i)
    { alpha1[i] = alphaField1[i];
      alpha2[i] = alphaField2[i]; }
  
  // Compute the state at P0 +/- epsilon * numerical difference
  std::vector<double> alpha1Num(nNodes);
  const double P_EPS = 1.e-4;
  load_params.values[0] += P_EPS;
  str->ComputeState(load_params, bc_params, solve_params);
  const auto& thetaField = str->GetStateField().Get();
  for(int i=0; i<nNodes; ++i)
    alpha1Num[i] = thetaField[i];
  load_params.values[0] -= 2.*P_EPS;
  str->ComputeState(load_params, bc_params, solve_params);
  for(int i=0; i<nNodes; ++i)
    { alpha1Num[i] -= thetaField[i];
      alpha1Num[i] /= (2.*P_EPS); }
  // Restore the original load
  load_params.values[0] += P_EPS;

  // Compute the state at P1 +/- epsilon & numerical difference
  std::vector<double> alpha2Num(nNodes);
  load_params.values[1] += P_EPS;
  str->ComputeState(load_params, bc_params, solve_params);
  for(int i=0; i<nNodes; ++i)
    alpha2Num[i] = thetaField[i];
  load_params.values[1] -= 2.*P_EPS;
  str->ComputeState(load_params, bc_params, solve_params);
  for(int i=0; i<nNodes; ++i)
    { alpha2Num[i] -= thetaField[i];
      alpha2Num[i] /= (2.*P_EPS); }
  // Restore the original load
  load_params.values[1] += P_EPS;
  
  // Plot the computed and numerical sensitivities
  std::fstream pfile;
  pfile.open((char*)"ss.dat", std::ios::out);
  for(int i=0; i<nNodes; ++i)
    pfile << coordinates[i]<<" "<<alpha1[i]<<" "<<alpha1Num[i]<<" "<<alpha2[i]<<" "<<alpha2Num[i]<<"\n";
  pfile.flush(); pfile.close();

  // Preliminary check on accuracy
  for(int i=0; i<nNodes; ++i)
    { assert(std::abs(alpha1[i]-alpha1Num[i])+std::abs(alpha2[i]-alpha2Num[i])<1.e-3); }
  
  // Clean up
  delete str;
  for(auto& x:load_params.dirs) delete x;
  PetscFinalize();
}
