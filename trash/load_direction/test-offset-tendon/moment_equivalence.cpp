// Sriramajayam

// Uses the solution from an offset tendon with a
// constant load/moment to compare final state solutions
// Plot the printed file to compare solution profiles

#include <el_TendonElastica.h>
#include <el_ConstantLoadDirection.h>
#include <el_ConstantMoment.h>
#include <el_OffsetTendonLoadDirection.h>
#include <el_offsetTendonMoment.h>
#include <el_P11DElement.h>
#include <random>
#include <iostream>
#include <fstream>

int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);

  // Create a uniform 1D mesh on the interval [0,1]
  // ------------------------------------------------
  const int nNodes = 30;
  const double h = 1./static_cast<double>(nNodes-1);
  std::vector<double> coordinates(nNodes);
  for(int n=0; n<nNodes; ++n)
    coordinates[n] = 0.+h*static_cast<double>(n);
  const int nElements = nNodes-1;
  std::vector<int> connectivity(2*nElements);
  for(int e=0; e<nElements; ++e)
    { connectivity[2*e] = e+1;
      connectivity[2*e+1] = e+2; }
  Segment<1>::SetGlobalCoordinatesArray(coordinates);
  std::cout<<"\nNumber of elements: "<<nElements<<std::flush;

  // Random load generation
  std::random_device rd;  
  std::mt19937 gen(rd());
  std::uniform_real_distribution<> dis(0.,5.);
    
  // One load
  const int nLoads = 1;
  std::vector<int> PNodes({nNodes-1});
  
  // Dirichlet BCs: clamp the left end
  el::DirichletBCs bc_params;
  bc_params.dofs = std::vector<int>({0});
  bc_params.values = std::vector<double>({0.});
    
  // Solve parameters
  el::Solve_Params solve_params({
      .consistentLinearization = true, // Automatically consistent
	.EPS = 1.e-10,
	.resScale = 1.,
	.dofScale = 1.,
	.nMaxIters = 25,
	.verbose = true});
  
  // Create strut
  const double EI = 1.;
  el::TendonElastica* str = new el::TendonElastica(coordinates, connectivity, EI, PNodes);

  // ---------------------------------
  // Create one offset-tendon load+moment
  el::LoadParams tendon_load_params;
  tendon_load_params.values.resize(nLoads);
  tendon_load_params.dirs.resize(nLoads);
  tendon_load_params.values[0] = dis(gen);
  el::RoutingPost RtPost;
  RtPost.center[0]=0.; RtPost.center[0]=0.5; RtPost.radius=0.1; RtPost.orientation=-1.;
  const double offset = 0.2;
  tendon_load_params.dirs[0] = new el::OffsetTendonLoadDirection(PNodes[0],0,nLoads,offset,RtPost);
  tendon_load_params.moments.resize(1);
  tendon_load_params.moments[0] = new el::OffsetTendonMoment(tendon_load_params.dirs[0]);

  // Solve
  str->ComputeState(tendon_load_params, bc_params, solve_params);

  // Record the solution
  std::vector<double> tendon_xy;
  el::AngleToCartesianMap(str->GetStateField(),
			  str->GetElementArray(),
			  str->GetLocalToGlobalMap(),
			  tendon_xy);
  const auto& tendon_state = str->GetStateField().Get();
  std::vector<double> tendon_theta(nNodes);
  for(int n=0; n<nNodes; ++n)
    tendon_theta[n] = tendon_state[n];

  //----------------------------------
  // Create constant load + moment based on the previous solution
  el::LoadParams const_load_params;
  const_load_params.values.resize(nLoads);
  const_load_params.dirs.resize(nLoads);
  const_load_params.values[0] = tendon_load_params.values[0];
  const double hval = tendon_load_params.dirs[0]->GetDirection(0,el::DirCosine::Horizontal); // Element 0 is a proxy
  const double vval = tendon_load_params.dirs[0]->GetDirection(0,el::DirCosine::Vertical);
  const double tendon_dir[] = {hval, vval};
  const_load_params.dirs[0] = new el::ConstantLoadDirection(PNodes[0],0,nLoads,tendon_dir);
  const auto& mit = tendon_load_params.moments[0]->GetFunctional();
  assert(mit.begin()->first==PNodes[0]);
  const double mval = mit.begin()->second;
  const_load_params.moments.resize(nLoads);
  const_load_params.moments[0] = new el::ConstantMoment(PNodes[0], nLoads, -mval);

  // Solve
  str->ComputeState(const_load_params, bc_params, solve_params);
  
  // Record the solution
  std::vector<double> const_xy;
  el::AngleToCartesianMap(str->GetStateField(),
			  str->GetElementArray(),
			  str->GetLocalToGlobalMap(),
			  const_xy);
  const auto& const_state = str->GetStateField().Get();
  std::vector<double> const_theta(nNodes);
  for(int n=0; n<nNodes; ++n)
    const_theta[n] = const_state[n];
  
  // Plot the two solutions
  // -----------------------
  std::fstream stream;
  stream.open("ss.dat", std::ios::out);
  for(int n=0; n<nNodes; ++n)
    stream << coordinates[n] << " "<<tendon_theta[n]<<" "<<const_theta[n]<<" "
	   <<tendon_xy[2*n]<<" "<<tendon_xy[2*n+1]<<" "<<const_xy[2*n]<<" "<<const_xy[2*n+1]<<"\n";
  stream.close();

  // Preliminary check on accuracy
  for(int i=0; i<nNodes; ++i)
    { assert(std::abs(tendon_theta[i]-const_theta[i])<1.e-3); }

  // Clean up
  delete str;
  for(auto&x: tendon_load_params.dirs) delete x;
  for(auto&x: tendon_load_params.moments) delete x;
  for(auto&x: const_load_params.dirs) delete x;
  for(auto&x: const_load_params.moments) delete x;
  PetscFinalize();
}
