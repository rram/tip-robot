// Sriramajayam

#include <el_TendonElastica.h>
#include <el_OffsetTendonLoadDirection.h>
#include <el_offsetTendonMoment.h>
#include <el_P11DElement.h>
#include <random>
#include <iostream>
#include <fstream>

int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);

  // Create a uniform 1D mesh on the interval [0,1]
  // ------------------------------------------------
  const int nNodes = 40;
  const double h = 1./static_cast<double>(nNodes-1);
  std::vector<double> coordinates(nNodes);
  for(int n=0; n<nNodes; ++n)
    coordinates[n] = 0.+h*static_cast<double>(n);
  const int nElements = nNodes-1;
  std::vector<int> connectivity(2*nElements);
  for(int e=0; e<nElements; ++e)
    { connectivity[2*e] = e+1;
      connectivity[2*e+1] = e+2; }
  Segment<1>::SetGlobalCoordinatesArray(coordinates);
  std::cout<<"\nNumber of elements: "<<nElements<<std::flush;

  // Create one load dummy load
  // --------------------------------------
  const int nLoads = 1;
  std::vector<int> PNodes({nNodes-1});
  el::LoadParams load_params;
  load_params.values.resize(nLoads);
  load_params.dirs.resize(nLoads);
  load_params.values[0] = 4.0;
  el::RoutingPost RtPost;
  RtPost.center[0]=0.; RtPost.center[1]=0.5; RtPost.radius=0.1; RtPost.orientation=-0.;
  const double offset = 0.1;
  load_params.dirs[0] = new el::OffsetTendonLoadDirection(PNodes[0],0,nLoads,offset,RtPost);
  
  // Create one moment load
  load_params.moments.resize(1);
  load_params.moments[0] = new el::OffsetTendonMoment(load_params.dirs[0]);
  
  // Dirichlet BCs: clamp the left end
  // ----------------------------------
  el::DirichletBCs bc_params;
  bc_params.dofs = std::vector<int>({0});
  bc_params.values = std::vector<double>({0.});
    
  // Solve parameters
  // ------------------
  el::Solve_Params solve_params({
      .consistentLinearization = true, // Automatically consistent
	.EPS = 1.e-10,
	.resScale = 1.,
	.dofScale = 1.,
	.nMaxIters = 25,
	.verbose = true});

  // Create strut
  // --------------
  const double EI = 1.;
  el::TendonElastica* str = new el::TendonElastica(coordinates, connectivity, EI, PNodes);
  
  // Compute the state
  // -----------------------------------
  str->ComputeState(load_params, bc_params, solve_params);

  // Compute xy coordinates
  // -----------------------
  std::vector<double> xy;
  el::AngleToCartesianMap(str->GetStateField(),
			  str->GetElementArray(),
			  str->GetLocalToGlobalMap(),
			  xy);
  
  // Plot the state
  // -----------------------------------
  const auto& state = str->GetStateField().Get();
  std::fstream stream;
  stream.open("ss.dat", std::ios::out);
  for(int n=0; n<nNodes; ++n)
    stream << coordinates[n] << " "<<state[n]<<" "<<xy[2*n]<<" "<<xy[2*n+1]<<"\n";
  stream.close();

  // Clean up
  delete str;
  for(auto&x: load_params.dirs) delete x;
  for(auto&x: load_params.moments) delete x;
  PetscFinalize();
}
