// Sriramajayam

#include <el_TendonElastica.h>
#include <el_P11DElement.h>
#include <el_OffsetTendonLoadDirection.h>
#include <iostream>
#include <fstream>
#include <random>

int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);

  // Create a uniform 1D mesh on the interval [0,1]
  const int nNodes = 20;
  const double h = 1./static_cast<double>(nNodes-1);
  std::vector<double> coordinates(nNodes);
  for(int n=0; n<nNodes; ++n)
    coordinates[n] = 0.+h*static_cast<double>(n);
  const int nElements = nNodes-1;
  std::vector<int> connectivity(2*nElements);
  for(int e=0; e<nElements; ++e)
    { connectivity[2*e] = e+1;
      connectivity[2*e+1] = e+2; }
  Segment<1>::SetGlobalCoordinatesArray(coordinates);

  // Generate a random values for state/loads/directions/sensitivities
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_real_distribution<> dis(-2.,2.);
  
  // Create 1 tendon load
  const int nLoads = 2;
  std::vector<int> PNodes({nNodes/2, nNodes-1});
  el::LoadParams load_params;
  load_params.values.resize(nLoads);
  load_params.dirs.resize(nLoads);
  const double RtPoint[] = {0.1,0.4};
  const double offsets[] = {0.15,-0.3};
  std::vector<el::RoutingPost> RtPosts(2);
  RtPosts[0].center[0]=RtPoint[0]; RtPosts[0].center[1]=RtPoint[1]; RtPosts[0].radius=0.125; RtPosts[0].orientation=-1;
  RtPosts[1].center[0]=RtPoint[0]; RtPosts[1].center[1]=RtPoint[1]; RtPosts[1].radius=0.075; RtPosts[1].orientation=1.;
  for(int i=0; i<nLoads; ++i)
    { load_params.dirs[i] = new el::OffsetTendonLoadDirection(PNodes[i],i,nLoads,offsets[i],RtPosts[i]);
      load_params.values[i] = dis(gen); }
  
  // Dirichlet BCs
  el::DirichletBCs bc_params;
  bc_params.dofs = std::vector<int>({0});
  bc_params.values = std::vector<double>({0.});

  // Create strut
  const double EI = std::sqrt(5.);
  el::TendonElastica *str = new el::TendonElastica(coordinates, connectivity, EI, PNodes);

  // Generate random values for the state
  std::vector<double> theta(nNodes);
  for(int i=0; i<nNodes; ++i)
    theta[i] = dis(gen);
  str->SetStateField(&theta[0]);
  
  // Do a consistency test for each load
  for(int L=0; L<nLoads; ++L)
    {
      // Random values for sensitivities
      std::vector<double> alpha(nNodes);
      for(int i=0; i<nNodes; ++i)
	alpha[i] = dis(gen);

      // Check consistency wrt L-th load
      str->SensitivityConsistencyTest(L, &alpha[0], load_params, bc_params);
    }

  // Clean up
  delete str;
  for(auto& x:load_params.dirs) delete x;
  PetscFinalize();
}
