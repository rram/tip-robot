// Sriramajayam

#include <el_RoutedTendonDirection.h>
#include <Element.h>
#include <LocalToGlobalMap.h>
#include <el_Utils.h>
#include <array>
#include <algorithm>
#include <iostream>
#include <cassert>
#include <cmath>

namespace el
{
  // Constructor
  RoutedTendonDirection::RoutedTendonDirection(const int pnum, const int nloads,
					       const RoutingPost& rtpost,
					       const std::vector<int>& rtnodes,
					       const std::vector<double>& deltas)
    :LoadDirection(pnum, nloads),
     RtPost(rtpost),
     lpCalcObj(RtPost,rtnodes,deltas,nloads),
     RtNodes(rtnodes),
     Offsets(deltas)
  {
    const int nRtNodes = static_cast<int>(RtNodes.size());
    assert(nRtNodes>=1 && nRtNodes==static_cast<int>(Offsets.size()));

    // Check for an ordered sequence of nodes
    assert(RtNodes[0]>0);
    for(int i=0; i<nRtNodes-1; ++i)
      assert(RtNodes[i]<RtNodes[i+1]);
    
    // Element intervals: {(0,RtNodes[0]-1), (RtNodes[0],RtNodes[1]-1),..}
    const int nIntervals = nRtNodes;
    ElmIntervals.resize(nIntervals);
    for(int i=0; i<nIntervals; ++i)
      { ElmIntervals[i].first = 0;
	ElmIntervals[i].second = RtNodes[i]-1; }

    // Direction cosines
    Hval.resize(nIntervals);
    Vval.resize(nIntervals);

    // Sensitivities
    dHval.resize(nRtNodes);
    dVval.resize(nRtNodes);
    for(int i=0; i<nRtNodes; ++i)
      { dHval[i].resize(nLoads);
	dVval[i].resize(nLoads); }

    // Sensitivities
    dir_deriv_index.resize(nIntervals);
    dir_deriv_Hval.resize(nIntervals);
    dir_deriv_Vval.resize(nIntervals);
    // i-th interval contains variations from 0 to RtNodes[i]
    for(int i=0; i<nIntervals; ++i)
      {
	const int nVars = RtNodes[i]+1;
	dir_deriv_index[i].resize(nVars);
	dir_deriv_Hval[i].resize(nVars);
	dir_deriv_Vval[i].resize(nVars);
	for(int j=0; j<nVars; ++j)
	  dir_deriv_index[i][j] = j;
      }
    
    // Resize temporaries. Coordinate calcs for routing point, routing nodes and loading points
    const int nVars = RtNodes[nRtNodes-1]+1;
    rtStruct.Resize(nRtNodes, nLoads, nVars);
    lpStruct.Resize(nRtNodes, nLoads, nVars);
    rpStruct.Resize(1, nLoads, nVars);
    
    // done
  }
  
  // Copy constructor
  RoutedTendonDirection::RoutedTendonDirection(const RoutedTendonDirection& obj)
    :LoadDirection(obj),
     RtPost(obj.RtPost),
     lpCalcObj(obj.lpCalcObj),
     RtNodes(obj.RtNodes),
     Offsets(obj.Offsets),
     rtStruct(obj.rtStruct),
     lpStruct(obj.lpStruct),
     rpStruct(obj.rpStruct) {}


  // Returns the index in the vector of load directions/sensitivities for a
  // given element number
  int RoutedTendonDirection::GetIndex(const int elm) const
  {
    const int nIntervals = static_cast<int>(ElmIntervals.size());
    for(int i=0; i<nIntervals; ++i)
      { if(elm>=ElmIntervals[i].first && elm<=ElmIntervals[i].second)
	  return i; }

    // Otherwise, return the last interval (LOGICAL HACK, assuming that the loading coefficient is zero)
    return nIntervals-1;
  }
     
  
  // Main functionality
  // Update the load direction, its sensitivities and its variations
  void RoutedTendonDirection::UpdateDirection(const LoadDirectionParams* params)
  {
    assert(params!=nullptr && "el::RoutedTendonDirection::UpdateDirection- parameters not provided.");
    
    // Sanity checks
    const int nIntervals = static_cast<int>(ElmIntervals.size());
    const int nRtNodes = static_cast<int>(RtNodes.size());
    assert(nIntervals==nRtNodes);
    assert(static_cast<int>(Hval.size())==nIntervals && static_cast<int>(Vval.size())==nIntervals);
    assert(static_cast<int>(dHval.size())==nIntervals && static_cast<int>(dVval.size())==nIntervals);
    assert(static_cast<int>(dir_deriv_index.size())==nIntervals &&
	   static_cast<int>(dir_deriv_Hval.size())==nIntervals &&
	   static_cast<int>(dir_deriv_Vval.size())==nIntervals);
    for(int i=0; i<nIntervals; ++i)
      {
	const int nVars = RtNodes[i]+1;
	assert(static_cast<int>(dHval[i].size())==nLoads && static_cast<int>(dVval[i].size())==nLoads);
	assert(static_cast<int>(dir_deriv_index[i].size())==nVars &&
	       static_cast<int>(dir_deriv_Hval[i].size())==nVars &&
	       static_cast<int>(dir_deriv_Vval[i].size())==nVars);
      }
    
    // Compute the coordinates of the routing nodes, their sensitivities and variations
    lpCalcObj.ComputeRoutingNodeDetails(params, ElmIntervals, rtStruct);
    
    // Compute coordinates of loading points, sensitivites and variations.
    lpCalcObj.ComputeLoadingPointDetails(params, ElmIntervals, rtStruct, lpStruct);

    // Compute the coordinates of the routing point, its sensitivites and variations
    lpCalcObj.ComputeRoutingPostDetails(lpStruct, rpStruct);
    
    // Aliases
    const auto& LoadPts = lpStruct.Coords;
    const auto& dLoadPts = lpStruct.dCoords;
    const auto& var_LoadPts = lpStruct.var_Coords;

    // Compute the direction cosines and their sensitivities wrt loads
    // Interval 'i' contains the sum of the direction cosines from i to nIntervals
    // |---0---(H0+H1+H2+..,V0+V1+V2+..)---1---P(H1+H2+..,V1+V2+..)---2--...---n---(Hn,Vn)---

    // Compute direction cosines and sensitivities
    std::fill(Hval.begin(), Hval.end(), 0.);
    std::fill(Vval.begin(), Vval.end(), 0.);
    for(auto& x:dHval) std::fill(x.begin(), x.end(), 0.);
    for(auto& x:dVval) std::fill(x.begin(), x.end(), 0.);
    
    for(int i=0; i<nIntervals; ++i)
      for(int j=i; j<nIntervals; ++j)
	{
	  // Line joining loading point j to j+1
	  const auto& P = (j==0) ? rpStruct.Coords[0] : LoadPts[j-1];
	  const auto& Q = LoadPts[j];
	  const double dir[] = {P.first-Q.first, P.second-Q.second};
	  const double norm = std::sqrt(dir[0]*dir[0]+dir[1]*dir[1]);
	  assert(norm>1.e-3 && "el::RoutedTendonDirection::UpdateDirection- Tendon length close to zero.");

	  // Direction cosines
	  Hval[i] += dir[0]/norm;
	  Vval[i] += dir[1]/norm;

	  // Sensitivities
	  const auto& dP = (j==0) ? rpStruct.dCoords[0] : dLoadPts[j-1];
	  const auto& dQ = dLoadPts[j];
	  for(int L=0; L<nLoads; ++L)
	    {
	      const double ddir[] = {dP[L].first-dQ[L].first, dP[L].second-dQ[L].second};
	      const double dnorm = (dir[0]*ddir[0]+dir[1]*ddir[1])/norm;
	      dHval[i][L] += ddir[0]/norm - dir[0]*dnorm/(norm*norm);
	      dVval[i][L] += ddir[1]/norm - dir[1]*dnorm/(norm*norm);
	    }
	}

    
    // Compute variations of (Hval[0],Vval[0]), ... (Hval[n],Vval[n])
    for(int i=0; i<nIntervals; ++i)
      {
	// initialize
	std::fill(dir_deriv_Hval[i].begin(), dir_deriv_Hval[i].end(), 0.);
	std::fill(dir_deriv_Vval[i].begin(), dir_deriv_Vval[i].end(), 0.);
	
	// Line joining loading point i to i+1
	const auto& P = (i==0) ? rpStruct.Coords[0] : LoadPts[i-1];
	const auto& Q = LoadPts[i];
	const double dir[] = {P.first-Q.first, P.second-Q.second};
	const double norm = std::sqrt(dir[0]*dir[0]+dir[1]*dir[1]);
	assert(norm>1.e-3 && "el::RoutedTendonDirection::UpdateDirection- Tendon length close to zero.");

	// (Hval[i],Vval[i]) has variations wrt 0,..,RtNodes[i]
	const auto& var_P = (i==0) ? rpStruct.var_Coords[0] : var_LoadPts[i-1];
	const auto& var_Q = var_LoadPts[i];
	for(int a=0; a<=RtNodes[i]; ++a)
	  {
	    const double var_dir[] = {var_P[a].first-var_Q[a].first, var_P[a].second-var_Q[a].second};
	    const double var_norm = (dir[0]*var_dir[0]+dir[1]*var_dir[1])/norm;
	    dir_deriv_Hval[i][a] += var_dir[0]/norm - dir[0]*var_norm/(norm*norm);
	    dir_deriv_Vval[i][a] += var_dir[1]/norm - dir[1]*var_norm/(norm*norm);
	  }
      }
      
    // --done-- 
    return;
  }
}
