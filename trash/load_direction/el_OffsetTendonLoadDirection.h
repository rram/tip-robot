// Sriramajayam

#ifndef EL_OFFSET_TENDON_LOAD_DIRECTION_H
#define EL_OFFSET_TENDON_LOAD_DIRECTION_H

#include <el_LoadDirection.h>
#include <el_TendonLoadPoints.h>
#include <cmath>
#include <cassert>

namespace el
{
  //! Class defining a follower load that consists of a tendon
  //! attached off-center from a point on the centerline, but
  //! along the local normal direction.
  //! The tendon is routed to a fixed point.
  class OffsetTendonLoadDirection: public LoadDirection
  {
  public:
    //! Constructor
    //! \param[in] pnode Node to which tendon is attached
    //! \param[in] pnum Load number corresponding to this direction
    //! \param[in] nloads Total number of loads
    //! \param[in] delta Length of the offset, signed. Oriented along the local normal
    //! \param[in] pcoords Coordinates of the fixed routing points
  OffsetTendonLoadDirection(const int pnode,
			    const int pnum, const int nloads,
			    const double delta,
			    const RoutingPost& rpost);

    //! Destructor
    inline virtual ~OffsetTendonLoadDirection() {}

    //! Copy constructor
    OffsetTendonLoadDirection(const OffsetTendonLoadDirection& obj);
    
    //! Returns the location of the routing point
    inline const RoutingPost* GetRoutingPost() const
    { return &RtPost; }

    //! Returns the offset
    inline double GetOffset() const
    { return offset; }

    //! Returns the loading node
    inline int GetLoadingNode() const
    { return PNode; }
    
    //! Main functionality
    //! Update the load direction and its sensitivity
    virtual void UpdateDirection(const LoadDirectionParams* obj) override;
    
  protected:
    //! Returns the index in the vector of load directions and sensitivities for a given element number
    //! \param[in] elm Element number for which to fetch the index
    virtual int GetIndex(const int elm) const override
    { return 0; }
      
  private:
    const int PNode;
    const double offset;
    const RoutingPost RtPost;
    detail::TendonLoadPoints lpCalcObj; //!< Object for computing load point coordinates, sensitivities & variations
    detail::TendonCoordsStruct rtStruct; //!< Routing node coordinates, sensitivities and variations.
    detail::TendonCoordsStruct lpStruct; //!< Loading point coordinates, sensitivities and variations.
    detail::TendonCoordsStruct rpStruct; //!< Coordinates, sensitivities and variations of routing point
  };
}

#endif
