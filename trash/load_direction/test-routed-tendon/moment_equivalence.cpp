// Sriramajayam

// Uses the solution from a routed tendon with a
// constant load to compare final state solutions
// Plot the printed file to compare solution profiles

#include <el_TendonElastica.h>
#include <el_ConstantLoadDirection.h>
#include <el_ConstantMoment.h>
#include <el_RoutedTendonDirection.h>
#include <el_RoutedTendonMoment.h>
#include <el_P11DElement.h>
#include <random>
#include <iostream>
#include <fstream>

int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);

  // Create a uniform 1D mesh on the interval [0,1]
  // ------------------------------------------------
  const int nNodes = 30;
  const double h = 1./static_cast<double>(nNodes-1);
  std::vector<double> coordinates(nNodes);
  for(int n=0; n<nNodes; ++n)
    coordinates[n] = 0.+h*static_cast<double>(n);
  const int nElements = nNodes-1;
  std::vector<int> connectivity(2*nElements);
  for(int e=0; e<nElements; ++e)
    { connectivity[2*e] = e+1;
      connectivity[2*e+1] = e+2; }
  Segment<1>::SetGlobalCoordinatesArray(coordinates);
  std::cout<<"\nNumber of elements: "<<nElements<<std::flush;

  // Random load generation
  std::random_device rd;  
  std::mt19937 gen(rd());
  std::uniform_real_distribution<> dis(0.,0.5);
    
  // Dirichlet BCs: clamp the left end
  el::DirichletBCs bc_params;
  bc_params.dofs = std::vector<int>({0});
  bc_params.values = std::vector<double>({0.});
    
  // Solve parameters
  el::Solve_Params solve_params({
      .consistentLinearization = true, 
	.EPS = 1.e-10,
	.resScale = 1.,
	.dofScale = 1.,
	.nMaxIters = 25,
	.verbose = true});

  std::vector<double> Hvals(2), Vvals(2), Mvals(2);
  std::vector<double> tendon_theta(nNodes), tendon_xy;
  std::vector<double> const_theta(nNodes), const_xy;
  const std::vector<int> RtNodes({nNodes/2, nNodes-1});
  const double EI = 1.;
  const double Pval = 2.;//dis(gen);
  
  // Create one routed-tendon
  {
    const int nLoads = 1;
    std::vector<int> PNodes({nNodes-1});
    el::TendonElastica* str = new el::TendonElastica(coordinates, connectivity, EI, PNodes);
    el::LoadParams tendon_load_params;
    tendon_load_params.values.resize(nLoads);
    tendon_load_params.dirs.resize(nLoads);
    tendon_load_params.moments.resize(nLoads);
    tendon_load_params.values[0] = Pval;
    el::RoutingPost RtPost;
    RtPost.center[0]=0.; RtPost.center[1]=0.5; RtPost.radius=0.; RtPost.orientation=0.;
    const std::vector<double> offsets({0.1,0.2});
    tendon_load_params.dirs[0] = new el::RoutedTendonDirection(0,nLoads,RtPost,RtNodes,offsets);
    tendon_load_params.moments[0] = new el::RoutedTendonMoment(tendon_load_params.dirs[0]);
    
    // Solve
    str->ComputeState(tendon_load_params, bc_params, solve_params);

    // Record the solution
    el::AngleToCartesianMap(str->GetStateField(),
			    str->GetElementArray(),
			    str->GetLocalToGlobalMap(),
			    tendon_xy);
    const auto& state = str->GetStateField().Get();
    for(int n=0; n<nNodes; ++n)
      tendon_theta[n] = state[n];

    // Track the direction cosines
    Hvals[0] = tendon_load_params.dirs[0]->GetDirection(0,el::DirCosine::Horizontal);
    Vvals[0] = tendon_load_params.dirs[0]->GetDirection(0,el::DirCosine::Vertical);
    Hvals[1] = tendon_load_params.dirs[0]->GetDirection(nElements-1,el::DirCosine::Horizontal);
    Vvals[1] = tendon_load_params.dirs[0]->GetDirection(nElements-1,el::DirCosine::Vertical);
    // Track the moments
    const auto& momentMap = tendon_load_params.moments[0]->GetFunctional();
    for(int i=0; i<2; ++i)
      {
	auto it = momentMap.find(RtNodes[i]);
	assert(it!=momentMap.end());
	Mvals[i] = -it->second;
      }
    for(auto& x:tendon_load_params.dirs) delete x;
    for(auto& x:tendon_load_params.moments) delete x;
    delete str;
  }

  // Create 2 constant loads based on the previous solution and resolve
  {
    const int nLoads = static_cast<int>(RtNodes.size());
    el::TendonElastica* str = new el::TendonElastica(coordinates, connectivity, EI, RtNodes);
    el::LoadParams const_load_params;
    const_load_params.values.resize(nLoads);
    const_load_params.dirs.resize(nLoads);
    const_load_params.moments.resize(nLoads);
    const_load_params.values[0] = Pval;
    const_load_params.values[1] = Pval;
    // Extract (H1,V1), (H2,V2) from (H1+H2,V1+V2), (H2,V2)
    const double dir0[] = {Hvals[0]-Hvals[1], Vvals[0]-Vvals[1]};
    const double dir1[] = {Hvals[1], Vvals[1]};
    const_load_params.dirs[0] = new el::ConstantLoadDirection(RtNodes[0],0,nLoads,dir0);
    const_load_params.dirs[1] = new el::ConstantLoadDirection(RtNodes[1],1,nLoads,dir1);
    const_load_params.moments[0] = new el::ConstantMoment(RtNodes[0], nLoads, Mvals[0]);
    const_load_params.moments[1] = new el::ConstantMoment(RtNodes[1], nLoads, Mvals[1]);
    
    // Solve
    str->ComputeState(const_load_params, bc_params, solve_params);
    
    // Record the solution
    el::AngleToCartesianMap(str->GetStateField(),
			    str->GetElementArray(),
			    str->GetLocalToGlobalMap(),
			    const_xy);
    const auto& state = str->GetStateField().Get();
    for(int n=0; n<nNodes; ++n)
      const_theta[n] = state[n];

    for(auto& x:const_load_params.dirs) delete x;
    for(auto& x:const_load_params.moments) delete x;
    delete str;
  }
  
  // Plot the two solutions
  // -----------------------
  std::fstream stream;
  stream.open("ss.dat", std::ios::out);
  for(int n=0; n<nNodes; ++n)
    stream << coordinates[n] << " "<<tendon_theta[n]<<" "<<const_theta[n]<<" "
	   <<tendon_xy[2*n]<<" "<<tendon_xy[2*n+1]<<" "<<const_xy[2*n]<<" "<<const_xy[2*n+1]<<"\n";
  stream.close();

  // Preliminary check on accuracy
  for(int i=0; i<nNodes; ++i)
    { assert(std::abs(tendon_theta[i]-const_theta[i])<1.e-3); }
  
  PetscFinalize();
}
