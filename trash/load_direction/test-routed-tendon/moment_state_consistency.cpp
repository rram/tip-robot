// Sriramajayam

#include <el_TendonElastica.h>
#include <el_P11DElement.h>
#include <el_RoutedTendonDirection.h>
#include <el_RoutedTendonMoment.h>
#include <iostream>
#include <fstream>
#include <random>

int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);

  // Create a uniform 1D mesh on the interval [0,1]
  const int nNodes = 6;
  const double h = 1./static_cast<double>(nNodes-1);
  std::vector<double> coordinates(nNodes);
  for(int n=0; n<nNodes; ++n)
    coordinates[n] = 0.+h*static_cast<double>(n);
  const int nElements = nNodes-1;
  std::vector<int> connectivity(2*nElements);
  for(int e=0; e<nElements; ++e)
    { connectivity[2*e] = e+1;
      connectivity[2*e+1] = e+2; }
  Segment<1>::SetGlobalCoordinatesArray(coordinates);

  // Generate a random values for state/loads/directions
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_real_distribution<> dis(-2.,2.);
  
  // Create 1 tendon load
  const int nLoads = 1;
  std::vector<int> PNodes({nNodes-1});
  el::LoadParams load_params;
  load_params.values.resize(nLoads);
  load_params.dirs.resize(nLoads);
  load_params.moments.resize(nLoads);
  el::RoutingPost RtPost;
  RtPost.center[0]=0.3; RtPost.center[1]=0.4; RtPost.radius=0.2; RtPost.orientation=-1.;
  std::vector<int> RtNodes({nNodes/2, nNodes-1});
  std::vector<double> offsets({0.15, -0.2});
  load_params.dirs[0] = new el::RoutedTendonDirection(0,nLoads,RtPost,RtNodes,offsets);
  load_params.values[0] = dis(gen);
  load_params.moments[0] = new el::RoutedTendonMoment(load_params.dirs[0]);
  
  // Dirichlet BCs
  el::DirichletBCs bc_params;
  bc_params.dofs = std::vector<int>({0});
  bc_params.values = std::vector<double>({0.});

  // Create strut
  const double EI = std::sqrt(5.);
  el::TendonElastica *str = new el::TendonElastica(coordinates, connectivity, EI, PNodes);

  // Generate random values for the state
  std::vector<double> theta(nNodes);
  for(int i=0; i<nNodes; ++i)
    theta[i] = dis(gen);
  
  // Do a consistency test
  str->StateConsistencyTest(&theta[0], load_params, bc_params);

  // Clean up
  delete str;
  for(auto& x:load_params.dirs) delete x;
  for(auto& x:load_params.moments) delete x;
  PetscFinalize();
}
