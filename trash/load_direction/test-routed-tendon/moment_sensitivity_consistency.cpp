// Sriramajayam

#include <el_TendonElastica.h>
#include <el_P11DElement.h>
#include <el_RoutedTendonDirection.h>
#include <el_RoutedTendonMoment.h>
#include <iostream>
#include <fstream>
#include <random>

int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);

  // Create a uniform 1D mesh on the interval [0,1]
  const int nNodes = 40;
  const double h = 1./static_cast<double>(nNodes-1);
  std::vector<double> coordinates(nNodes);
  for(int n=0; n<nNodes; ++n)
    coordinates[n] = 0.+h*static_cast<double>(n);
  const int nElements = nNodes-1;
  std::vector<int> connectivity(2*nElements);
  for(int e=0; e<nElements; ++e)
    { connectivity[2*e] = e+1;
      connectivity[2*e+1] = e+2; }
  Segment<1>::SetGlobalCoordinatesArray(coordinates);
  
  // Generate a random values for state/loads/directions
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_real_distribution<> dis(-1.5,1.5);

    // Create 2 tendon loads
  const int nLoads = 2;
  std::vector<int> PNodes({20,39}); // Terminal nodes of the two loads
  el::LoadParams load_params;
  load_params.values.resize(nLoads);
  load_params.values[0] = dis(gen);
  load_params.values[1] = dis(gen);
  load_params.dirs.resize(nLoads);
  load_params.moments.resize(nLoads);
  // 0th load+moments
  el::RoutingPost RtPost0;
  RtPost0.center[0]=0.; RtPost0.center[1]=0.1; RtPost0.radius=0.123; RtPost0.orientation=1.; 
  const std::vector<double> offsets0({-0.2,0.3});
  const std::vector<int> RtNodes0({10,20});
  load_params.dirs[0] = new el::RoutedTendonDirection(0,nLoads,RtPost0,RtNodes0,offsets0);
  load_params.moments[0] = new el::RoutedTendonMoment(load_params.dirs[0]);
  // 1st load+moments
  el::RoutingPost RtPost1;
  RtPost1.center[0]=-0.2; RtPost1.center[1]=0.2; RtPost1.radius=0.045; RtPost1.orientation=-1.; 
  const std::vector<double> offsets1({-0.1,0.2,-0.15});
  const std::vector<int> RtNodes1({15,30,39});
  load_params.dirs[1] = new el::RoutedTendonDirection(1,nLoads,RtPost1,RtNodes1,offsets1);
  load_params.moments[1] = new el::RoutedTendonMoment(load_params.dirs[1]);
  
  // Dirichlet BCs
  el::DirichletBCs bc_params;
  bc_params.dofs = std::vector<int>({0});
  bc_params.values = std::vector<double>({0.});

  // Create strut
  const double EI = 1.;
  el::TendonElastica *str = new el::TendonElastica(coordinates, connectivity, EI, PNodes);

  // Solve parameters
  el::Solve_Params solve_params
    ({.consistentLinearization=true,
	.EPS=1.e-10,
	.resScale=1.,
	.dofScale=1.,
	.nMaxIters=25,
	.verbose=false});

  // Compute the sensivities at the given loads
  str->ComputeStateAndSensitivities(load_params, bc_params, solve_params);
  const auto& alphaField1 = str->GetSensitivityField(0).Get();
  const auto& alphaField2 = str->GetSensitivityField(1).Get();
  std::vector<double> alpha1(nNodes);
  std::vector<double> alpha2(nNodes);
  for(int i=0; i<nNodes; ++i)
    { alpha1[i] = alphaField1[i];
      alpha2[i] = alphaField2[i]; }
  
  // Compute the state at P0 +/- epsilon * numerical difference
  std::vector<double> alpha1Num(nNodes);
  const double P_EPS = 1.e-4;
  load_params.values[0] += P_EPS;
  str->ComputeState(load_params, bc_params, solve_params);
  const auto& thetaField = str->GetStateField().Get();
  for(int i=0; i<nNodes; ++i)
    alpha1Num[i] = thetaField[i];
  load_params.values[0] -= 2.*P_EPS;
  str->ComputeState(load_params, bc_params, solve_params);
  for(int i=0; i<nNodes; ++i)
    { alpha1Num[i] -= thetaField[i];
      alpha1Num[i] /= (2.*P_EPS); }
  // Restore the original load
  load_params.values[0] += P_EPS;

    // Compute the state at P1 +/- epsilon & numerical difference
  std::vector<double> alpha2Num(nNodes);
  load_params.values[1] += P_EPS;
  str->ComputeState(load_params, bc_params, solve_params);
  for(int i=0; i<nNodes; ++i)
    alpha2Num[i] = thetaField[i];
  load_params.values[1] -= 2.*P_EPS;
  str->ComputeState(load_params, bc_params, solve_params);
  for(int i=0; i<nNodes; ++i)
    { alpha2Num[i] -= thetaField[i];
      alpha2Num[i] /= (2.*P_EPS); }
  // Restore the original load
  load_params.values[1] += P_EPS;

  // Plot the computed and numerical sensitivities
  std::fstream pfile;
  pfile.open((char*)"ss.dat", std::ios::out);
  for(int i=0; i<nNodes; ++i)
    pfile << coordinates[i]<<" "<<alpha1[i]<<" "<<alpha1Num[i]<<" "<<alpha2[i]<<" "<<alpha2Num[i]<<"\n";
  pfile.flush(); pfile.close();

  // Preliminary check on accuracy
  for(int i=0; i<nNodes; ++i)
    { assert(std::abs(alpha1[i]-alpha1Num[i])+std::abs(alpha2[i]-alpha2Num[i])<1.e-3); }
  
  // Clean up
  delete str;
  for(auto& x:load_params.dirs) delete x;
  for(auto& x:load_params.moments) delete x;
  PetscFinalize();
}
