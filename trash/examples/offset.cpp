// Sriramajayam

#include <el_TendonElastica.h>
#include <el_OffsetTendonLoadDirection.h>
#include <el_OffsetTendonMoment.h>
#include <el_P11DElement.h>
#include <iostream>
#include <fstream>

int main(int argc, char** argv)
{
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);
  
  // Create a uniform 1D mesh
  const int nNodes = 50;
  const double h = 1./static_cast<double>(nNodes-1);
  std::vector<double> coordinates(nNodes);
  for(int n=0; n<nNodes; ++n)
    coordinates[n] = h*static_cast<double>(n);
  const int nElements = nNodes-1;
  std::vector<int> connectivity(2*nElements);
  for(int e=0; e<nElements; ++e)
    { connectivity[2*e] = e+1;
      connectivity[2*e+1] = e+2; }
  Segment<1>::SetGlobalCoordinatesArray(coordinates);

  // Create 2 loads
  const int nLoads = 2;
  std::vector<int> PNodes(nLoads);
  PNodes[0] = nNodes/2;
  PNodes[1] = nNodes-1;
  el::LoadParams load_params;
  load_params.values.resize(nLoads);
  load_params.dirs.resize(nLoads);
  load_params.moments.resize(nLoads);

  // Create 2 routing posts
  std::vector<el::RoutingPost> RtPosts(2);
  RtPosts[0].center[0] = 0.; RtPosts[0].center[1] = -0.25;
  RtPosts[1].center[0] = 0.; RtPosts[1].center[1] = 0.25;
  RtPosts[0].radius = 0.; RtPosts[0].orientation = 0.;
  RtPosts[1].radius = 0.; RtPosts[1].orientation = 0.;

  // Create loading directions
  load_params.dirs[0] =
    new el::OffsetTendonLoadDirection(PNodes[0], 0, nLoads, -0.1, RtPosts[0]);
  load_params.dirs[1] = new
    el::OffsetTendonLoadDirection(PNodes[1], 1, nLoads, 0.1, RtPosts[1]);

  // Create moments
  load_params.moments[0] = new el::OffsetTendonMoment(load_params.dirs[0]);
  load_params.moments[1] = new el::OffsetTendonMoment(load_params.dirs[1]);

  // Dirichlet BCs
  el::DirichletBCs bc_params;
  bc_params.dofs = std::vector<int>({0});
  bc_params.values = std::vector<double>({0.});

  // Solve parameters
  el::Solve_Params solve_params({
      .consistentLinearization = true,
	.EPS = 1.e-10,
	.resScale = 1.,
	.dofScale = 1.,
	.nMaxIters = 25,
	.verbose = false});

  // Create strut
  const double EI = 1.;
  el::TendonElastica* str = new el::TendonElastica(coordinates, connectivity,
						   EI, PNodes);

  // Set the loads
  load_params.values[0] = 1.;
  load_params.values[1] = 2.;
  
  // Compute the state & sensitivities
  str->ComputeStateAndSensitivities(load_params, bc_params, solve_params);
  const auto& state = str->GetStateField().Get();
  
  // Compute xy coordinates
  std::vector<double> xy;
  el::AngleToCartesianMap(str->GetStateField(), str->GetElementArray(),
			  str->GetLocalToGlobalMap(), xy);

  // Plot the state and deformed shape
  std::fstream stream;
  stream.open("ss.dat", std::ios::out);
  for(int n=0; n<nNodes; ++n)
    stream<<coordinates[n]<<" "<<state[n]<<" "<<xy[2*n]<<" "<<xy[2*n+1]<<"\n";
  stream.close();

  // Plot the sensitivities wrt the two loads
  const auto& alpha = str->GetSensitivityField(0).Get();
  const auto& beta = str->GetSensitivityField(1).Get();
  stream.open("sense.dat", std::ios::out);
  for(int n=0; n<nNodes; ++n)
    stream<<coordinates[n]<<" "<<alpha[n]<<" "<<beta[n]<<"\n";
  stream.close();

  
  // Clean up
  for(auto& x:load_params.dirs) delete x;
  for(auto& x:load_params.moments) delete x;
  delete str;
  PetscFinalize();
}
