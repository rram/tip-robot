// Sriramajayam

#include <el_Load_Optimizer.h>
#include <el_Objective_AngleProfile.h>
#include <el_TendonElastica.h>
#include <el_TendonLoadDirection.h>
#include <el_OffsetTendonLoadDirection.h>
#include <el_OffsetTendonMoment.h>
#include <el_P11DElement.h>
#include <functional>
#include <iostream>
#include <fstream>
#include <random>

double scaleFactor = 0.;

// Desired shape
double ThetaFunc(const double& s)
{ return scaleFactor*s*s; }

// Objective function
void ObjectiveFunc(const double& s, const double& theta,
		   double* Jval, double* dJval, void* params);
  
int main(int argc, char** argv)
{
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);
  
  // Create a uniform 1D mesh
  const int nNodes = 50;
  const double h = 0.6/static_cast<double>(nNodes-1);
  std::vector<double> coordinates(nNodes);
  for(int n=0; n<nNodes; ++n)
    coordinates[n] = h*static_cast<double>(n);
  const int nElements = nNodes-1;
  std::vector<int> connectivity(2*nElements);
  for(int e=0; e<nElements; ++e)
    { connectivity[2*e] = e+1;
      connectivity[2*e+1] = e+2; }
  Segment<1>::SetGlobalCoordinatesArray(coordinates);

  // Create 2 loads
  const int nLoads = 2;
  std::vector<int> PNodes(nLoads);
  PNodes[0] = nNodes/2;
  PNodes[1] = nNodes-1;
  el::LoadParams load_params;
  load_params.values.resize(nLoads);
  std::fill(load_params.values.begin(), load_params.values.end(), 0.);
  load_params.dirs.resize(nLoads);
  load_params.moments.resize(nLoads);

  // Create routing posts
  const double RtPoints[] = {0.043959, -0.08924, 0.02383, 0.061697};
  std::vector<el::RoutingPost> RtPosts(nLoads);
  for(int i=0; i<nLoads; ++i)
    { RtPosts[i].center[0] = RtPoints[nLoads*i+0];
      RtPosts[i].center[1] = RtPoints[nLoads*i+1];
      RtPosts[i].radius = 0.;
      RtPosts[i].orientation = 0.;
    }
  std::vector<double> offsets({-0.04,0.04});
  
  // Create loading directions & moments
  for(int i=0; i<nLoads; ++i)
    {
      load_params.dirs[i] = new el::OffsetTendonLoadDirection(PNodes[i], i, nLoads, offsets[i], RtPosts[i]);
      load_params.moments[i] = new el::OffsetTendonMoment(load_params.dirs[i]);
    }
  
  // Dirichlet BCs
  el::DirichletBCs bc_params;
  bc_params.dofs = std::vector<int>({0});
  bc_params.values = std::vector<double>({0.});

  // Solve parameters
  el::Solve_Params solve_params({
      .consistentLinearization = true,
	.EPS = 1.e-10,
	.resScale = 1.,
	.dofScale = 1.,
	.nMaxIters = 25,
	.verbose = false});

  // Create strut
  const double EI = 1.;
  el::TendonElastica* str = new el::TendonElastica(coordinates, connectivity, EI, PNodes);

  // Objective
  el::AngleProfileFunction f_objective(ObjectiveFunc);
  el::Objective_AngleProfile objective(f_objective);
  
  // Create load optimizer
  el::Load_Optimizer* Opt = new el::Load_Optimizer(*str);
  
  // Result
  el::Solve_Results result;

  // Optimize
  for(int step=0; step<20; ++step)
    {
      Opt->Optimize(load_params, bc_params, objective, solve_params, result);
      std::cout<<"\n\nOptimal load: "<<load_params.values[0]<<", "<<load_params.values[1]<<std::flush;

      // This solution
      const auto& state = str->GetStateField().Get();
      std::vector<double> xy;
      el::AngleToCartesianMap(str->GetStateField(),
			      str->GetElementArray(),
			      str->GetLocalToGlobalMap(),
			      xy);

      // Exact solution
      std::vector<double> exState(nNodes);
      for(int n=0; n<nNodes; ++n)
	exState[n] = ThetaFunc(coordinates[n]);
      std::vector<double> exact_xy;
      el::AngleToCartesianMap(exState,
			      str->GetElementArray(),
			      str->GetLocalToGlobalMap(),
			      exact_xy);

      // Plot
      std::fstream stream;
      stream.open("optstate.dat", std::ios::out);
      for(int n=0; n<nNodes; ++n)
	stream << coordinates[n] << " "<<exState.Get(n)[0]<<" "<<state[n]<<" "
	       <<exact_xy[2*n]<<" "<<exact_xy[2*n+1]<<" "<<xy[2*n]<<" "<<xy[2*n+1]<<"\n";
      stream.close();

      // Increase the scale factor
      scaleFactor += 0.1;
    }
  
  // Clean up
  for(auto& x:load_params.dirs) delete x;
  for(auto& x:load_params.moments) delete x;
  delete str;
  delete Opt;
  PetscFinalize();
}

  // Objective function
void ObjectiveFunc(const double& s, const double& theta,
		   double* Jval, double* dJval, void* params)
{
  // Desired value
  const double val = ThetaFunc(s);
  if(Jval!=nullptr)
    (*Jval) = 0.5*(theta-val)*(theta-val);
  if(dJval!=nullptr)
    (*dJval) = theta-val;
  return;
}
  
