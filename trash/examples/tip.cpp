// Sriramajayam

#include <el_TendonElastica.h>
#include <el_OffsetTendonLoadDirection.h>
#include <el_OffsetTendonMoment.h>
#include <el_P11DElement.h>
#include <iostream>
#include <fstream>
#include <random>


// Helper function for solution
void Solve(el::TendonElastica* str,
	   const el::DirichletBCs& bc_params,
	   el::LoadParams& load_params,
	   const el::Solve_Params& solve_params,
	   const std::vector<int>& PNodes,
	   const std::vector<el::RoutingPost>& RtPosts,
	   const std::vector<double>& offset,
	   double* tip, double* len);
  
int main(int argc, char** argv)
{
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);
  
  // Create a uniform 1D mesh
  const int nNodes = 50;
  const double h = 0.6/static_cast<double>(nNodes-1);
  std::vector<double> coordinates(nNodes);
  for(int n=0; n<nNodes; ++n)
    coordinates[n] = h*static_cast<double>(n);
  const int nElements = nNodes-1;
  std::vector<int> connectivity(2*nElements);
  for(int e=0; e<nElements; ++e)
    { connectivity[2*e] = e+1;
      connectivity[2*e+1] = e+2; }
  Segment<1>::SetGlobalCoordinatesArray(coordinates);

  // Create 2 loads
  const int nLoads = 2;
  std::vector<int> PNodes(nLoads);
  PNodes[0] = nNodes/2;
  PNodes[1] = nNodes-1;
  el::LoadParams load_params;
  load_params.values.resize(nLoads);
  std::fill(load_params.values.begin(), load_params.values.end(), 0.);
  load_params.dirs.resize(nLoads);
  load_params.moments.resize(nLoads);

  // Create routing posts
  const double RtPoints[] = {0.043959, -0.08924, 0.02383, 0.061697};
  std::vector<el::RoutingPost> RtPosts(nLoads);
  for(int i=0; i<nLoads; ++i)
    { RtPosts[i].center[0] = RtPoints[nLoads*i+0];
      RtPosts[i].center[1] = RtPoints[nLoads*i+1];
      RtPosts[i].radius = 0.;
      RtPosts[i].orientation = 0.;
    }
  std::vector<double> offsets({-0.04,0.04});
  
  // Create loading directions & moments
  for(int i=0; i<nLoads; ++i)
    {
      load_params.dirs[i] = new el::OffsetTendonLoadDirection(PNodes[i], i, nLoads, offsets[i], RtPosts[i]);
      load_params.moments[i] = new el::OffsetTendonMoment(load_params.dirs[i]);
    }
  
  // Dirichlet BCs
  el::DirichletBCs bc_params;
  bc_params.dofs = std::vector<int>({0});
  bc_params.values = std::vector<double>({0.});

  // Solve parameters
  el::Solve_Params solve_params({
      .consistentLinearization = true,
	.EPS = 1.e-10,
	.resScale = 1.,
	.dofScale = 1.,
	.nMaxIters = 25,
	.verbose = false});

  // Create strut
  const double EI = 1.;
  el::TendonElastica* str = new el::TendonElastica(coordinates, connectivity,
						   EI, PNodes);

  // Random step sizes
  std::random_device rd; 
  std::mt19937 gen(rd());
  std::uniform_real_distribution<> dis0(-1.,2.);
  std::uniform_real_distribution<> dis1(-0.25, 0.5);
  
  std::fstream stream;
  stream.open((char*)"ws.dat", std::ios::app);
  double tip[2], len[2];
  int count = 0;
  while(true)
    {
      double dP0 = dis0(gen);
      double dP1 = dis1(gen);
      if(len[0]<0.05) dP0 = -std::abs(dP0);
      if(len[1]<0.05) dP1 = -std::abs(dP1);
      double try_P0 = load_params.values[0] + dP0;
      double try_P1 = load_params.values[1] + dP1;
      if(try_P0>-1.e-6 && try_P1>-1.e-6)
	{
	  load_params.values[0] = try_P0;
	  load_params.values[1] = try_P1;
	}
      //if(load_params.values[0]+dP0<P0 && len[0]>0.05) load_params.values[0] += dP0;
      //if(load_params.values[1]+dP1<P1 && len[1]>0.05) load_params.values[1] += dP1;
      //if(load_params.values[0]+dP0>=P0 && load_params.values[1]+dP1>=P1) break;
      if(count>1000) break;
      
      std::cout<<"\n\n"<<++count<<": solving with load "<<load_params.values[0]<<", "<<load_params.values[1]
	       <<"\ndP0: "<<dP0<<", dP1: "<<dP1<<", len: "<<len[0]<<", "<<len[1]<<std::flush;
      Solve(str, bc_params, load_params, solve_params, PNodes, RtPosts, offsets, tip, len);
      stream<<load_params.values[0]<<" "<<load_params.values[1]<<" "
	    <<tip[0]<<" "<<tip[1]<<" "
	    <<len[0]<<" "<<len[1]<<"\n";
      stream.flush();
      //if(len[0]<0.05 || len[1]<0.05) break;
    }
  stream.close();
  
  
  // Clean up
  for(auto& x:load_params.dirs) delete x;
  for(auto& x:load_params.moments) delete x;
  delete str;
  PetscFinalize();
}


void Solve(el::TendonElastica* str,
	   const el::DirichletBCs& bc_params,
	   el::LoadParams& load_params,
	   const el::Solve_Params& solve_params,
	   const std::vector<int>& PNodes,
	   const std::vector<el::RoutingPost>& RtPosts,
	   const std::vector<double>& offset,
	   double* tip, double* len)
{
  static int count = 0;
  str->ComputeState(load_params, bc_params, solve_params);
  std::vector<double> xy;
  el::AngleToCartesianMap(str->GetStateField(), str->GetElementArray(),
			  str->GetLocalToGlobalMap(), xy);
  const auto& state = str->GetStateField().Get();
  tip[0] = xy[xy.size()-2];
  tip[1] = xy[xy.size()-1];
  const int nNodes = static_cast<int>(xy.size())/2;
  std::string filename = "ss-"+std::to_string(count++)+".dat";
  std::fstream pfile;
  pfile.open(filename.c_str(), std::ios::out);
  for(int i=0; i<nNodes; ++i)
		 pfile<<xy[2*i]<<" "<<xy[2*i+1]<<"\n";
						 pfile.close();
  const int nLoads = static_cast<int>(PNodes.size());
  for(int t=0; t<nLoads; ++t)
    {
      const double& theta = state[PNodes[t]];
      const double* Xn = &xy[2*PNodes[t]];
      const double Xl[] = {Xn[0]-offset[t]*std::sin(theta),
			   Xn[1]+offset[t]*std::cos(theta)};
      len[t] = std::sqrt(std::pow(Xl[0]-RtPosts[t].center[0],2.)+std::pow(Xl[1]-RtPosts[t].center[1],2.));
    }
  return;
}
