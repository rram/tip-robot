// Sriramajayam

#include <el_Load_Optimizer.h>
#include <el_TendonElastica.h>
#include <el_OffsetTendonLoadDirection.h>
#include <el_OffsetTendonMoment.h>
#include <el_Objective_AngleProfile.h>
#include <el_P11DElement.h>
#include <fstream>
#include <iostream>

// Objective function
void ObjFunc(const double& s, const double& theta,
	     double* fval, double* dfval, void*);

int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);

  // Create a uniform 1D mesh
  const int nNodes = 40;
  const double h = 1./static_cast<double>(nNodes-1);
  std::vector<double> coordinates(nNodes);
  for(int n=0; n<nNodes; ++n)
    coordinates[n] = h*static_cast<double>(n);
  const int nElements = nNodes-1;
  std::vector<int> connectivity(2*nElements);
  for(int e=0; e<nElements; ++e)
    { connectivity[2*e] = e+1;
      connectivity[2*e+1] = e+2; }
  Segment<1>::SetGlobalCoordinatesArray(coordinates);

  // Number of loads
  const int nLoads = 1;
  std::vector<int> PNodes({nNodes-1});

  // Create the elastica
  const double EI = 1.;
  el::TendonElastica * str = new el::TendonElastica(coordinates, connectivity,
						    EI, PNodes);

  // Create offset tendon loads + moments
  el::LoadParams load_params;
  load_params.values.resize(nLoads);
  load_params.values[0] = 0;
  load_params.dirs.resize(nLoads);
  const double offsets[] = {0.05};
  el::RoutingPost RtPost;
  RtPost.center[0] = 0.; RtPost.center[1] = 0.5;
  RtPost.radius = 0.; RtPost.orientation = 0.;
  load_params.dirs[0] = new el::OffsetTendonLoadDirection(PNodes[0],0,nLoads,offsets[0],RtPost);
  load_params.moments.resize(nLoads);
  load_params.moments[0] = new el::OffsetTendonMoment(load_params.dirs[0]);

  // Dirichlet BCs: clamp the left end
  el::DirichletBCs bc_params;
  bc_params.dofs = std::vector<int>({0});
  bc_params.values = std::vector<double>({0.});

  // Solver parameters
  el::Solve_Params solve_params({
      .consistentLinearization = false,
	.EPS = 1.e-10,
	.resScale = 1.,
	.dofScale = 1.,
	.nMaxIters = 25,
	.verbose = true});

  // Create load optimizer
  el::Load_Optimizer* Opt = new el::Load_Optimizer(*str);
  std::function<decltype(ObjFunc)> obj_func_ptr(ObjFunc);
  el::Objective_AngleProfile objective(obj_func_ptr);

  // Result
  el::Solve_Results result;
  Opt->Optimize(load_params, bc_params, objective, solve_params, result);

  // Print the solution
  std::cout<<"\nOptimal load: "<<load_params.values[0]<<std::flush;
  const auto& state = str->GetStateField().Get();
  std::vector<double> xy;
  el::AngleToCartesianMap(str->GetStateField(), str->GetElementArray(),
			  str->GetLocalToGlobalMap(), xy);
  std::fstream stream;
  stream.open("optstate.dat", std::ios::out);
  for(int n=0; n<nNodes; ++n)
    stream<<coordinates[n]<<" "<<state[n]<<" "<<xy[2*n]<<" "<<xy[2*n+1]<<"\n";
  stream.close();
  
  // Clean up
  delete str;
  delete Opt;
  for(auto& x:load_params.dirs) delete x;
  for(auto& x:load_params.moments) delete x;
  PetscFinalize();
}


// Objective function
void ObjFunc(const double& s, const double& theta,
	     double* fval, double* dfval, void* params)
{
  double target = 0.1*s*(s-2.);
  if(fval!=nullptr)
    (*fval) = 0.5*(theta-target)*(theta-target);
  if(dfval!=nullptr)
    (*dfval) = (theta-target);
  return;
}
