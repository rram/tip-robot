// Sriramajayam

#include <el_Load_Optimizer.h>
#include <el_Objective_PointControl.h>
#include <el_Objective_PointwiseControl.h>
#include <el_TendonElastica.h>
#include <el_TendonLoadDirection.h>
#include <el_OffsetTendonLoadDirection.h>
#include <el_OffsetTendonMoment.h>
#include <el_P11DElement.h>
#include <iostream>
#include <fstream>
#include <random>

  
int main(int argc, char** argv)
{
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);
  
  // Create a uniform 1D mesh
  const int nNodes = 50;
  const double h = 0.6/static_cast<double>(nNodes-1);
  std::vector<double> coordinates(nNodes);
  for(int n=0; n<nNodes; ++n)
    coordinates[n] = h*static_cast<double>(n);
  const int nElements = nNodes-1;
  std::vector<int> connectivity(2*nElements);
  for(int e=0; e<nElements; ++e)
    { connectivity[2*e] = e+1;
      connectivity[2*e+1] = e+2; }
  Segment<1>::SetGlobalCoordinatesArray(coordinates);

  // Create 2 loads
  const int nLoads = 2;
  std::vector<int> PNodes(nLoads);
  PNodes[0] = nNodes/2;
  PNodes[1] = nNodes-1;
  el::LoadParams load_params;
  load_params.values.resize(nLoads);
  std::fill(load_params.values.begin(), load_params.values.end(), 0.);
  load_params.dirs.resize(nLoads);
  load_params.moments.resize(nLoads);

  // Create routing posts
  const double RtPoints[] = {0.043959, -0.08924, 0.02383, 0.061697};
  //const double RtPoints[] = {0., -0.3, 0., 0.3};  
  std::vector<el::RoutingPost> RtPosts(nLoads);
  for(int i=0; i<nLoads; ++i)
    { RtPosts[i].center[0] = RtPoints[nLoads*i+0];
      RtPosts[i].center[1] = RtPoints[nLoads*i+1];
      RtPosts[i].radius = 0.;
      RtPosts[i].orientation = 0.;
    }
  std::vector<double> offsets({-0.04,0.04});
  
  // Create loading directions & moments
  for(int i=0; i<nLoads; ++i)
    {
      load_params.dirs[i] = new el::OffsetTendonLoadDirection(PNodes[i], i, nLoads, offsets[i], RtPosts[i]);
      load_params.moments[i] = new el::OffsetTendonMoment(load_params.dirs[i]);
    }
  
  // Dirichlet BCs
  el::DirichletBCs bc_params;
  bc_params.dofs = std::vector<int>({0});
  bc_params.values = std::vector<double>({0.});

  // Solve parameters
  el::Solve_Params solve_params({
      .consistentLinearization = true,
	.EPS = 1.e-10,
	.resScale = 1.,
	.dofScale = 1.,
	.nMaxIters = 25,
	.verbose = false});

  // Create strut
  const double EI = 1.;
  el::TendonElastica* str = new el::TendonElastica(coordinates, connectivity, EI, PNodes);

  // Load up to a point
  const double P0 = 25.;
  const double P1 = 10.5;
  for(int i=0; i<=20; ++i)
    {
      std::cout<<"\n"<<i<<std::flush;
      load_params.values[1] = static_cast<double>(i)*P1/20.;
      str->ComputeStateAndSensitivities(load_params, bc_params, solve_params);
    }
  for(int i=0; i<=20; ++i)
    {
      std::cout<<"\n"<<i<<std::flush;
      load_params.values[0] = static_cast<double>(i)*P0/20.;
      str->ComputeStateAndSensitivities(load_params, bc_params, solve_params);
    }

  const auto& state = str->GetStateField().Get();
  std::vector<double> xy;
  el::AngleToCartesianMap(str->GetStateField(),
			  str->GetElementArray(),
			  str->GetLocalToGlobalMap(),
			  xy);

  std::fstream stream;
  stream.open("initstate.dat", std::ios::out);
  for(int n=0; n<nNodes; ++n)
    stream << coordinates[n] << " "<<state[n]<<" "<<xy[2*n]<<" "<<xy[2*n+1]<<"\n";
  stream.close();
  
  // Create load optimizer
  el::Load_Optimizer* Opt = new el::Load_Optimizer(*str);
  
  // Objective
  el::Objective_PointControlSpec target;
  target.nodenum = nNodes-1;
  target.x_tgt = xy[2*(nNodes-1)];  target.y_tgt = xy[2*(nNodes-1)+1];
  target.Coef_x = 1.;  target.Coef_y = 1.;
  target.theta_tgt = 0.; target.Coef_theta = 0.; // no theta control
  target.P_tgt = std::vector<double>({0.,0.}); // Load regularization
  target.Coef_P = std::vector<double>({0.,0.});
  el::Objective_PointControl objective;
  objective.Set(target);
  
  // Result
  el::Solve_Results result;

  // Optimize
  Opt->Optimize(load_params, bc_params, objective, solve_params, result);
  std::cout<<"\n\nOptimal load: "<<load_params.values[0]<<", "<<load_params.values[1]<<std::flush;

  for(int i=0; i<10; ++i)
    {
      target.x_tgt -= 0.01;
      target.P_tgt[0] = load_params.values[0];
      target.P_tgt[1] = load_params.values[1];
      target.Coef_P = std::vector<double>({1.e-2, 1.e-2});
      for(int k=0; k<4; ++k)
	{
	  target.Coef_P[0] /= 10.;
	  target.Coef_P[1] /= 10.;
	  objective.Set(target);
	  Opt->Optimize(load_params, bc_params, objective, solve_params, result);
	  std::cout<<"\nOptimal load: "
		   <<load_params.values[0]<<", "<<load_params.values[1]<<"\n\n"<<std::flush;
	}
    }

   for(int i=0; i<10; ++i)
    {
      target.y_tgt += 0.01;
      target.P_tgt[0] = load_params.values[0];
      target.P_tgt[1] = load_params.values[1];
      target.Coef_P = std::vector<double>({1.e-2, 1.e-2});
      for(int k=0; k<4; ++k)
	{
	  target.Coef_P[0] /= 10.;
	  target.Coef_P[1] /= 10.;
	  objective.Set(target);
	  Opt->Optimize(load_params, bc_params, objective, solve_params, result);
	  std::cout<<"\nOptimal load: "
		   <<load_params.values[0]<<", "<<load_params.values[1]<<"\n\n"<<std::flush;
	}
    }
   
  el::AngleToCartesianMap(str->GetStateField(),
			  str->GetElementArray(),
			  str->GetLocalToGlobalMap(),
			  xy);
  stream.open("optstate.dat", std::ios::out);
  for(int n=0; n<nNodes; ++n)
    stream << coordinates[n] << " "<<state[n]<<" "<<xy[2*n]<<" "<<xy[2*n+1]<<"\n";
  stream.close();
  
  // Clean up
  for(auto& x:load_params.dirs) delete x;
  for(auto& x:load_params.moments) delete x;
  delete str;
  delete Opt;
  PetscFinalize();
}
