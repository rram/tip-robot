// Sriramajayam

#include <el_TendonElastica.h>
#include <el_ConstantLoadDirection.h>
#include <el_FollowerLoadDirection.h>
#include <el_TendonLoadDirection.h>
#include <P11DElement.h>
#include <el_Objective_PointwiseControl.h>
#include <random>
#include <iostream>
#include <fstream>

int main(int argc, char** argv)
{
  // Choose the number of loads
  // ---------------------------
  const int nLoads = 2;
  
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);

  // Create a uniform 1D mesh on the interval [0,1]
  // ------------------------------------------------
  const int nNodes = 41;
  const double h = 1./static_cast<double>(nNodes-1);
  std::vector<double> coordinates(nNodes);
  for(int n=0; n<nNodes; ++n)
    coordinates[n] = 0.+h*static_cast<double>(n);
  const int nElements = nNodes-1;
  std::vector<int> connectivity(2*nElements);
  for(int e=0; e<nElements; ++e)
    { connectivity[2*e] = e+1;
      connectivity[2*e+1] = e+2; }
  Segment<1>::SetGlobalCoordinatesArray(coordinates);
  std::cout<<"\nNumber of elements: "<<nElements<<std::flush;

  // Create N loads, uniformly spaced and of the specified type
  // -----------------------------------------------------------
  std::vector<int> PNodes({20,40});
  std::cout<<"\nLoading node locations: "
	   <<coordinates[PNodes[0]]<<", "<<coordinates[PNodes[1]];

  // Set load values
  el::LoadParams load_params;
  load_params.values.resize(nLoads);
  load_params.dirs.resize(nLoads);
  load_params.values = std::vector<double>({std::sqrt(3.), std::sqrt(2.)});

  // Create loading directions
  const double angles[] = {3.*M_PI/2., 2.*M_PI/3.};
  for(int i=0; i<nLoads; ++i)
    { const double dir[] = {std::cos(angles[i]), std::sin(angles[i])};
      load_params.dirs[i] = new el::ConstantLoadDirection(PNodes[i],i,nLoads,dir); }
  
  // Dirichlet BCs: clamp the left end
  // ----------------------------------
  std::map<int,double> bc_params{};
  bc_params[0] = 0.;
  
  // Solve parameters
  // ------------------
  el::Solve_Params solve_params({
      .consistentLinearization=false, // Automatically consistent
	.EPS = 1.e-10,
	.resScale = 1.,
	.dofScale = 1.,
	.nMaxIters = 25,
	.verbose = true});

  // Create strut
  // --------------
  const double EI = 1.;
  const double origin[] = {0.,0.};
  el::TendonElastica* str = new el::TendonElastica(coordinates, connectivity, EI, PNodes, origin);
  
  // Compute the state and sensitivities
  // -----------------------------------
  str->ComputeStateAndSensitivities(load_params, bc_params, solve_params);
  
  // Plot the state and sensitivities
  // -----------------------------------
  const auto& state = str->GetStateField();
  const auto& alpha0 = str->GetSensitivityField(0);
  const auto& alpha1 = str->GetSensitivityField(1);
  std::fstream stream;
  stream.open("ss.dat", std::ios::out);
  for(int n=0; n<nNodes; ++n)
    stream << coordinates[n] << " "<<state[n]<<" "
	   <<alpha0[n]<<" "<<alpha1[n]<<"\n";
  stream.close();

  // Compute xy coordinates
  // -----------------------
  const auto& xy = str->GetCartesianCoordinates();
  const double xtip = xy[2*(nNodes-1)];
  const double ytip = xy[2*(nNodes-1)+1];
  
  // test sensitivities of the tip wrt each load
  const auto& xysense = str->GetTipSensitivities();

  // Compute the sensitivities in an alternate way
  el::Objective_PointwiseControl Obj(nNodes-1, std::vector<double>{0.,0.});
  
  el::Objective_Params objParams({.load_params=&load_params, .ElmArray=&str->GetElementArray(), .L2GMap=&str->GetLocalToGlobalMap()});
  el::Objective_PointControlSpec objSpec;
  objSpec.nodenum=nNodes-1;
  objSpec.x_tgt=0.;
  objSpec.y_tgt=0.;
  objSpec.theta_tgt=0.;
  objSpec.Coef_theta=0.;
  objSpec.P_tgt=std::vector<double>(nLoads,0.);
  objSpec.Coef_P=std::vector<double>(nLoads,0.);
  assert(std::abs(xtip-objSpec.x_tgt)+std::abs(ytip-objSpec.y_tgt)>1.e-3);
  double Jval;
  std::vector<double> dJvals;

  // Sensitivities of  xtip.
  objSpec.Coef_x=1.;
  objSpec.Coef_y=0.;
  Obj.Set(objSpec);
  Obj.Evaluate(str->GetStateField(), str->GetSensitivityFields(), objParams, Jval, dJvals);
  for(int L=0; L<nLoads; ++L)
    dJvals[L] /= (xtip-objSpec.x_tgt);
  std::cout<<"\nSensitivities of xtip: ";
  for(int L=0; L<nLoads; ++L)
    std::cout<<xysense[L][0]<<"(="<<dJvals[L]<<") "<<std::flush;

  // Sensitivities of ytip
  objSpec.Coef_x = 0.;
  objSpec.Coef_y = 1.;
  Obj.Set(objSpec);
  Obj.Evaluate(str->GetStateField(), str->GetSensitivityFields(), objParams, Jval, dJvals);
  for(int L=0; L<nLoads; ++L)
    dJvals[L] /= (ytip-objSpec.y_tgt);
  std::cout<<"\nSensitivities of ytip: ";
  for(int L=0; L<nLoads; ++L)
    std::cout<<xysense[L][1]<<"(="<<dJvals[L]<<") "<<std::flush;

  // Clean up
  delete str;
  for(auto&x: load_params.dirs) delete x;
  PetscFinalize();
}
