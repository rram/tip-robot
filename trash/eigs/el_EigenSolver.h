// Sriramajayam

#ifndef EL_EIGEN_SOLVER_H
#define EL_EIGEN_SOLVER_H

#include <slepceps.h>
#include <vector>

namespace el
{
  //! Class for managing the calculation of eigenvalues
  //! Assumes a linear non-Hermitian eigenvalue problem
  class EigenSolver
  {
  public:
    //! Constructor
    //! \param[in] matSize Size of the matrix
    //! \param[in] numEV Number of eigenpairs
    //! \param[in] which Which eigenpairs to compute
    EigenSolver(const int matSize, const int numEV, EPSWhich which);

    //! Destructor
    virtual ~EigenSolver();

    //! Destroy the eigen solver
    void Destroy();

    //! Solve
    //! \param[in] kMat Matrix for which to find eigenpairs
    //! \param[out] rparts Real parts of computed eigenvalues
    //! \param[out] iparts Imaginary parts of computed eigenvalues
    //! \param[out] error Error in the computed eigenvalues
    //! \return true if the solve was successful and false otherwise
    bool Solve(Mat kMat, std::vector<double>& rparts, std::vector<double>& iparts,
	       std::vector<double>& error);
    
    //! Returns the EPS context
    void GetEPS(EPS* retEPS);
    
  private:
    const int size; //!< Size of the matrix
    const int nev; //!< Number of requsted eigenvalues
    EPS eps;
    bool flag; //!< Persistence flag.
  };
    
}

#endif

