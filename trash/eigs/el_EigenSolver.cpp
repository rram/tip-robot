// Sriramajayam

#include <el_EigenSolver.h>
#include <cassert>

namespace el
{
  // Constructor
  EigenSolver::EigenSolver(const int matSize, const int numEV, EPSWhich which)
    :size(matSize), nev(numEV)
  {
    PetscErrorCode ierr;
    PetscBool init_flag;
    ierr = SlepcInitialized(&init_flag); CHKERRV(ierr);
    assert(init_flag==PETSC_TRUE);
    assert(nev>=1);
    assert(size>0);
    
    // Setup EPS
    ierr = EPSCreate(PETSC_COMM_WORLD, &eps); CHKERRV(ierr);
    ierr = EPSSetProblemType(eps, EPS_NHEP); CHKERRV(ierr);
    ierr = EPSSetWhichEigenpairs(eps, which); CHKERRV(ierr);
    ierr = EPSSetDimensions(eps, nev, PETSC_DEFAULT, PETSC_DEFAULT); CHKERRV(ierr);
    ierr = EPSSetFromOptions(eps); CHKERRV(ierr);
    
    // done
    flag = true;
  }

  // Destructor
  EigenSolver::~EigenSolver()
  { assert(flag==false); }

  // Destroy
  void EigenSolver::Destroy()
  {
    assert(flag==true);
    PetscErrorCode ierr;
    ierr = EPSDestroy(&eps); CHKERRV(ierr);
    flag = false;
    return;
  }

  // Solve
  bool EigenSolver::Solve(Mat kMat, std::vector<double>& rpart, std::vector<double>& ipart,
			  std::vector<double>& errors)
  {
    assert(flag);

    // Check sizes
    PetscErrorCode ierr;
    int rows, cols;
    ierr = MatGetSize(kMat, &rows, &cols); CHKERRQ(ierr);
    assert(rows==cols && rows==size);

    // Set the operator
    ierr = EPSSetOperators(eps, kMat, PETSC_NULL); CHKERRQ(ierr);

    // Solve
    ierr = EPSSolve(eps); CHKERRQ(ierr);
    
    // How many converged
    PetscInt nconv;
    ierr = EPSGetConverged(eps, &nconv); CHKERRQ(ierr);
    rpart.resize(nconv);
    ipart.resize(nconv);
    errors.resize(nconv);
    PetscScalar kr, ki;
    PetscReal error;
    for(int i=0; i<nconv; ++i)
      {
	ierr = EPSGetEigenvalue(eps, i, &kr, &ki); CHKERRQ(ierr);
	ierr = EPSComputeError(eps, i, EPS_ERROR_RELATIVE, &error); CHKERRQ(ierr);
	rpart[i] = static_cast<double>(kr);
	ipart[i] = static_cast<double>(ki);
	errors[i] = static_cast<double>(error);
      }

    if(nconv>=nev) return true;
    else return false;
  }

  // Return the eps context
  void EigenSolver::GetEPS(EPS* retEPS)
  { retEPS = &eps; }
}
