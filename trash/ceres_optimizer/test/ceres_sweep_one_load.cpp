// Sriramajayam

#include <el_Ceres_LoadOptimizer.h>
#include <el_ConstantLoadDirection.h>
#include <el_Objective_AngleProfile.h>
#include <P11DElement.h>
#include <iostream>
#include <fstream>

// Objective function
void ObjFunc(const double& x, const double& theta, double* fval, double* dfval, void*);

int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);

  // Create a uniform 1D mesh on the interval [0,1]
  const int nNodes = 20;
  const double h = 1./static_cast<double>(nNodes-1);
  std::vector<double> coordinates(nNodes);
  for(int n=0; n<nNodes; ++n)
    coordinates[n] = 0.+h*static_cast<double>(n);
  const int nElements = nNodes-1;
  std::vector<int> connectivity(2*nElements);
  for(int e=0; e<nElements; ++e)
    { connectivity[2*e] = e+1;
      connectivity[2*e+1] = e+2; }
  Segment<1>::SetGlobalCoordinatesArray(coordinates);
  
  // Create one load and a loading direction of the specified type
  const int nLoads = 1;
  std::vector<int> PNodes({nNodes/2});
  el::LoadParams load_params;
  load_params.values.resize(nLoads);
  load_params.values[0] = 0.;
  load_params.dirs.resize(nLoads);
  const double dir[] = {std::sqrt(1./2.), std::sqrt(1./2.)};
  load_params.dirs[0] = new el::ConstantLoadDirection(PNodes[0], 0, nLoads, dir);
  
  // Dirichlet BCs: clamp the left end
  std::map<int,double> bc_params{};
  bc_params[0] = 0.;

  // Solve parameters
  el::Solve_Params solve_params({
      .consistentLinearization = true,
      .EPS = 1.e-10,
	.resScale = 1.,
	.dofScale = 1.,
	.nMaxIters = 25,
	.verbose = true});
  
  // Create strut
  const double EI = 1.;
  const double origin[] = {0.,0.};
  el::TendonElastica *str = new el::TendonElastica(coordinates, connectivity, EI, PNodes, origin);

  // Objective function and parameters
  std::function<decltype(ObjFunc)> obj_func_ptr = ObjFunc;
  el::Objective_AngleProfile objective(obj_func_ptr);
  
  // Create load optimizer
  el::Ceres_LoadOptimizer Opt(*str, objective, load_params, bc_params, solve_params);
    
  // Optimize
  Opt.Optimize();
  const double OptLoad = load_params.values[0];
  std::cout<<"\n\nOptimal load: "<<OptLoad;

  // Set the initial guess for theta to be 0:
  load_params.values[0] = 0.;
  str->ComputeState(load_params, bc_params, solve_params);

  // Sweep loads over the range OptLoad +/- 1 in steps of 0.05.
  std::fstream jfile;
  jfile.open((char*)"psweep.dat", std::ios::out);
  double jmin;
  objective.Evaluate(*str, jmin);
  double try_OptLoad = 0.;
  for(int lstep=0; lstep<=40; ++lstep)
    {
      std::cout<<"\nLoad step: "<<lstep<<std::flush;
      load_params.values[0] = OptLoad-1.+static_cast<double>(lstep)*2./40.;
      str->ComputeState(load_params, bc_params, solve_params);
      double Jval;
      objective.Evaluate(*str, Jval);
      jfile<<load_params.values[0]<<" "<<Jval<<"\n";
      jfile.flush();
      if(Jval<jmin)
	{ jmin = Jval;
	  try_OptLoad = load_params.values[0]; }
    }
  jfile.close();
  std::cout<<"\n\nOptimal load computed from sweep: "<<try_OptLoad
	   <<" :should be: "<<OptLoad<<"\n"<<std::flush;
  
  // Clean up
  delete str;
  for(auto& x:load_params.dirs) delete x;
  PetscFinalize();
}


// Objective function
void ObjFunc(const double& s, const double& theta, double* fval, double* dfval, void* params)
{
  // target function: 0.1*s*(s-2)
  double target = 0.1*s*(s-2.);
  
  if(fval!=nullptr)
    (*fval) = 0.5*(theta-target)*(theta-target);
  if(dfval!=nullptr)
    (*dfval) = (theta-target);
  return;
}
