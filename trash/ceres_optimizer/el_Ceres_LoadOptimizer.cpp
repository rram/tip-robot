// Sriramajayam

#include <el_Ceres_LoadOptimizer.h>

namespace el
{
  // Implementation of objective and gradient evaluation

  // Constructor
  CeresFirstOrderFunction::
  CeresFirstOrderFunction(TendonElastica& istr,
			  ObjectiveFunctional& obj,
			  LoadParams& lp,
			  const std::map<int,double>& bp,
			  const Solve_Params& sp)
    :str(&istr),
     objective(&obj),
     nLoads(istr.GetNumLoads()),
     load_params(&lp),
     bc_params(&bp),
     solve_params(&sp)
  {}

  
  // Main functionality. Evaluate the objective and its gradient
  bool CeresFirstOrderFunction::Evaluate(const double* parameters,
					 double* cost,
					 double* gradient) const
  {
    assert(cost!=nullptr);

    // Copy parameters into the load
    for(int L=0; L<nLoads; ++L)
      load_params->values[L] = parameters[L];
    
    if(gradient==nullptr) // only objective
      {
	str->ComputeState(*load_params, *bc_params, *solve_params);
	objective->Evaluate(*str, *cost);
      }
    else // objective + gradient
      {
	str->ComputeStateAndSensitivities(*load_params, *bc_params, *solve_params);
	std::vector<double> dJvals(nLoads,0.);
	objective->Evaluate(*str, *cost, dJvals);
	for(int L=0; L<nLoads; ++L)
	  gradient[L] = dJvals[L];
      }

    // done
    return true;
  }

  
  // Implementation of optimizer 

  // Constructor
  Ceres_LoadOptimizer::Ceres_LoadOptimizer(TendonElastica& istr,
					   ObjectiveFunctional& obj,
					   LoadParams& lp,
					   const std::map<int,double>& bp,
					   const Solve_Params& sp)
    :nLoads(istr.GetNumLoads()),
     load_values(istr.GetNumLoads())
  {
    // Create the problem
    problem = new ceres::GradientProblem(new CeresFirstOrderFunction(istr, obj, lp, bp, sp));
  }

  // Destructor, nothing to do
  Ceres_LoadOptimizer::~Ceres_LoadOptimizer()
  { delete problem; }

  // Main functionality: optimize
  void Ceres_LoadOptimizer::Optimize()
  { ceres::Solve(options, *problem, load_values.data(), &summary); }

}
