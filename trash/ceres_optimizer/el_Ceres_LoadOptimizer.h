// Sriramajayam

#ifndef TR_CERES_LOAD_OPTIMIZER_H
#define TR_CERES_LOAD_OPTIMIZER_H

#include <el_TendonElastica.h>
#include <el_ObjectiveFunctional.h>
#include <ceres/ceres.h>

namespace el
{
  //! Helper class for evaluating objective and its gradient
  class CeresFirstOrderFunction: public ceres::FirstOrderFunction
  {
  public:
    //! Constructor
    CeresFirstOrderFunction(TendonElastica& istr, ObjectiveFunctional& obj,
			    LoadParams& lp,
			    const std::map<int,double>& bp,
			    const Solve_Params& sp);

    // Disable copy and assignment
    CeresFirstOrderFunction(const CeresFirstOrderFunction&) = delete;
    CeresFirstOrderFunction operator=(const CeresFirstOrderFunction&) = delete;
    
    //! Destructor, does nothing
    inline virtual ~CeresFirstOrderFunction() {}

    //! Main functionality. Evaluate the objective and its gradient
    virtual bool Evaluate(const double* parameters,
			  double* cost,
			  double* gradient) const;

    //! Return the number of parameters
    inline virtual int NumParameters() const
    { return nLoads; }
      
  private:
    TendonElastica* str; //! Pointer to the structure
    ObjectiveFunctional* objective; //!< Objective functional
    const int nLoads; //!< Number of optimization variables
    LoadParams* load_params; //!< load parameters;
    const std::map<int,double>* bc_params; //!< Pointer to dirichlet bcs
    const Solve_Params* solve_params; //!< Pointer to solver params
  };

  //! Wrapper for ceres functionality for load optimization
  class Ceres_LoadOptimizer
  {
  public:

    //! Constructor
    Ceres_LoadOptimizer(TendonElastica& istr,
			ObjectiveFunctional& obj,
			LoadParams& lp,
			const std::map<int,double>& bp,
			const Solve_Params& sp);
    
    //! Destructor
    virtual ~Ceres_LoadOptimizer();

    //! Main functionality: optimize
    void Optimize();

    //! Access solver options
    inline ceres::GradientProblemSolver::Options& GetOptions()
    { return options; }

    //! Access summary
    inline ceres::GradientProblemSolver::Summary& GetSummary()
    { return summary; }
    
  private:
    const int nLoads;
    std::vector<double> load_values;
    ceres::GradientProblem* problem;
    ceres::GradientProblemSolver::Options options;
    ceres::GradientProblemSolver::Summary summary;
  };
}

#endif
