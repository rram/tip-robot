// Sriramajayam

#include <el_NLopt_LoadOptimizer.h>
#include <el_TendonElastica.h>
#include <el_TendonLoadDirection.h>
#include <el_Objective_PointwiseControl.h>
#include <P11DElement.h>
#include <iostream>
#include <fstream>

std::vector<double> GetTargets();

void PlotXY(const std::string filename, const std::vector<double>& xy);

int main(int argc, char** argv)
{
  PetscInitialize(&argc, &argv, PETSC_NULLPTR, PETSC_NULLPTR);

  // Get the list of target points
  auto target_points = GetTargets();
  PlotXY("triangle.dat", target_points);
  
  // Create a uniform 1D mesh
  const int nNodes = 181;
  const double h = 0.6/static_cast<double>(nNodes-1);
  std::vector<double> coordinates(nNodes);
  for(int n=0; n<nNodes; ++n)
    coordinates[n] = h*static_cast<double>(n);

  // Create elastica
  const int nLoads = 2;
  std::vector<int> PNodes(nLoads);
  PNodes[0] = 0.2/h;
  PNodes[1] = nNodes-1;
  const double origin[] = {0.,0.};
  el::TendonElastica *str = new el::TendonElastica(coordinates, 1., PNodes, origin);
  
  // Create 2 loads
  el::LoadParams load_params;
  load_params.values.resize(nLoads,0.);
  load_params.dirs.resize(nLoads);
  const double RtPoints[] = {0.046334, -0.097126, 0.02611, 0.051106};
  el::RoutingPost RtPost;
  RtPost.center[0] = RtPoints[0]; RtPost.center[1] = RtPoints[1];
  RtPost.radius = 0.; RtPost.orientation = 0.;
  load_params.dirs[0] = new el::TendonLoadDirection(PNodes[0],0,*str,RtPost);
  RtPost.center[0] = RtPoints[2]; RtPost.center[1] = RtPoints[3];
  load_params.dirs[1] = new el::TendonLoadDirection(PNodes[1],1,*str,RtPost);

  // Dirichlet BCs
  std::map<int,double> bc_params{};
  bc_params[0] = 0.;
  
  // Solve parameters
  el::Solve_Params solve_params({
      .consistentLinearization = true,
	.EPS = 1.e-10,
	.resScale = 1.,
	.dofScale = 1.,
	.nMaxIters = 25,
	.verbose = false});

  // Optimization parameters
  el::NLopt_Params opt_params;
  opt_params.set_lb = false;
  opt_params.set_ub = false;
  opt_params.set_abs_f = false;
  opt_params.set_rel_f = true; opt_params.rel_ftol = 1.e-6;
  opt_params.set_abs_x = false;
  opt_params.set_rel_x = true; opt_params.rel_xtol = 1.e-6;
  
  // Objective
  el::Objective_PointwiseControl objective(nNodes-1, std::vector<double>{0.,0.});
        
  // Create load optimizer
  el::NLopt_LoadOptimizer opt(*str, objective, load_params, bc_params, solve_params);

  // Position the tip in the workspace
  const double P0 = 6.;
  const double P1 = 22.;
  const int nLoadSteps = 5;
  for(int s=0; s<nLoadSteps; ++s)
    {
      load_params.values[0] = P0*static_cast<double>(s)/static_cast<double>(nLoadSteps);
      load_params.values[1] = P1*static_cast<double>(s)/static_cast<double>(nLoadSteps);
      str->ComputeStateAndSensitivities(load_params, bc_params, solve_params);
    }
  auto& xy = str->GetCartesianCoordinates();
  PlotXY("init.dat", xy);

  // Dummy optimization run
  std::vector<double> tip(2);
  tip[0] = xy[2*(nNodes-1)];
  tip[1] = xy[2*(nNodes-1)+1];
  objective.SetNodeLocation(tip);
  opt.Optimize(opt_params);

  // Trace the triangle
  const int nTargets = static_cast<int>(target_points.size()/2);
  std::vector<double> tendon_lengths{};
  std::vector<double> tendon_loads{};
  const double* xy_P0 = &xy[2*PNodes[0]];
  const double* xy_P1 = &xy[2*PNodes[1]];
  for(int t=0; t<nTargets; ++t)
    {
      std::cout<<"\nTarget tip position "<<t+1<<" of "<<nTargets<<std::flush;
      tip[0] = target_points[2*t];
      tip[1] = target_points[2*t+1];
      objective.SetNodeLocation(tip);
      opt.Optimize(opt_params);

      // record
      //PlotXY("state-"+std::to_string(t)+".dat", xy);
      tendon_lengths.push_back(std::sqrt(std::pow(xy_P0[0]-RtPoints[0],2.)+std::pow(xy_P0[1]-RtPoints[1],2.)));
      tendon_lengths.push_back(std::sqrt(std::pow(xy_P1[0]-RtPoints[2],2.)+std::pow(xy_P1[1]-RtPoints[3],2.)));
      tendon_loads.push_back(load_params.values[0]);
      tendon_loads.push_back(load_params.values[1]);
    }

  // Print tendon lengths to file
  std::fstream pfile;
  pfile.open((char*)"summary.dat", std::ios::out);
  pfile<<"#index \t target point(x,y) \t loads(P0,P1) \t lengths(L0,L1) ";
  for(int t=0; t<nTargets; ++t)
    pfile << "\n" << t+1 <<"\t"
	  <<target_points[2*t]<<" "<<target_points[2*t+1]<<"\t"
	  <<tendon_loads[2*t]<<" "<<tendon_loads[2*t+1]<<"\t"
	  <<tendon_lengths[2*t]<<" "<<tendon_lengths[2*t+1];
  pfile.flush();
  pfile.close();
    
  // clean up
  for(auto& x:load_params.dirs) delete x;
  delete str;
  PetscFinalize();
}


std::vector<double> GetTargets()
{
  std::vector<double> points{};
  
  // Triangle
  const double A = 0.08*std::sqrt(3.);
  const double origin[] = {0.3, 0.27};
  const double V1[] = {origin[0]+A/2., origin[1]-A/(2.*std::sqrt(3.))};
  const double V2[] = {origin[0], origin[1]+A/(std::sqrt(3.))};
  const double V3[] = {origin[0]-A/2., origin[1]-A/(2.*std::sqrt(3.))};
  const double M[] = {0.5*(V1[0]+V2[0]), 0.5*(V1[1]+V2[1])};
  const double pi = M_PI;
  
  // Edges
  const double* Verts[] = {V1,V2,V3,V1,M};
  for(int edge=0; edge<4; ++edge)
    {    
      const double* src = Verts[edge];
      const double* tgt = Verts[edge+1];

      const int nsteps = 41;
      for(int i=0; i<=nsteps; ++i)
	{
	  const double lambda = static_cast<double>(i)/static_cast<double>(nsteps);
	  points.push_back( lambda*tgt[0] + (1.-lambda)*src[0] );
	  points.push_back( lambda*tgt[1] + (1.-lambda)*src[1] );
	}
    }

  // In-circle
  double radius = A/(2.*std::sqrt(3.));
  const int steps = 71;
  for(int i=0; i<= steps; ++i)
    {
      double theta = pi/6. + static_cast<double>(i)*2.*pi/static_cast<double>(steps);
      points.push_back( origin[0] + radius*cos(theta) );
      points.push_back( origin[1] + radius*sin(theta) );
    }

  // Direct to the center, then to vertex 1
  const double* NextVerts[] = {M,origin,V1};
  for(int edge=0; edge<2; ++edge)
    {    
      const double* src = NextVerts[edge];
      const double* tgt = NextVerts[edge+1];
      
      const int nsteps = 21;
      for(int i=0; i<=nsteps; ++i)
	{
	  const double lambda = static_cast<double>(i)/static_cast<double>(nsteps);
	  points.push_back( lambda*tgt[0] + (1.-lambda)*src[0] );
	  points.push_back( lambda*tgt[1] + (1.-lambda)*src[1] );
	}
    }

  // Draw circumcircle
  radius = A/std::sqrt(3.);
  const int csteps = 131;
  for(int i=0; i<= csteps; ++i)
    {
      double theta = -pi/6. + static_cast<double>(i)*2.*pi/static_cast<double>(csteps);
      points.push_back( origin[0] + radius*cos(theta) );
      points.push_back( origin[1] + radius*sin(theta) );
    }

  // done
  return points;
}

void PlotXY(const std::string filename, const std::vector<double>& xy)
{
  std::fstream pfile;
  pfile.open(filename.c_str(), std::ios::out);
  const int nPoints = static_cast<int>(xy.size()/2);
  for(int p=0; p<nPoints; ++p)
    pfile << xy[2*p]<<" "<<xy[2*p+1]<<"\n";
  pfile.close();
}
