# Sriramajayam

project(smiley)

# Copy assets
file(COPY ${CMAKE_SOURCE_DIR}/examples/smiley/smiley-spline.data DESTINATION ${CMAKE_BINARY_DIR}/examples/smiley/)

# Identical for all targets
foreach(TARGET draw_smiley)
  
  # Add this target
  add_executable(${TARGET}  ${TARGET}.cpp)
  
  # include headers
  target_include_directories(${TARGET} PUBLIC
    ${DGPP_INCLUDE_DIRS}
    ${CMAKE_SOURCE_DIR}/src/utils
    ${CMAKE_SOURCE_DIR}/src/ops
    ${CMAKE_SOURCE_DIR}/src/load_direction
    ${CMAKE_SOURCE_DIR}/src/elastica
    ${CMAKE_SOURCE_DIR}/src/objective
    ${CMAKE_SOURCE_DIR}/src/load_optimizer)
  
  # Link
  target_link_directories(${TARGET} PUBLIC ${DGPP_LIBRARY_DIRS})
  target_link_libraries(${TARGET} PUBLIC
    tr_nlopt tr_objective tr_elastica tr_direction tr_ops tr_utils
    ${DGPP_LIBRARIES})
  
  # choose appropriate compiler flags
  target_compile_features(${TARGET} PUBLIC ${tr_COMPILE_FEATURES})
  
  add_test(NAME ${TARGET} COMMAND ${TARGET})

endforeach()
