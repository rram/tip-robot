// Sriramajayam

#include <el_NLopt_LoadOptimizer.h>
#include <el_TendonElastica.h>
#include <el_TendonLoadDirection.h>
#include <el_Objective_PointwiseControl.h>
#include <P11DElement.h>
#include <iostream>
#include <fstream>

std::vector<double> ReadTargets(const std::string filename);

void PlotXY(const std::string filename, const std::vector<double>& xy);

int main(int argc, char** argv)
{
  PetscInitialize(&argc, &argv, PETSC_NULLPTR, PETSC_NULLPTR);

  // Get the list of target points
  auto target_points = ReadTargets("smiley-spline.data");
  PlotXY("smiley.dat", target_points);
  
  // Create a uniform 1D mesh
  const int nNodes = 181;
  const double h = 0.6/static_cast<double>(nNodes-1);
  std::vector<double> coordinates(nNodes);
  for(int n=0; n<nNodes; ++n)
    coordinates[n] = h*static_cast<double>(n);

  // Create elastica
  const int nLoads = 2;
  std::vector<int> PNodes(nLoads);
  PNodes[0] = 0.2/h;
  PNodes[1] = nNodes-1;
  const double origin[] = {0.,0.};
  el::TendonElastica *str = new el::TendonElastica(coordinates, 1., PNodes, origin);
  
  // Create 2 loads
  el::LoadParams load_params;
  load_params.values.resize(nLoads,0.);
  load_params.dirs.resize(nLoads);
  const double RtPoints[] = {0.046334, -0.097126, 0.02611, 0.051106};
  el::RoutingPost RtPost;
  RtPost.center[0] = RtPoints[0]; RtPost.center[1] = RtPoints[1];
  RtPost.radius = 0.; RtPost.orientation = 0.;
  load_params.dirs[0] = new el::TendonLoadDirection(PNodes[0],0,*str,RtPost);
  RtPost.center[0] = RtPoints[2]; RtPost.center[1] = RtPoints[3];
  load_params.dirs[1] = new el::TendonLoadDirection(PNodes[1],1,*str,RtPost);

  // Dirichlet BCs
  std::map<int,double> bc_params{};
  bc_params[0] = 0.;
  
  // Solve parameters
  el::Solve_Params solve_params({
      .consistentLinearization = true,
	.EPS = 1.e-10,
	.resScale = 1.,
	.dofScale = 1.,
	.nMaxIters = 25,
	.verbose = false});

  // Optimization parameters
  el::NLopt_Params opt_params;
  opt_params.set_lb = false;
  opt_params.set_ub = false;
  opt_params.set_abs_f = false;
  opt_params.set_rel_f = true; opt_params.rel_ftol = 1.e-6;
  opt_params.set_abs_x = false;
  opt_params.set_rel_x = true; opt_params.rel_xtol = 1.e-6;
  

  // Objective
  el::Objective_PointwiseControl objective(nNodes-1, std::vector<double>{0.,0.});
        
  // Create load optimizer
  el::NLopt_LoadOptimizer opt(*str, objective, load_params, bc_params, solve_params);

  // Position the tip in the workspace
  const double P0 = 5.;
  const double P1 = 28.;
  const int nLoadSteps = 5;
  for(int s=0; s<nLoadSteps; ++s)
    {
      load_params.values[0] = P0*static_cast<double>(s)/static_cast<double>(nLoadSteps);
      load_params.values[1] = P1*static_cast<double>(s)/static_cast<double>(nLoadSteps);
      str->ComputeStateAndSensitivities(load_params, bc_params, solve_params);
    }
  auto& xy = str->GetCartesianCoordinates();
  PlotXY("init.dat", xy);
  
  // Dummy optimization run
  std::vector<double> tip(2);
  tip[0] = xy[2*(nNodes-1)];
  tip[1] = xy[2*(nNodes-1)+1];
  objective.SetNodeLocation(tip);
  opt.Optimize(opt_params);

  // Trace the triangle
  const int nTargets = static_cast<int>(target_points.size()/2);
  std::vector<double> tendon_lengths{};
  std::vector<double> tendon_loads{};
  const double* xy_P0 = &xy[2*PNodes[0]];
  const double* xy_P1 = &xy[2*PNodes[1]];
  for(int t=0; t<nTargets; ++t)
    {
      std::cout<<"\nTarget tip position "<<t+1<<" of "<<nTargets<<std::flush;
      tip[0] = target_points[2*t];
      tip[1] = target_points[2*t+1];
      objective.SetNodeLocation(tip);
      opt.Optimize(opt_params);

      //if(t%10==0)
      //PlotXY("state-"+std::to_string(t)+".dat", xy);
      
      // record
      tendon_lengths.push_back(std::sqrt(std::pow(xy_P0[0]-RtPoints[0],2.)+std::pow(xy_P0[1]-RtPoints[1],2.)));
      tendon_lengths.push_back(std::sqrt(std::pow(xy_P1[0]-RtPoints[2],2.)+std::pow(xy_P1[1]-RtPoints[3],2.)));
      tendon_loads.push_back(load_params.values[0]);
      tendon_loads.push_back(load_params.values[1]);
    }

  // Print tendon lengths to file
  std::fstream pfile;
  pfile.open((char*)"summary.dat", std::ios::out);
  pfile<<"#index \t target point(x,y) \t loads(P0,P1) \t lengths(L0,L1) ";
  for(int t=0; t<nTargets; ++t)
    pfile << "\n" << t+1 <<"\t"
	  <<target_points[2*t]<<" "<<target_points[2*t+1]<<"\t"
	  <<tendon_loads[2*t]<<" "<<tendon_loads[2*t+1]<<"\t"
	  <<tendon_lengths[2*t]<<" "<<tendon_lengths[2*t+1];
  pfile.flush();
  pfile.close();
    
  // clean up
  for(auto& x:load_params.dirs) delete x;
  delete str;
  PetscFinalize();
}


std::vector<double> ReadTargets(const std::string filename)
{
  std::vector<double> TargetPts{};
  
  std::fstream pfile;
  pfile.open(filename.c_str(), std::ios::in);
  assert(pfile.good());
  double xy[2];
  pfile >> xy[0];
  while(pfile.good())
    {
      pfile >> xy[1];
      TargetPts.push_back( xy[0] );
      TargetPts.push_back( xy[1] );
      pfile >> xy[0];
    }
  pfile.close();
  const int nPoints = static_cast<int>(TargetPts.size())/2;
  std::cout<<"\nRead "<<nPoints<<" points \n"<<std::flush;

  // Transform
  std::vector<double> newTargetPts{};
  for(int p=0; p<nPoints; p++)
    {
      double xy[] = {TargetPts[2*p], TargetPts[2*p+1]};
      xy[0] /= 3800;
      xy[1] /= 3800;
      xy[0] += 0.235;
      xy[1] += 0.235;
      xy[0] -= 0.04;
      xy[1] -= 0.01;
      if(p==0 || p%5==0 || p==nPoints-1)
	{
	  newTargetPts.push_back( xy[0] );
	  newTargetPts.push_back( xy[1] );
	}
    }
  
  // done
  return newTargetPts;
}



void PlotXY(const std::string filename, const std::vector<double>& xy)
{
  std::fstream pfile;
  pfile.open(filename.c_str(), std::ios::out);
  const int nPoints = static_cast<int>(xy.size()/2);
  for(int p=0; p<nPoints; ++p)
    pfile << xy[2*p]<<" "<<xy[2*p+1]<<"\n";
  pfile.close();
}
